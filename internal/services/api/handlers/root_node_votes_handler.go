package handlers

import (
	"net/http"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type RootNodeVotesHandler struct {
	EventHandler
	storage data.RootNodeVotesStorage
}

func NewRootNodeVotesHandler(eventHandler EventHandler, storage data.RootNodeVotesStorage) RootNodeVotesHandler {
	return RootNodeVotesHandler{
		EventHandler: eventHandler,
		storage:      storage,
	}
}

// GetRootNodeVotes returns root node votes.
// @Summary   	Get root node votes
// @Description Returns root node votes.
// @ID       	rootnode-votings
// @Accept    	json
// @Produce    	json
// @Param     	filter[rootAddress] query string false "Returns ALL root node proposals for `rootAddress`."
// @Param     	filter[startBlock] query int false "Returns root node proposals after `startBlock`."
// @Param     	sort query string false "Sorting order. Supported values: `account_address` (sort by account_address, asc), `-block` (sort by account_address, desc). Default - `-account_address`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.EventsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/root-node-metrics/rootnode-votings [get]
func (h *RootNodeVotesHandler) GetRootNodeVotes(w http.ResponseWriter, r *http.Request) {
	h.getEvents(h.storage.Select, w, r)
}

// GetRootNodeAggregatedVotes returns aggregated root node votes.
// @Summary   	Get aggregated root node votes
// @Description Returns aggregated root node votes.
// @ID       	rootnode-votings-aggregated
// @Accept    	json
// @Produce    	json
// @Param     	filter[rootAddress] query string false "Returns ALL root node proposals for `rootAddress`."
// @Param     	filter[startBlock] query int false "Returns root node proposals after `startBlock`."
// @Param     	sort query string false "Sorting order. Supported values: `account_address` (sort by account_address, asc), `-block` (sort by account_address, desc). Default - `-account_address`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.AggregatedEventsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/root-node-metrics/rootnode-votings/aggregated [get]
func (h *RootNodeVotesHandler) GetRootNodeAggregatedVotes(w http.ResponseWriter, r *http.Request) {
	h.getAggregatedEvents(h.storage.SelectAggregated, w, r)
}
