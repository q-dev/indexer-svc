package resources

import (
	"strconv"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type BlocksList struct {
	Data  []Block `json:"data"`
	Links *Links  `json:"links,omitempty"`
}

type Block struct {
	ID   string `json:"id"`
	Type string `json:"type"`

	Attributes BlockAttrs `json:"attributes"`
}

type BlockAttrs struct {
	Hash         string `json:"hash"`
	ParentHash   string `json:"parent_hash"`
	Miner        string `json:"miner"`
	MainAccount  string `json:"mainaccount"`
	Timestamp    int64  `json:"timestamp"`
	InTurnSigner string `json:"inturn_signer"`
	Difficulty   int64  `json:"difficulty"`
}

// PopulateBlocksList from data layer.
func PopulateBlocksList(blocks []data.Block) *BlocksList {
	resp := &BlocksList{
		Data: make([]Block, len(blocks)),
	}
	for i, block := range blocks {
		resp.Data[i] = Block{
			ID:   strconv.FormatUint(block.Number, 10),
			Type: "blocks",
			Attributes: BlockAttrs{
				Hash:         block.Hash,
				ParentHash:   block.Parent,
				Miner:        block.Signer,
				MainAccount:  block.MainAccount,
				Timestamp:    block.Timestamp,
				InTurnSigner: block.InTurnSigner,
				Difficulty:   block.Difficulty,
			},
		}
	}

	return resp
}
