package resources

type LayerZeroData struct {
	Root      []RootList      `json:"root"`
	Exclusion []ExclusionList `json:"exclusion"`
}

type LayerZeroList struct {
	Data LayerZeroData `json:"data"`
}

func PopulateLayerZeroList(root []RootList, exclusion []ExclusionList) LayerZeroList {
	return LayerZeroList{
		Data: LayerZeroData{
			Root:      root,
			Exclusion: exclusion,
		},
	}
}
