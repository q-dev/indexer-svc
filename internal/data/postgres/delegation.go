package postgres

import (
	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type DelegationStorage struct {
	db *pgdb.DB
}

func (s *DelegationStorage) Insert(delegations ...data.Delegation) error {
	if len(delegations) == 0 {
		return nil
	}

	query := squirrel.Insert("delegations").Columns(
		"delegator",
		"validator",
	)

	for _, delegation := range delegations {
		query = query.Values(
			delegation.Delegator,
			delegation.Validator,
		)
	}
	query = query.Suffix("ON CONFLICT DO NOTHING")

	return errors.Wrap(s.db.Exec(query), "failed to insert delegations")
}

func (s *DelegationStorage) Delete(delegations ...data.Delegation) error {
	if len(delegations) == 0 {
		return nil
	}

	query := squirrel.Delete("delegations")
	pairs := make([]squirrel.Sqlizer, len(delegations))
	for i, delegation := range delegations {
		pair := squirrel.And{
			squirrel.Eq{"delegator": delegation.Delegator},
			squirrel.Eq{"validator": delegation.Validator},
		}
		pairs[i] = pair
	}
	orClause := squirrel.Or(pairs)
	query = query.Where(orClause)

	return errors.Wrap(s.db.Exec(query), "failed to delete delegations")
}

func (s *DelegationStorage) CountDelegators() (uint64, error) {
	query := squirrel.Select("count(distinct delegator)").From("delegations")
	var count uint64
	err := s.db.Get(&count, query)
	if err != nil {
		return 0, errors.Wrap(err, "failed to count delegators")
	}
	return count, nil
}
