-- +migrate Up
-- force resync and make col required
DELETE
FROM blocks;

ALTER TABLE blocks
    ADD COLUMN signer       text    NOT NULL,
    ADD COLUMN timestamp    bigint  NOT NULL;

-- +migrate Down
ALTER TABLE blocks
DROP COLUMN signer,
DROP COLUMN timestamp;
