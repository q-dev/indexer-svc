-- +migrate Up

CREATE TYPE LAYER_ZERO_LIST_TYPE AS ENUM (
    'exclusion-list',
    'root-list'
);

CREATE TYPE LAYER_ZERO_LIST_STATUS AS ENUM (
    'active',
    'proposed'
);

CREATE TABLE layer_zero_lists (
    hash      CHAR(66)               PRIMARY KEY,
    timestamp TIMESTAMP              NOT NULL,
    type      LAYER_ZERO_LIST_TYPE   NOT NULL,
    status    LAYER_ZERO_LIST_STATUS NOT NULL,
    outdated  BOOLEAN                NOT NULL DEFAULT FALSE
);

CREATE TABLE node_account_pairs (
    node         CHAR(42) NOT NULL PRIMARY KEY,
    main_account CHAR(42)
);

CREATE TABLE node_exclusion (
    list_hash    CHAR(66),
    node         CHAR(42) NOT NULL,
    start_block  BIGINT   NOT NULL,
    end_block    BIGINT,
    PRIMARY KEY (list_hash, node, start_block, end_block),
    FOREIGN KEY (list_hash) REFERENCES layer_zero_lists(hash),
    FOREIGN KEY (node) REFERENCES node_account_pairs(node)
);

CREATE TABLE list_signers (
    list_hash    CHAR(66),
    signer       CHAR(42) NOT NULL,
    PRIMARY KEY (list_hash, signer),
    FOREIGN KEY (list_hash) REFERENCES layer_zero_lists(hash),
    FOREIGN KEY (signer) REFERENCES node_account_pairs(node)
);

CREATE TABLE root_nodes (
    list_hash    CHAR(66),
    node         CHAR(42) NOT NULL,
    PRIMARY KEY (list_hash, node),
    FOREIGN KEY (list_hash) REFERENCES layer_zero_lists(hash),
    FOREIGN KEY (node) REFERENCES node_account_pairs(node)
);

-- +migrate Down

DROP TABLE root_nodes;
DROP TABLE list_signers;
DROP TABLE node_exclusion;
DROP TABLE node_account_pairs;
DROP TABLE layer_zero_lists;

DROP TYPE LAYER_ZERO_LIST_STATUS;
DROP TYPE LAYER_ZERO_LIST_TYPE;
