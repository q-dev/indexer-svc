package postgres

import (
	"database/sql"
	"fmt"
	"sort"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// SummaryMetricsStorage implements data.SummaryMetricsStorage.
type SummaryMetricsStorage struct {
	db *pgdb.DB
}

// Select blocks with params.
// The SQL query:
// WITH block_metrics AS (
//
//	SELECT
//	    MAX(id) - 1000*101 + 1 AS min_block,
//	    MAX(id) AS max_block
//	FROM blocks
//
// ),
// calculations AS (
//
//		SELECT
//		    b.mainaccount AS main_account,
//		    SUM(CASE WHEN b.signer = b.inturn_signer THEN 1 ELSE 0 END) AS total_inturn_blocks,
//		    (select count(bk.inturn_signer) from blocks bk where bk.inturn_signer = b.signer) AS total_due_blocks,
//	     count(b.signer) as total_produced,
//		    b.signer AS signer
//		FROM blocks b
//		WHERE b.id BETWEEN (SELECT min_block FROM block_metrics) AND (SELECT max_block FROM block_metrics)
//		GROUP BY b.mainaccount, b.signer
//
// )
// SELECT
//
//		main_account,
//		signer,
//		total_inturn_blocks,
//		total_due_blocks,
//		total_produced-total_inturn_blocks AS total_outofturn_blocks,
//		CASE
//		    WHEN total_due_blocks = 0 THEN 0
//		    ELSE total_inturn_blocks::float / total_due_blocks
//	    END AS availability
//
// FROM calculations;
func (s *SummaryMetricsStorage) Select(params *data.SelectSummaryMetricsParams) ([]data.SummaryMetrics, error) {
	blockMetrics := squirrel.Select("0 AS min_block", "MAX(id) AS max_block")
	if params.Cycles != 0 {
		blockMetrics = squirrel.Select(
			fmt.Sprintf("MAX(id) - %d*101 + 1 AS min_block", params.Cycles),
			"MAX(id) AS max_block",
		)
	}

	blockMetrics = blockMetrics.From("blocks").
		Prefix("WITH block_metrics AS (").
		Suffix(")")
	blockMetricsSql, _, _ := blockMetrics.ToSql()

	calculations := squirrel.Select(
		"b.mainaccount AS main_account",
		"SUM(CASE WHEN b.signer = b.inturn_signer THEN 1 ELSE 0 END) AS total_inturn_blocks",
		"(select count(bk.inturn_signer) from blocks bk where (bk.id BETWEEN (SELECT min_block FROM block_metrics) AND (SELECT max_block FROM block_metrics)) and bk.inturn_signer = b.signer) AS total_due_blocks",
		"count(b.signer) as total_produced",
		"b.signer AS signer",
	).From("blocks b").
		Where("b.id BETWEEN (SELECT min_block FROM block_metrics) AND (SELECT max_block FROM block_metrics)").
		GroupBy("b.mainaccount", "b.signer").
		Prefix("calculations AS (").Suffix(")")
	calculationsSql, _, _ := calculations.ToSql()

	query := squirrel.Select(
		"main_account",
		"signer",
		"total_inturn_blocks",
		"total_due_blocks",
		"total_produced-total_inturn_blocks AS total_outofturn_blocks",
		"CASE WHEN total_due_blocks = 0 THEN 0 ELSE total_inturn_blocks::float / total_due_blocks END AS availability",
	).From("calculations").
		Prefix(fmt.Sprintf("%s, %s", blockMetricsSql, calculationsSql))

	if params.Signer != nil {
		query = query.Where(
			squirrel.Eq{
				"signer": *params.Signer,
			},
		)
	}

	if params.MainAccount != nil {
		query = query.Where(
			squirrel.Eq{
				"main_account": *params.MainAccount,
			},
		)
	}

	order, sign := "ASC", ">"
	if params.IsDesc {
		order, sign = "DESC", "<"
	}

	query = query.OrderBy(fmt.Sprintf("main_account %s", order)).
		Limit(params.Limit)

	if params.Cursor != nil {
		query = query.Where(
			fmt.Sprintf("(main_account %s ?)", sign),
			*params.Cursor,
		)
	}

	var result []data.SummaryQuery
	err := s.db.Select(&result, query)

	return s.calculateMetrics(result), err
}

func (s *SummaryMetricsStorage) calculateMetrics(metricsQuery []data.SummaryQuery) []data.SummaryMetrics {
	var metricsSumInMap = make(map[string]data.SummaryMetrics)
	metricSumIdx := 0

	for _, metric := range metricsQuery {
		metricsByMinerOne := data.MetricsByMiner{
			DueBlocks:       metric.TotalDueBlocks,
			InTurnBlocks:    metric.TotalInTurnBlocks,
			OutOfTurnBlocks: metric.TotalOutOfTurnBlocks,
		}
		if _, ok := metricsSumInMap[metric.MainAccount]; !ok {
			metricByMiner := make(map[string]data.MetricsByMiner)
			metricByMiner[metric.Signer] = metricsByMinerOne
			metricsSumInMap[metric.MainAccount] = data.SummaryMetrics{
				Number:               uint64(metricSumIdx),
				MainAccount:          metric.MainAccount,
				TotalDueBlocks:       metric.TotalDueBlocks,
				TotalInTurnBlocks:    metric.TotalInTurnBlocks,
				TotalOutOfTurnBlocks: metric.TotalOutOfTurnBlocks,
				MetricsByMiner:       metricByMiner,
			}
			metricSumIdx++
		} else {
			metricsSumOne := metricsSumInMap[metric.MainAccount]
			metricsSumOne.TotalDueBlocks += metricsByMinerOne.DueBlocks
			metricsSumOne.TotalInTurnBlocks += metricsByMinerOne.InTurnBlocks
			metricsSumOne.TotalOutOfTurnBlocks += metricsByMinerOne.OutOfTurnBlocks
			metricsSumOne.MetricsByMiner[metric.Signer] = metricsByMinerOne
			metricsSumInMap[metric.MainAccount] = metricsSumOne
		}
	}

	var keys []string
	for key := range metricsSumInMap {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	metricsSum := make([]data.SummaryMetrics, 0, len(metricsSumInMap))
	for _, val := range keys {
		metricsSum = append(metricsSum, metricsSumInMap[val])
	}

	return metricsSum
}

func (s *SummaryMetricsStorage) queryBlock(query squirrel.SelectBuilder) (*data.SummaryMetrics, error) {
	var metrics data.SummaryMetrics
	switch err := s.db.Get(&metrics, query); err {
	case nil:
		return &metrics, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get metrics")
	}
}
