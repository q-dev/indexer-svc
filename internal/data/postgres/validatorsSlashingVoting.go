package postgres

import (
	"database/sql"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// ValidatorsSlashingVotingStorage implements data.ValidatorsSlashingVotingStorage.
type ValidatorsSlashingVotingStorage struct {
	db *pgdb.DB
}

var (
	vSVotingColumns = []string{
		"proposal_id",
		"voting_type",
		"voting_status",
		"current_quorum",
		"required_quorum",
		"cast(extract(epoch from voting_end_time) as integer) as voting_end_time",
		"cast(extract(epoch from voting_start_time) as integer) as voting_start_time",
		"cast(extract(epoch from veto_end_time) as integer) as veto_end_time",
	}
)

func (s *ValidatorsSlashingVotingStorage) Insert(votings ...data.ValidatorsSlashingVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert("validators_slashing_votings").Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_quorum",
		"required_quorum",
		"root_nodes_number",
		"voting_end_time",
		"veto_end_time",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentQuorum,
			voting.RequiredQuorum,
			voting.RootNodesNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
			time.Unix(int64(voting.VetoEndTime), 0),
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_quorum=EXCLUDED.current_quorum," +
		"root_nodes_number=EXCLUDED.root_nodes_number",
	)

	return errors.Wrap(s.db.Exec(query), "failed to insert validators slashing voting")
}

// GetLatest validators slashing voting.
func (s *ValidatorsSlashingVotingStorage) GetLatest() (*data.ValidatorsSlashingVoting, error) {
	return s.queryBlock(squirrel.Select(vSVotingColumns...).From("validators_slashing_votings").
		Where("proposal_id = (SELECT max(proposal_id) from validators_slashing_votings)"))
}

func (s *ValidatorsSlashingVotingStorage) GetEarliestUnfinishedVotings() ([]data.ValidatorsSlashingVoting, error) {
	var votings []data.ValidatorsSlashingVoting
	query := squirrel.Select(vSVotingColumns...).From("validators_slashing_votings").
		Where("voting_status IN (1, 3, 4)").OrderBy("proposal_id ASC")
	if err := s.db.Select(&votings, query); err != nil {
		return nil, errors.Wrap(err, "failed to select validators slashing votings")
	}
	return votings, nil
}

func (s *ValidatorsSlashingVotingStorage) Update(voting *data.ValidatorsSlashingVoting) error {
	query := squirrel.Update("validators_slashing_votings").SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_quorum":    voting.CurrentQuorum,
		"root_nodes_number": voting.RootNodesNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query), "failed to update validators slashing voting")
}

func (s *ValidatorsSlashingVotingStorage) queryBlock(query squirrel.SelectBuilder) (*data.ValidatorsSlashingVoting, error) {
	var voting data.ValidatorsSlashingVoting
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get validators slashing voting")
	}
}
