package postgres

import (
	"time"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"github.com/Masterminds/squirrel"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

var rnVotingTables = []string{
	"emergency_update_votings",
	"validators_slashing_votings",
	ContractRegistryAddressVotingTable,
	ContractRegistryUpgradeVotingTable,
	ContractRegistryVotingTable,
}

// RootNodeVotesStorage implements data.VoteStorage.
type RootNodeVotesStorage struct {
	*VotingEventsStorage
}

// SelectAggregated root node aggregated votes with params.
func (s *RootNodeVotesStorage) SelectAggregated(params data.SelectVotingEventsParams) (data.CountVotingEvents, error) {
	queryUnion := squirrel.Select("voting_type, COUNT(*) as amount").From(rnVotingTables[0]).GroupBy("voting_type")
	// TODO find a better way to iterate over array - now I just removed duplication
	for _, tableName := range rnVotingTables[1:] {
		err := UnionAll(&queryUnion,
			squirrel.Select("voting_type, COUNT(*) as amount").
				From(tableName).
				Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).
				GroupBy("voting_type"))
		if err != nil {
			return data.CountVotingEvents{}, errors.Wrap(err, "failed to union count query", logan.F{
				"table_name": tableName,
			})
		}
	}

	return s.selectAggregated(params, queryUnion)
}
