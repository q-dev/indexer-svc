package postgres

import (
	"database/sql"
	"fmt"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// RootsMembershipStorage implements data.RootsVotingStorage.
type RootsMembershipStorage struct {
	db *pgdb.DB
}

func (s *RootsMembershipStorage) Insert(rnMembership *data.RootsMembership) error {
	// todo: update on conflict
	query := squirrel.Insert("rn_membership_periods").SetMap(map[string]interface{}{
		"period_id":    rnMembership.PeriodId,
		"root_address": rnMembership.RootAddress,
		"start_block":  rnMembership.StartBlock,
		"end_block":    rnMembership.EndBlock,
		"start_time":   rnMembership.StartTime,
		"end_time":     rnMembership.EndTime,
	})

	return errors.Wrap(s.db.Exec(query), "failed to insert root membership period")
}

// GetLatest roots voting.
func (s *RootsMembershipStorage) GetLatest() (*data.RootsMembership, error) {
	return s.queryBlock(squirrel.Select("rn_membership_periods.*").From("rn_membership_periods").
		Where("period_id = (SELECT max(period_id) from rn_membership_periods)"))
}

func (s *RootsMembershipStorage) GetUnfinishedPeriodByAddress(rootAddress string) (*data.RootsMembership, error) {
	return s.queryBlock(squirrel.Select("rn_membership_periods.*").From("rn_membership_periods").
		Where("root_address = ?", rootAddress).OrderBy("start_block DESC").Limit(1))
}

func (s *RootsMembershipStorage) FindNextPeriodByAddress(rootAddress string, startBlock int64) (*data.RootsMembership, error) {
	return s.queryBlock(squirrel.Select("rn_membership_periods.*").From("rn_membership_periods").
		Where("root_address = ?", rootAddress).Where("start_block > ?", startBlock).OrderBy("period_id ASC").Limit(1))
}

func (s *RootsMembershipStorage) GetUnfinishedPeriods() ([]data.RootsMembership, error) {
	var periods []data.RootsMembership
	query := squirrel.Select("rn_membership_periods.*").From("rn_membership_periods").
		Where("end_block = 0").OrderBy("root_address, start_block")
	if err := s.db.Select(&periods, query); err != nil {
		return nil, errors.Wrap(err, "failed to select unfinished periods")
	}
	return periods, nil
}

func (s *RootsMembershipStorage) GetUnfinishedPeriodsByAddress(rootAddress string) ([]data.RootsMembership, error) {
	var periods []data.RootsMembership
	query := squirrel.Select("rn_membership_periods.*").
		From("rn_membership_periods").
		Where("root_address = ?", rootAddress).
		Where("end_block = 0").
		OrderBy("start_block DESC")

	err := s.db.Select(&periods, query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to select unfinished periods for address")
	}

	return periods, nil
}

func (s *RootsMembershipStorage) SelectActiveRootAddresses() ([]string, error) {
	query := squirrel.Select("lower(root_address)").
		From("rn_membership_periods").
		Where("end_block = 0")

	var addresses []string
	err := s.db.Select(&addresses, query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to select root addresses without end_block")
	}

	return addresses, nil
}

func (s *RootsMembershipStorage) Update(rnMembership *data.RootsMembership) error {
	query := squirrel.Update("rn_membership_periods").SetMap(map[string]interface{}{
		"end_block": rnMembership.EndBlock,
		"end_time":  rnMembership.EndTime,
	}).Where("period_id = ?", rnMembership.PeriodId)

	return errors.Wrap(s.db.Exec(query), "failed to update root membership period")
}

// Select root membership periods with params.
func (s *RootsMembershipStorage) Select(params *data.SelectRootsMembershipParams) ([]data.RootsMembership, error) {
	query := squirrel.Select("rn_membership_periods.*").From("rn_membership_periods")

	order := "ASC"
	if params.IsDesc {
		order = "DESC"
	}

	query = query.OrderBy(fmt.Sprintf("rn_membership_periods.start_block %s", order)).
		Limit(params.Limit)

	if params.Cursor != nil {
		query = query.Offset(*params.Cursor)
	}

	if params.RootAddress != "" {
		query = query.Where("(rn_membership_periods.root_address = ?)", params.RootAddress)
	} else {
		query = query.Where("(rn_membership_periods.end_block = 0)")
	}

	var votings []data.RootsMembership
	if err := s.db.Select(&votings, query); err != nil {
		return nil, errors.Wrap(err, "failed to select root membership periods")
	}

	return votings, nil
}

func (s *RootsMembershipStorage) queryBlock(query squirrel.SelectBuilder) (*data.RootsMembership, error) {
	var voting data.RootsMembership
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get root membership period")
	}
}

// Erase all history starting fromID inclusively.
func (s *RootsMembershipStorage) Erase(fromID uint64) error {
	query := squirrel.Delete("rn_membership_periods").
		Where("period_id >= ?", fromID)

	return errors.Wrap(s.db.Exec(query), "failed to erase roots membership periods")
}
