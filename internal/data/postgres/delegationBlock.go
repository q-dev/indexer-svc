package postgres

import (
	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type DelegationBlockStorage struct {
	db *pgdb.DB
}

func (s *DelegationBlockStorage) GetNumber() (uint64, error) {
	query := squirrel.Select().Columns("*").From("delegation_block")
	var res uint64 = 0
	err := s.db.Get(&res, query)
	if err != nil {
		insertQuery := squirrel.Insert("delegation_block").Values(0)
		err = s.db.Exec(insertQuery)
	}
	return res, errors.Wrap(err, "failed to get last queried block number for delegations")
}

func (s *DelegationBlockStorage) Update(number uint64) error {
	query := squirrel.Update("delegation_block").Set("number", number)
	return errors.Wrap(s.db.Exec(query), "failed to update last queried block number for delegations")
}
