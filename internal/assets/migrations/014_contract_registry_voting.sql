-- +migrate Up
CREATE TABLE contract_registry_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_majority    TEXT       DEFAULT NULL,
    required_majority   TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    votes_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL
);

-- +migrate Down
DROP TABLE contract_registry_votings;