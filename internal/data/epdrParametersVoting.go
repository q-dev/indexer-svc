package data

// EpdrParametersVoting voting.
type EpdrParametersVoting BaseVoting

// EpdrParametersVotingStorage stores epdr parameters votings.
type EpdrParametersVotingStorage interface {
	Insert(votings ...EpdrParametersVoting) error
	GetLatest() (*EpdrParametersVoting, error)

	GetEarliestUnfinishedVotings() ([]EpdrParametersVoting, error)
	Update(voting *EpdrParametersVoting) error
}
