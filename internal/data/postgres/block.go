package postgres

import (
	"database/sql"
	"fmt"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// BlockStorage implements data.BlockStorage.
type BlockStorage struct {
	db *pgdb.DB
}

func (s *BlockStorage) Insert(block *data.Block) error {
	// todo: update on conflict
	query := squirrel.Insert("blocks").SetMap(map[string]interface{}{
		"id":            block.Number,
		"hash":          block.Hash,
		"parent":        block.Parent,
		"signer":        block.Signer,
		"mainaccount":   block.MainAccount,
		"timestamp":     block.Timestamp,
		"inturn_signer": block.InTurnSigner,
		"difficulty":    block.Difficulty,
	})

	return errors.Wrap(s.db.Exec(query), "failed to insert block")
}

func (s *BlockStorage) GetByNumber(number uint64) (*data.Block, error) {
	return s.queryBlock(squirrel.Select("blocks.*").From("blocks").
		Where("id = ?", number))
}

// GetLatest block.
func (s *BlockStorage) GetLatest() (*data.Block, error) {
	return s.queryBlock(squirrel.Select("blocks.*").From("blocks").
		Where("id = (SELECT max(id) from blocks)"))
}

// Select blocks with params.
func (s *BlockStorage) Select(params *data.SelectBlocksParams) ([]data.Block, error) {
	query := squirrel.Select("blocks.*").From("blocks")

	if params.From != nil {
		query = query.Where("(blocks.id >= ?)", *params.From)
	}
	if params.To != nil {
		query = query.Where("(blocks.id <= ?)", *params.To)
	}

	order, sign := "ASC", ">"
	if params.IsDesc {
		order, sign = "DESC", "<"
	}
	query = query.OrderBy(fmt.Sprintf("blocks.id %s", order)).
		Limit(params.Limit)

	if params.Cursor != nil {
		query = query.Where(
			fmt.Sprintf("(blocks.id %s ?)", sign),
			*params.Cursor,
		)
	}

	if params.Signer != nil {
		query = query.Where("(blocks.signer = ?)", *params.Signer)
	}

	if params.InTurnSigner != nil {
		query = query.Where("(blocks.inturn_signer = ?)", *params.InTurnSigner)
	}

	if params.AnySigner != nil {
		query = query.Where("(blocks.signer = ? OR blocks.inturn_signer = ?)", *params.AnySigner, *params.AnySigner)
	}

	if params.OutOfTurn {
		query = query.Where("(blocks.signer <> blocks.inturn_signer)")
	}

	var blocks []data.Block
	if err := s.db.Select(&blocks, query); err != nil {
		return nil, errors.Wrap(err, "failed to select blocks")
	}

	return blocks, nil
}

func (s *BlockStorage) queryBlock(query squirrel.SelectBuilder) (*data.Block, error) {
	var block data.Block
	switch err := s.db.Get(&block, query); err {
	case nil:
		return &block, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get block")
	}
}

// Erase all history starting fromID inclusively.
func (s *BlockStorage) Erase(fromID uint64) error {
	query := squirrel.Delete("blocks").
		Where("id >= ?", fromID)

	return errors.Wrap(s.db.Exec(query), "failed to erase blocks")
}
