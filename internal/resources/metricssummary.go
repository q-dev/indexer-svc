package resources

import (
	"strconv"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type SummaryMetricsList struct {
	Data  []SummaryMetrics `json:"data"`
	Links *Links           `json:"links,omitempty"`
}

type SummaryMetrics struct {
	ID   string `json:"id"`
	Type string `json:"metrics"`

	Attributes SummaryMetricsAttrs `json:"attributes"`
}

type SummaryMetricsAttrs struct {
	MainAccount     string                         `db:"mainaccount"`
	DueBlocks       uint64                         `db:"totaldueblocks"`
	InTurnBlocks    uint64                         `db:"totalinturnblocks"`
	OutOfTurnBlocks uint64                         `db:"totaloutofturnblocks"`
	MetricsByMiner  map[string]data.MetricsByMiner `db:"metricsbyminer"`
}

// PopulateMetricsList from data layer.
func PopulateSummaryMetricsList(metrics []data.SummaryMetrics) *SummaryMetricsList {
	resp := &SummaryMetricsList{
		Data: make([]SummaryMetrics, len(metrics)),
	}
	for i, metric := range metrics {
		resp.Data[i] = SummaryMetrics{
			ID:   strconv.FormatUint(metric.Number, 10),
			Type: "metrics",
			Attributes: SummaryMetricsAttrs{
				MainAccount:     metric.MainAccount,
				DueBlocks:       metric.TotalDueBlocks,
				InTurnBlocks:    metric.TotalInTurnBlocks,
				OutOfTurnBlocks: metric.TotalOutOfTurnBlocks,
				MetricsByMiner:  metric.MetricsByMiner,
			},
		}
	}

	return resp
}
