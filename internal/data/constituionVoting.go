package data

// ConstitutionVoting voting.
type ConstitutionVoting struct {
	BaseVoting
	VotingSubType uint8 `db:"voting_subtype"`
}

// ConstitutionVotingStorage stores constitution votings.
type ConstitutionVotingStorage interface {
	Insert(votings ...ConstitutionVoting) error
	GetLatest() (*ConstitutionVoting, error)

	GetEarliestUnfinishedVotings() ([]ConstitutionVoting, error)
	Update(voting *ConstitutionVoting) error
}
