package postgres

import (
	"database/sql"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// VotingAgentStorage implements data.VotingAgent.
type VotingAgentStorage struct {
	db *pgdb.DB
}

func (s *VotingAgentStorage) Insert(agents ...data.VotingAgent) error {
	if len(agents) == 0 {
		return nil
	}

	query := squirrel.Insert("voting_agents").
		Columns("address", "voting_agent", "start_block", "end_block")
	for _, agent := range agents {
		query = query.Values(agent.Address, agent.VotingAgent, agent.StartBlock, agent.EndBlock)
	}

	return errors.Wrap(s.db.Exec(query), "failed to insert voting agent")
}

func (s *VotingAgentStorage) GetByVotingAgents(address ...string) ([]data.VotingAgent, error) {
	if len(address) > 0 {
		return nil, nil
	}

	var res []data.VotingAgent

	query := squirrel.Select("voting_agents.*").
		From("voting_agents").
		Where(squirrel.Eq{
			"voting_agent": address,
			"end_block":    0,
		})

	err := s.db.Select(&res, query)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}

	return res, nil
}

func (s *VotingAgentStorage) GetLatestBlock() (int64, error) {
	latestBlock := sql.NullInt64{}

	err := s.db.Get(&latestBlock, squirrel.Select("MAX(end_block)").From("voting_agents"))
	if err != nil {
		return 0, err
	}

	return latestBlock.Int64, nil
}
