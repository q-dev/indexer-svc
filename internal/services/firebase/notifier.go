package firebase

import (
	"context"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"google.golang.org/api/iterator"

	"cloud.google.com/go/firestore"
	"firebase.google.com/go/v4/messaging"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/logan/v3"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

const maxNotificationsAtOnce = 10

// Payment represents firebase payment model
type Payment struct {
	NetworkId int64

	TxHash    string
	Sender    string
	Recipient string
	Value     string

	Token *string

	Timestamp int64

	Gas      int64
	GasPrice string
	Input    []byte
}

type Notification struct {
	NetworkId   int64
	TxHash      string
	TxTimestamp int64
	Created     int64
	Sent        int64
}

// Transaction represents firebase transaction model
type Transaction struct {
	NetworkId int64

	Hash      string
	Block     int64
	Sender    string
	Recipient *string
	Value     string

	Nonce    int64
	GasPrice string
	Gas      int64
	Input    []byte

	Timestamp int64
}

// Notifier implements ingest notifier and sends data to firebase instance
type Notifier struct {
	disabled        bool
	log             *logan.Entry
	firestoreClient *firestore.Client
	messagingClient *messaging.Client
	chainId         int64
}

// NewNotifier creates new firebase notifier
func NewNotifier(
	log *logan.Entry,
	firestoreClient *firestore.Client,
	messagingClient *messaging.Client,
	chainId int64,
) *Notifier {
	return &Notifier{
		disabled:        false,
		log:             log.WithField("svc", "firebase"),
		firestoreClient: firestoreClient,
		messagingClient: messagingClient,
		chainId:         chainId,
	}
}

// NewDisabledNotifier creates new disabled notifier
func NewDisabledNotifier(
	log *logan.Entry,
	chainId int64,
) *Notifier {
	return &Notifier{
		disabled: true,
		log:      log.WithField("svc", "firebase-disabled"),
		chainId:  chainId,
	}
}

// NewTransaction sends transaction data to firebase instance
func (n *Notifier) NewTransaction(tx *data.Transaction) {
	if n.disabled {
		n.log.WithField("tx", tx.Hash).Warn("disabled, skipping sending notification about transaction")
		return
	}
	n.log.WithField("tx", tx.Hash).Info("new transaction")

	n.saveTransaction(tx)
}

// NewPayment sends payment data to firebase instance
func (n *Notifier) NewPayment(pmnt *data.Payment) {
	if n.disabled {
		n.log.WithField("tx", pmnt.TxHash).Warn("disabled, skipping sending notification about payment")
		return
	}
	n.log.WithField("tx", pmnt.TxHash).Info("new payment")

	n.savePayment(pmnt)
	n.saveNotification(pmnt)
}

func (n *Notifier) SendNotifications(timestamp int64, ctx context.Context) error {
	if n.disabled {
		n.log.WithField("timestamp", timestamp).Warn("disabled, skipping sending notification")
		return nil
	}
	err := n.sendPaymentNotifications(timestamp, ctx)
	if err != nil {
		return err
	}
	return nil
}

func (n *Notifier) saveTransaction(tx *data.Transaction) {
	firebaseTx := &Transaction{
		NetworkId: n.chainId,
		Hash:      tx.Hash,
		Block:     int64(tx.Block),
		Sender:    tx.Sender,
		Recipient: tx.Recipient,
		Value:     tx.Value.Unwrap().String(),

		Nonce:    int64(tx.Nonce),
		Gas:      int64(tx.Gas),
		GasPrice: tx.GasPrice.Unwrap().String(),
		Input:    tx.Input,

		Timestamp: tx.Timestamp,
	}

	doc := n.firestoreClient.Collection("transactions").Doc(tx.Hash)
	_, err := doc.Set(context.Background(), firebaseTx)
	if err != nil {
		n.log.
			WithField("tx", tx.Hash).
			Error(fmt.Sprintf("firestore save: %s", err.Error()))
	}
}

func (n *Notifier) savePayment(pmnt *data.Payment) {
	if pmnt.Tx == nil {
		n.log.Panic("expected tx in payment")
	}

	firebasePmnt := &Payment{
		NetworkId: n.chainId,
		TxHash:    pmnt.TxHash,
		Sender:    pmnt.Sender,
		Recipient: pmnt.Recipient,
		Value:     pmnt.Value.Unwrap().String(),

		Token: pmnt.Token,

		Timestamp: pmnt.Timestamp,

		Gas:      int64(pmnt.Tx.Gas),
		GasPrice: pmnt.Tx.GasPrice.Unwrap().String(),
		Input:    pmnt.Tx.Input,
	}

	doc := n.firestoreClient.Collection("payments").Doc(pmnt.TxHash)
	_, err := doc.Set(context.Background(), firebasePmnt)
	if err != nil {
		n.log.
			WithField("payment", pmnt.TxHash).
			Error(fmt.Sprintf("firestore save: %s", err.Error()))
	}
}

func (n *Notifier) saveNotification(pmnt *data.Payment) {
	firebaseNtf := &Notification{
		NetworkId:   n.chainId,
		TxHash:      pmnt.TxHash,
		TxTimestamp: pmnt.Timestamp,
		Created:     time.Now().Unix(),
		Sent:        0,
	}

	doc := n.firestoreClient.Collection("notifications").Doc(pmnt.TxHash)
	_, err := doc.Set(context.Background(), firebaseNtf)
	if err != nil {
		n.log.
			WithField("notification", pmnt.TxHash).
			Error(fmt.Sprintf("firestore save: %s", err.Error()))
	}
}

func (n *Notifier) sendPaymentNotifications(timestamp int64, ctx context.Context) error {
	nfCollection := n.firestoreClient.Collection("notifications")
	notificationsIter := nfCollection.OrderBy("TxTimestamp", firestore.Asc).
		Where("TxTimestamp", ">", timestamp).
		Where("Sent", "==", 0).
		Where("NetworkId", "==", n.chainId).
		OrderBy("Created", firestore.Asc).
		Limit(maxNotificationsAtOnce).Documents(ctx)

	defer notificationsIter.Stop()
	wg := &sync.WaitGroup{}
	wgDone := make(chan struct{})
	errChannel := make(chan error, maxNotificationsAtOnce)
	for {
		notif, err := notificationsIter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return errors.Wrap(err, "failed to extract firestore notification")
		}

		paymentHash := notif.Data()["TxHash"].(string)
		wg.Add(1)
		go func() {
			defer wg.Done()
			payment, err := n.firestoreClient.Collection("payments").Doc(paymentHash).Get(ctx)
			if err != nil {
				errChannel <- errors.Wrap(err, "firestore extract")
				return
			}
			n.prepareNotification(payment)
			n.updateNotification(paymentHash)
		}()
	}

	go func() {
		wg.Wait()
		close(wgDone)
		close(errChannel)
	}()

	select {
	case <-ctx.Done():
		n.log.WithError(ctx.Err()).Info("Waiting for finishing payment notifications handling")
		wg.Wait()
		n.log.WithError(ctx.Err()).Info("Payment notifications were handled")
		return nil
	case <-wgDone:
		break
	case err := <-errChannel:
		return err
	}

	return nil
}

func (n *Notifier) prepareNotification(payment *firestore.DocumentSnapshot) {
	title := "Transaction"
	body := "You have new transaction"

	msgNotification := &messaging.Notification{
		Title: title,
		Body:  body,
	}

	msgApns := &messaging.APNSConfig{
		Payload: &messaging.APNSPayload{
			Aps: &messaging.Aps{
				MutableContent: true,
			},
		},
	}

	msgFCPOptions := &messaging.FCMOptions{
		AnalyticsLabel: "payment",
	}

	var token string
	if payment.Data()["Token"] != nil {
		token = payment.Data()["Token"].(string)
	}

	msgData := map[string]string{
		"network_id": strconv.FormatInt(payment.Data()["NetworkId"].(int64), 10),
		"txHash":     payment.Data()["TxHash"].(string),
		"value":      payment.Data()["Value"].(string),
		"sender":     payment.Data()["Sender"].(string),
		"recipient":  payment.Data()["Recipient"].(string),
		"token":      token,

		"timestamp": strconv.FormatInt(payment.Data()["Timestamp"].(int64), 10),

		"gas":       strconv.FormatInt(payment.Data()["Gas"].(int64), 10),
		"gas_price": payment.Data()["GasPrice"].(string),
		"input":     hex.EncodeToString(payment.Data()["Input"].([]byte)),
	}

	msg := &messaging.Message{
		Data:         msgData,
		Notification: msgNotification,
		APNS:         msgApns,
		FCMOptions:   msgFCPOptions,
	}

	msg.Topic = strings.ToLower(payment.Data()["Sender"].(string))
	n.sendNotification(msg)

	msg.Topic = strings.ToLower(payment.Data()["Recipient"].(string))
	n.sendNotification(msg)
}

func (n *Notifier) sendNotification(msg *messaging.Message) {
	msgId, err := n.messagingClient.Send(context.Background(), msg)
	if err != nil {
		n.log.
			WithField("tx", msg.Data["txHash"]).
			WithField("topic", msg.Topic).
			Error(fmt.Sprintf("fcm send: %s", err.Error()))
	} else {
		n.log.
			WithField("tx", msg.Data["txHash"]).
			WithField("topic", msg.Topic).
			WithField("msgId", msgId).
			Info("fcm send success")
	}
}

func (n *Notifier) updateNotification(paymentHash string) {
	_, err := n.firestoreClient.Collection("notifications").Doc(paymentHash).
		Update(context.Background(), []firestore.Update{{Path: "Sent", Value: time.Now().Unix()}})
	if err != nil {
		n.log.
			WithField("notification", paymentHash).
			Error(fmt.Sprintf("firestore update: %s", err.Error()))
	}
}
