package api

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/contracts"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

type votingsHandler struct {
	log              *logan.Entry
	storage          data.VotingStorage
	contractRegistry *contracts.Registry
}

type getVotingsQuery struct {
	Sort   string  `schema:"sort"`
	Cursor *uint64 `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	Type     string   `schema:"filter[type],omitempty"`
	Status   []string `schema:"filter[status],omitempty"`
	Finished *bool    `schema:"filter[finished],omitempty"`
}

func (q *getVotingsQuery) Validate() error {
	return validation.Errors{
		"sort":  validation.Validate(q.Sort, validation.In("voting_end_time", "-voting_end_time")),
		"limit": validation.Validate(q.Limit, validation.Max(100)),
	}.Filter()
}

// @Summary   	Get votings
// @Description Returns history of votings.
// @ID       	votings
// @Accept    	json
// @Produce    	json
// @Param     	filter[type] query string false "Filter votings by type. Supported values : `constitution_voting`, `general_update_voting`, `emergency_update_voting`, `root_nodes_membership_voting`, `root_nodes_slashing_voting`, `validators_slashing_voting`, `epdr_membership_voting`, `epdr_parameters_voting`, `epqfi_membership_voting`, `epqfi_parameters_voting`, `eprs_membership_voting`, `eprs_parameters_voting`, `address_voting`, `upgrade_voting`."
// @Param     	filter[status] query string false "Filter by status. Supported values `0..7`. Example: `1,2,3`"
// @Param     	filter[finished] query bool false "Filter finished / unfinished votings"
// @Param     	sort query string false "Sorting order. Supported values: `voting_end_time` (sort by voting end time, asc), -`voting_end_time` (sort by voting end time, desc). Default - `-voting_end_time`."
// @Param     	page[cursor] query int false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.VotingsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/votings [get]
func (h *votingsHandler) getVotings(w http.ResponseWriter, r *http.Request) {
	cursor := uint64(0)
	query := getVotingsQuery{
		Sort:   "-voting_end_time",
		Limit:  20,
		Cursor: &cursor,
	}
	if err := schema.NewDecoder().Decode(&query, r.URL.Query()); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	currentRootNodesCount, err := h.contractRegistry.Roots().GetSize(nil)
	if err != nil {
		h.log.WithError(err).Error("failed to get current root nodes count")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	votings, err := h.storage.Select(
		&data.SelectVotingsParams{
			IsDesc:   strings.HasPrefix(query.Sort, "-"),
			Cursor:   query.Cursor,
			Limit:    query.Limit,
			Type:     "'" + strings.ReplaceAll(query.Type, ",", "','") + "'",
			Status:   query.Status,
			Finished: query.Finished,
		},
		currentRootNodesCount.Uint64(),
	)

	if err != nil {
		h.log.WithError(err).Errorf("failed to query votings; query %#v", query)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	resp, err := resources.PopulateVotingsList(votings)
	if err != nil {
		h.log.WithError(err).Error("failed to populate votings list")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	resp.Links = h.buildVotingsLinks(uint64(len(votings)), *r.URL, query)

	ape.Render(w, resp)
}

func (h *votingsHandler) buildVotingsLinks(votingsCount uint64, endpoint url.URL, query getVotingsQuery) *resources.Links {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(query, params); err != nil {
		panic(errors.Wrap(err, "failed to encode query"))
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if votingsCount == 0 {
		return links
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			if votingsCount < query.Limit {
				return ""
			}
			step := *query.Cursor + query.Limit
			params.Set("page[cursor]", strconv.FormatUint(step, 10))
			url.RawQuery = params.Encode()
			return url.String()
		} else {
			cursor := int64(0)
			if query.Cursor != nil {
				cursor = int64(*query.Cursor)
			}
			step := cursor - int64(query.Limit)
			if step < 0 {
				step = 0
			}
			params.Set("page[cursor]", strconv.FormatUint(uint64(step), 10))
			url.RawQuery = params.Encode()
		}

		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)
	return links
}
