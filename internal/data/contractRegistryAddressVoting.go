package data

// ContractRegistryAddressVoting voting.
type ContractRegistryAddressVoting ContractRegistryVoting

// ContractRegistryVotingStorage stores contract registry votings.
type ContractRegistryVotingStorage interface {
	Insert(votings ...ContractRegistryVoting) error
	GetLatestId() (*int64, error)

	GetEarliestUnfinishedVotings() ([]ContractRegistryVoting, error)
	Update(voting *ContractRegistryVoting) error
}

type ContractRegistryAddressVotingStorage ContractRegistryVotingStorage
