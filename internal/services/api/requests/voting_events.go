package requests

import (
	"net/http"
	"reflect"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
)

type GetVotingEventsRequest struct {
	Sort   string  `schema:"sort"`
	Cursor *string `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	StartBlock     uint64 `schema:"filter[startBlock],omitempty"`
	AccountAddress string `schema:"filter[account],omitempty"`
}

func NewGetVotingEventsRequest(r *http.Request) (GetVotingEventsRequest, error) {
	var request GetVotingEventsRequest
	request.Limit = defaultListLimit
	request.Sort = "desc"

	decoder := schema.NewDecoder()
	decoder.RegisterConverter([]string{}, func(input string) reflect.Value {
		return reflect.ValueOf(strings.Split(input, ","))
	})
	if err := decoder.Decode(&request, r.URL.Query()); err != nil {
		return request, err
	}

	return request, request.Validate()
}

func (r *GetVotingEventsRequest) Validate() error {
	return validation.Errors{
		"sort":  validation.Validate(r.Sort, validation.In("asc", "desc")),
		"limit": validation.Validate(r.Limit, validation.Max(maxListLimit)),
	}.Filter()
}
