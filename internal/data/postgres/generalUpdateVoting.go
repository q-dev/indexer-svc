package postgres

import (
	"database/sql"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// GeneralUpdateVotingStorage implements data.GeneralUpdateVotingStorage.
type GeneralUpdateVotingStorage struct {
	db *pgdb.DB
}

var (
	commonVotingColumns = []string{
		"proposal_id",
		"voting_type",
		"voting_status",
		"current_quorum",
		"required_quorum",
		"vetos_number",
		"cast(extract(epoch from voting_end_time) as integer) as voting_end_time",
		"cast(extract(epoch from voting_start_time) as integer) as voting_start_time",
		"cast(extract(epoch from veto_end_time) as integer) as veto_end_time",
	}
)

func (s *GeneralUpdateVotingStorage) Insert(votings ...data.GeneralUpdateVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert("general_update_votings").Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_quorum",
		"required_quorum",
		"root_nodes_number",
		"vetos_number",
		"voting_end_time",
		"veto_end_time",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentQuorum,
			voting.RequiredQuorum,
			voting.RootNodesNumber,
			voting.VetosNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
			time.Unix(int64(voting.VetoEndTime), 0),
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_quorum=EXCLUDED.current_quorum," +
		"root_nodes_number=EXCLUDED.root_nodes_number," +
		"vetos_number=EXCLUDED.vetos_number",
	)

	return errors.Wrap(s.db.Exec(query), "failed to insert general update voting")
}

// GetLatest general update voting.
func (s *GeneralUpdateVotingStorage) GetLatest() (*data.GeneralUpdateVoting, error) {
	return s.queryBlock(squirrel.Select(commonVotingColumns...).From("general_update_votings").
		Where("proposal_id = (SELECT max(proposal_id) from general_update_votings)"))
}

func (s *GeneralUpdateVotingStorage) GetEarliestUnfinishedVotings() ([]data.GeneralUpdateVoting, error) {
	var votings []data.GeneralUpdateVoting
	query := squirrel.Select(commonVotingColumns...).From("general_update_votings").
		Where("voting_status IN (1, 3, 4)").OrderBy("proposal_id ASC")
	if err := s.db.Select(&votings, query); err != nil {
		return nil, errors.Wrap(err, "failed to select general update votings")
	}
	return votings, nil
}

func (s *GeneralUpdateVotingStorage) Update(voting *data.GeneralUpdateVoting) error {
	query := squirrel.Update("general_update_votings").SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_quorum":    voting.CurrentQuorum,
		"root_nodes_number": voting.RootNodesNumber,
		"vetos_number":      voting.VetosNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query), "failed to update general update voting")
}

func (s *GeneralUpdateVotingStorage) queryBlock(query squirrel.SelectBuilder) (*data.GeneralUpdateVoting, error) {
	var voting data.GeneralUpdateVoting
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get general update voting")
	}
}
