package data

// Transaction.
type Transaction struct {
	Hash      string  `db:"hash"`
	Block     uint64  `db:"block_id"`
	Sender    string  `db:"sender"`
	Recipient *string `db:"recipient"`
	Value     *BigInt `db:"value"`

	Nonce    uint64  `db:"nonce"`
	GasPrice *BigInt `db:"gas_price"`
	Gas      uint64  `db:"gas"`
	Input    []byte  `db:"input"`

	Timestamp int64 `db:"timestamp"`
}

// SelectTxsParams.
type SelectTxsParams struct {
	Cursor *string
	Limit  uint64
	IsDesc bool

	Sender    *string
	Recipient *string

	// Counterparty is either Sender or Recipient
	// and mutually exclusive to those filters
	Counterparty *string
}

// TransactionStorage.
type TransactionStorage interface {
	Insert(txs ...Transaction) error
	Get(hash string) (*Transaction, error)
	Select(params *SelectTxsParams) ([]Transaction, error)
}
