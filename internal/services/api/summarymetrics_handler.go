package api

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

type summaryMetricsHandler struct {
	log     *logan.Entry
	storage data.SummaryMetricsStorage
}

type getSummaryMetricsQuery struct {
	Sort   string  `schema:"sort"`
	Cursor *uint64 `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	Cycles      uint64  `schema:"filter[cycles],omitempty"`
	Signer      *string `schema:"filter[signer],omitempty"`
	MainAccount *string `schema:"filter[mainaccount],omitempty"`
}

func (q *getSummaryMetricsQuery) Validate() error {
	return validation.Errors{
		"sort":  validation.Validate(q.Sort, validation.In("id", "-id")),
		"limit": validation.Validate(q.Limit, validation.Max(100)),
	}.Filter()
}

// @Summary   	Get metrics
// @Description Returns history of metrics.
// @ID       	metrics
// @Accept    	json
// @Produce    	json
// @Param     	filter[cycles] query int false "Filter by amount of cycles."
// @Param     	filter[signer] query string false "Filter by actual signer."
// @Param     	filter[mainaccount] query string false "Filter by main account."
// @Param     	sort query string false "Sorting order. Supported values: `mainaccount` (sort by id, asc), `-mainaccount` (sort by id, desc). Default - `-mainaccount`."
// @Param     	page[cursor] query int false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.SummaryMetricsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router		/metrics [get]
// @Deprecated
func (h *summaryMetricsHandler) getOldValidatorMetrics(w http.ResponseWriter, r *http.Request) {
	h.getMetrics(w, r)
}

// @Summary   	Get metrics
// @Description Returns history of metrics.
// @ID       	validator-metrics
// @Accept    	json
// @Produce    	json
// @Param     	filter[cycles] query int false "Filter by amount of cycles."
// @Param     	filter[signer] query string false "Filter by actual signer."
// @Param     	filter[mainaccount] query string false "Filter by main account."
// @Param     	sort query string false "Sorting order. Supported values: `mainaccount` (sort by id, asc), `-mainaccount` (sort by id, desc). Default - `-mainaccount`."
// @Param     	page[cursor] query int false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.SummaryMetricsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router		/validator-metrics [get]
func (h *summaryMetricsHandler) getValidatorMetrics(w http.ResponseWriter, r *http.Request) {
	h.getMetrics(w, r)
}

func (h *summaryMetricsHandler) getMetrics(w http.ResponseWriter, r *http.Request) {
	query := getSummaryMetricsQuery{
		Sort:  "mainaccount",
		Limit: 20,
	}
	if err := schema.NewDecoder().Decode(&query, r.URL.Query()); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	metrics, err := h.storage.Select(&data.SelectSummaryMetricsParams{
		Cursor: query.Cursor,
		IsDesc: strings.HasPrefix(query.Sort, "-"),
		Limit:  query.Limit,

		Cycles:      query.Cycles,
		Signer:      query.Signer,
		MainAccount: query.MainAccount,
	})
	if err != nil {
		h.log.WithError(err).Errorf("failed to query metrics; query %#v", query)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	resp := resources.PopulateSummaryMetricsList(metrics)
	resp.Links = buildSummaryMetricsLinks(metrics, *r.URL, query)
	ape.Render(w, resp)
}

func buildSummaryMetricsLinks(metrics []data.SummaryMetrics, endpoint url.URL, query getSummaryMetricsQuery) *resources.Links {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(query, params); err != nil {
		panic(errors.Wrap(err, "failed to encode query"))
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(metrics) == 0 {
		return links
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", strconv.FormatUint(metrics[len(metrics)-1].Number, 10))
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", strconv.FormatUint(metrics[0].Number, 10))
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)
	return links
}
