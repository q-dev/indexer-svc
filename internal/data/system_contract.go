package data

import "database/sql"

const (
	ContractRegistryName = "contract-registry"
)

type SystemContractsStorage interface {
	Upsert(contracts ...SystemContract) error
	UpdateImplAddress(contract SystemContract) error

	Select(params SelectSysContractsParams) ([]SystemContract, error)
	GetContractRegistry() (*SystemContract, error)
}

type SystemContract struct {
	ID             uint64         `db:"id"             structs:"-"`
	Contract       string         `db:"proxy"          structs:"proxy"`
	Implementation sql.NullString `db:"implementation" structs:"implementation"`
	Key            sql.NullString `db:"key"            structs:"key"`
}

type SelectSysContractsParams struct {
	IsDesc bool
	Cursor *uint64
	Limit  uint64

	Contracts []string
	Keys      []string
}
