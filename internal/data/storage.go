package data

// Storage.
type Storage interface {
	Clone() Storage
	ExecInTx(fn func() error) error

	Blocks() BlockStorage
	Transactions() TransactionStorage
	Payments() PaymentsStorage
	Delegations() DelegationsStorage
	DelegationBlock() DelegationBlocksStorage

	Votings() VotingStorage
	ConstitutionVotings() ConstitutionVotingStorage
	GeneralUpdateVotings() GeneralUpdateVotingStorage
	EmergencyUpdateVotings() EmergencyUpdateVotingStorage
	RootsVotings() RootsVotingStorage
	RootnodesSlashingVotings() RootnodesSlashingVotingStorage
	ValidatorsSlashingVotings() ValidatorsSlashingVotingStorage
	EpqfiMembershipVotings() EpqfiMembershipVotingStorage
	EpqfiParametersVotings() EpqfiParametersVotingStorage
	EpdrMembershipVotings() EpdrMembershipVotingStorage
	EpdrParametersVotings() EpdrParametersVotingStorage
	EprsMembershipVotings() EprsMembershipVotingStorage
	EprsParametersVotings() EprsParametersVotingStorage
	ContractRegistryAddressVotings() ContractRegistryAddressVotingStorage
	ContractRegistryUpgradeVotings() ContractRegistryUpgradeVotingStorage
	ContractRegistryVotings() ContractRegistryVotingStorage
	SystemContracts() SystemContractsStorage
	LayerZeroLists() LayerZeroListStorage

	RootsMembership() RootsMembershipStorage
	QTokenHolderVotes() QTokenHolderVotesStorage
	RootNodeVotes() RootNodeVotesStorage
	RootNodeProposals() RootNodeProposalsStorage
	VotingAgents() VotingAgentStorage
	RootNodeApprovals() RootNodeApprovalsStorage

	SummaryMetrics() SummaryMetricsStorage
}
