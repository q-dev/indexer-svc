package handlers

import (
	"database/sql"
	"errors"
	"net/http"
	"time"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/q-dev/q-client/governance"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/requests"
)

var (
	notFoundError = errors.New("not found")
)

// GetRootNodesList
// @Summary   	Get root nodes list
// @Description Returns root nodes list.
// @ID       	root-list
// @Accept    	json
// @Produce    	json
// @Param     	status query string true "Active or proposed list."
// @Success   	200 {object} resources.RootList
// @Failure   	400 "Bad Request"
// @Failure     404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Router     	/root-list [get]
func (h *layerZeroListsHandler) GetRootNodesList(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetRootNodesListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	if err := h.tryToUpdateLists(); err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to update layer zero lists")
		return
	}

	rootList, err := h.getRootList(request.Status)
	if err != nil {
		if errors.Is(err, notFoundError) {
			ape.RenderErr(w, problems.NotFound())
			return
		}

		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get root list from DB")
		return
	}

	ape.Render(w, rootList)
}

func (h *layerZeroListsHandler) getRootList(status string) (resources.RootList, error) {
	list, err := h.storage.GetRootList(status)
	if err != nil {
		h.log.WithError(err).Errorf("failed to get %s root list", status)
		return resources.RootList{}, err
	}
	if list.Hash == "" {
		return resources.RootList{}, notFoundError
	}

	var percentage float32
	if status != "active" {
		currentRoots, err := h.storage.GetCurrentRoots()
		if err != nil {
			h.log.WithError(err).Error("failed to get current roots from DB")
			return resources.RootList{}, err
		}
		percentage = getRootsAcceptedPercentage(currentRoots, list.Signers)
	}

	return resources.PopulateRootList(list, percentage), nil
}

func (h *layerZeroListsHandler) govRootListToData(govList governance.RootList, status string) data.RootNodeList {
	var list data.RootNodeList

	list.Hash = govList.Hash.String()
	list.Type = data.ListTypeRoot
	list.Timestamp = time.Unix(int64(govList.Timestamp), 0)
	list.Status = status

	list.RootList = make([]data.NodeAccountPair, 0, len(govList.Nodes))
	for _, root := range govList.Nodes {
		list.RootList = append(list.RootList, data.NodeAccountPair{
			Alias:       "",
			MainAccount: sql.NullString{String: root.String(), Valid: true},
		})
	}

	list.Signers = make([]data.NodeAccountPair, 0, len(govList.Signers))
	for _, signer := range govList.Signers {
		list.Signers = append(list.Signers, data.NodeAccountPair{
			Alias:       signer.String(),
			MainAccount: sql.NullString{},
		})
	}

	h.resolveSignersAliases(list.Signers)
	h.resolveNodesAliases(list.RootList)

	return list
}
