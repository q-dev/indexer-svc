-- +migrate Up
CREATE TABLE delegations
(
    id          BIGSERIAL  NOT NULL PRIMARY KEY,
    delegator   CHAR(42)   NOT NULL,
    validator   CHAR(42)   NOT NULL,
    UNIQUE (delegator, validator)
);

CREATE TABLE delegation_block
(
    number BIGINT
);

-- +migrate Down
DROP TABLE delegations;
DROP TABLE delegation_block;
