package postgres

import (
	"time"

	"github.com/Masterminds/squirrel"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// QTokenHolderVotesStorage implements data.QTokenHolderVotesStorage.
type QTokenHolderVotesStorage struct {
	*VotingEventsStorage
}

// SelectAggregated q token holder aggregated votes with params.
func (s *QTokenHolderVotesStorage) SelectAggregated(params data.SelectVotingEventsParams) (data.CountVotingEvents, error) {
	queryUnion := squirrel.Select("'total' as account_address, voting_type, COUNT(*) as amount").From("constitution_votings").GroupBy("voting_type")

	err := UnionAll(&queryUnion, squirrel.Select("'total' as account_address, voting_type, COUNT(*) as amount").From("general_update_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	err = UnionAll(&queryUnion, squirrel.Select("'total' as account_address, voting_type, COUNT(*) as amount").From("roots_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	err = UnionAll(&queryUnion, squirrel.Select("'total' as account_address, voting_type, COUNT(*) as amount").From("rootnodes_slashing_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	err = UnionAll(&queryUnion, squirrel.Select("'total' as account_address, voting_type, COUNT(*) as amount").From("epdr_membership_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	err = UnionAll(&queryUnion, squirrel.Select("'total' as account_address, voting_type, COUNT(*) as amount").From("eprs_membership_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	err = UnionAll(&queryUnion, squirrel.Select("'total' as account_address, voting_type, COUNT(*) as amount").From("epqfi_membership_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	return s.selectAggregated(params, queryUnion)
}
