package api

import (
	"net/http"
	"net/url"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

type paymentsHandler struct {
	log     *logan.Entry
	storage data.Storage
}

type getPaymentsQuery struct {
	Counterparty *string `schema:"filter[counterparty],omitempty"`
	Sender       *string `schema:"filter[sender],omitempty"`
	Recipient    *string `schema:"filter[recipient],omitempty"`

	Token    *string `schema:"filter[token],omitempty"`
	IsNative *string `schema:"filter[native],omitempty"`

	Sort   string  `schema:"sort"`
	Cursor *string `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	Includes *string `schema:"include"`
}

func (q *getPaymentsQuery) Validate() error {
	return validation.Errors{
		"filter[counterparty]": func() error {
			if q.Counterparty != nil && (q.Sender != nil || q.Recipient != nil) {
				return errors.New("filter[counterparty] can not be used with filter[sender] or filter[recipient]")
			}

			return isAddress(q.Counterparty)
		}(),
		"filter[sender]":    isAddress(q.Sender),
		"filter[recipient]": isAddress(q.Recipient),
		"filter[token]":     isAddress(q.Token),
		"filter[native]":    validation.Validate(q.IsNative, validation.In("true", "false")),
		"include":           validation.Validate(q.Includes, validation.In("transaction")),
		"sort":              validation.Validate(q.Sort, validation.In("timestamp", "-timestamp")),
		"page[limit]":       validation.Validate(int64(q.Limit), validation.Max(100)),
	}.Filter()
}

// @Summary   	Get payments
// @Description Returns history of payments.
// @ID       	payments
// @Accept    	json
// @Produce    	json
// @Param     	filter[counterparty] query string false "Returns transactions either where a given address is either sender or recipient. Can not be used with `filter[sender]`."
// @Param     	filter[sender] query string false "Filter by sender"
// @Param     	filter[recipient] query string false "Filter by recipient"
// @Param 		filter[token] query string false "Filter by token"
// @Param 		filter[native] query string false "Filter foreign / native payments."
// @Param 		includes query string false "Include transaction. Supported value: `transaction`."
// @Param     	sort query string false "Sorting order. Supported values: `block` (sort by block number, asc), `-block` (sort by block number, desc). Default - `-block`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `10`."
// @Success   	200 {object} resources.PaymentsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/payments [get]
func (h *paymentsHandler) getPayments(w http.ResponseWriter, r *http.Request) {
	query := getPaymentsQuery{
		Sort:  "-timestamp",
		Limit: 10,
	}

	if err := schema.NewDecoder().Decode(&query, r.URL.Query()); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	if err := query.Validate(); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	payments, err := h.storage.Payments().Select(&data.SelectPaymentsParams{
		Limit:  query.Limit,
		IsDesc: strings.HasPrefix(query.Sort, "-"),
		Cursor: query.Cursor,

		Recipient:    transformAddress(query.Recipient),
		Sender:       transformAddress(query.Sender),
		Counterparty: transformAddress(query.Counterparty),

		Token:    transformAddress(query.Token),
		IsNative: transformBool(query.IsNative),
	})
	if err != nil {
		h.log.WithError(err).Error("failed to query payments history")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if query.Includes != nil && *query.Includes == "transaction" {
		for i := range payments {
			tx, err := h.storage.Transactions().Get(payments[i].TxHash)
			if err != nil {
				h.log.WithError(err).Error("failed to get tx by hash")
				ape.RenderErr(w, problems.InternalError())
				return
			}

			payments[i].Tx = tx
		}
	}

	resp := resources.PopulatePaymentsList(payments)
	resp.Links = buildPaymentsLinks(payments, *r.URL, query)
	ape.Render(w, resp)
}

// todo: avoid duplication
func buildPaymentsLinks(payments []data.Payment, endpoint url.URL, query getPaymentsQuery) *resources.Links {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(query, params); err != nil {
		panic(errors.Wrap(err, "failed to encode query"))
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(payments) == 0 {
		return links
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", payments[len(payments)-1].TxHash)
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", payments[0].TxHash)
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)
	return links
}
