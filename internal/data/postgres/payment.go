package postgres

import (
	"database/sql"
	"fmt"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type PaymentStorage struct {
	db *pgdb.DB
}

func (s *PaymentStorage) Insert(payments ...data.Payment) error {
	if len(payments) == 0 {
		return nil
	}

	query := squirrel.Insert("payments").Columns(
		"tx_hash",
		"sender",
		"recipient",
		"token",
		"value",
		"timestamp",
	)

	for _, payment := range payments {
		query = query.Values(
			payment.TxHash,
			payment.Sender,
			payment.Recipient,
			payment.Token,
			payment.Value,
			payment.Timestamp,
		)
	}

	return errors.Wrap(s.db.Exec(query), "failed to insert payments")
}

func (s *PaymentStorage) Select(params *data.SelectPaymentsParams) ([]data.Payment, error) {
	query := squirrel.Select("payments.*").From("payments")

	if params.Counterparty != nil {
		query = query.Where(
			"(payments.sender = ? OR payments.recipient = ?)",
			*params.Counterparty,
			*params.Counterparty,
		)
	}

	if params.Sender != nil {
		query = query.Where("(payments.sender = ?)", *params.Sender)
	}

	if params.Recipient != nil {
		query = query.Where("(payments.recipient = ?)", *params.Recipient)
	}

	order, sign := "DESC", "<"
	if !params.IsDesc {
		order, sign = "ASC", ">"
	}
	query = query.OrderBy(fmt.Sprintf("payments.timestamp %s, payments.tx_hash %s", order, order)).
		Limit(params.Limit)

	if params.Cursor != nil {
		cursor, err := s.Get(*params.Cursor)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get tx at cursor")
		}

		if cursor == nil {
			return nil, nil
		}

		// todo: there is a simpler way to do this, don't remember it now
		query = query.Where(
			fmt.Sprintf("((payments.timestamp, payments.tx_hash) %s (?, ?))", sign),
			cursor.Timestamp,
			cursor.TxHash,
		)
	}

	if params.IsNative != nil && *params.IsNative {
		query = query.Where("(payments.token IS NULL)")
	}

	if params.IsNative != nil && !*params.IsNative {
		query = query.Where("(payments.token IS NOT NULL)")
	}

	if params.Token != nil {
		query = query.Where("(payments.token = ?)", *params.Token)
	}

	var payments []data.Payment
	if err := s.db.Select(&payments, query); err != nil {
		return nil, errors.Wrap(err, "failed to select payments")
	}

	return payments, nil
}

func (s *PaymentStorage) Get(txHash string) (*data.Payment, error) {
	query := squirrel.Select("*").From("payments").
		Where("tx_hash = ?", txHash)

	var payment data.Payment
	switch err := s.db.Get(&payment, query); err {
	case nil:
		return &payment, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get payment by hash")
	}

}
