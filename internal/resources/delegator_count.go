package resources

import (
	"strconv"
)

type DelegatorCount struct {
	Data DelegatorCountData `json:"data"`
}

type DelegatorCountData struct {
	ID         string              `json:"id"`
	Type       string              `json:"type"`
	Attributes DelegatorCountAttrs `json:"attributes"`
}

type DelegatorCountAttrs struct {
	Value string `json:"value"`
}

func NewDelegatorCount(count uint64) DelegatorCount {
	return DelegatorCount{
		Data: DelegatorCountData{
			ID:         "id",
			Type:       "delegator_count",
			Attributes: DelegatorCountAttrs{Value: strconv.FormatUint(count, 10)},
		},
	}
}
