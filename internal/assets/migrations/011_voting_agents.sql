-- +migrate Up
CREATE TABLE voting_agents
(
    id              BIGSERIAL PRIMARY KEY,
    address         CHAR(42)  NOT NULL,
    voting_agent    CHAR(42)  NOT NULL,
    start_block     BIGINT    NOT NULL,
    end_block       BIGINT    DEFAULT NULL
);

-- +migrate Down
DROP TABLE voting_agents;
