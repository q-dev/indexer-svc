package vars

import (
	"math/big"

	"gitlab.com/q-dev/q-client/common"
)

var (
	// bytes32(uint256(keccak256('eip1967.proxy.implementation')) - 1)
	ProxyImplementationKey = common.HexToHash("0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc")
	// crypto.Keccak256([]byte("ROOT_NODE_OPERATION")
	RNOperationPurpose, _ = new(big.Int).SetString("33a9d3006f267399569cda2996bb19776f92c98b990053176d19c710ed251a5d", 16)
)
