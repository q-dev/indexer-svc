package postgres

import (
	"database/sql"
	"fmt"
	"math"
	"strings"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

const EpochLength = 101

// RootNodeApprovalsStorage implements data.RootNodeApproval.
type RootNodeApprovalsStorage struct {
	db *pgdb.DB
}

func (s *RootNodeApprovalsStorage) Insert(approvals ...data.RootNodeApproval) error {
	if len(approvals) == 0 {
		return nil
	}

	query := squirrel.Insert("approvals").Columns(
		"signature",
		"block",
		"signer",
		"alias",
	)

	for _, approval := range approvals {
		query = query.Values(
			approval.Signature,
			approval.Block,
			approval.Signer,
			approval.Alias,
		)
	}

	query = query.Suffix("ON CONFLICT (signature) DO NOTHING")

	return errors.Wrap(s.db.Exec(query), "failed to insert root node approval")
}

func (s *RootNodeApprovalsStorage) GetLatest() (approval *data.RootNodeApproval, err error) {
	return s.queryBlock(squirrel.Select("approvals.*").From("approvals").
		Where("block = (SELECT max(block) from approvals)"))
}

// Select root node approvals with params.
// SELECT
//
// COALESCE(cycles.first, startCycle) AS first,
// COALESCE(cycles.last, endCycle) AS last,
// COALESCE(cycles.signer, nap.main_account) AS signer,
// COALESCE(cycles.alias, nap.node) AS alias
//
// FROM layer_zero_lists lzl
// JOIN root_nodes rn ON lzl.id = rn.list_id
// JOIN node_account_pairs nap ON rn.node = nap.node
// LEFT JOIN (
//
//	SELECT
//	    MIN(block) as first,
//	    MAX(block) as last,
//	    signer,
//	    alias
//	FROM approvals
//	WHERE block BETWEEN startCycle + 1 AND endCycle
//	GROUP BY signer, alias
//
// ) cycles ON LOWER(nap.main_account) = cycles.signer
// WHERE
//
//	nap.active IS TRUE,
//	lzl.type = 'root-list' AND
//	lzl.status = 'active' AND (
//	lzl.timestamp BETWEEN (now() - (endCycle * 5) * interval '1 second') AND (now() - (startCycle * 5) * interval '1 second')
//	OR
//	lzl.timestamp = (SELECT MAX(timestamp) FROM layer_zero_lists WHERE type = 'root-list' AND status = 'active'))
//
// ORDER BY signer DESC
// LIMIT 100;
func (s *RootNodeApprovalsStorage) Select(params data.SelectRootNodeApprovalParams, highestBlock uint64) (data.RootNodeBreakdown, error) {
	queryMaxId := squirrel.Select("max(block)").From("approvals")
	var latestTransitionBlock sql.NullInt64
	err := s.db.Get(&latestTransitionBlock, queryMaxId)
	if err != nil {
		return data.RootNodeBreakdown{}, errors.Wrap(err, "failed to get max block")
	}
	if !latestTransitionBlock.Valid {
		return data.RootNodeBreakdown{}, nil
	}

	endCycle := uint64(latestTransitionBlock.Int64)

	if highestBlock-uint64(latestTransitionBlock.Int64) < params.BlocksDelay {
		endCycle = endCycle - params.BlocksDelay
	}

	blocksCount := (params.Cycles - 1) * EpochLength
	startCycle := endCycle - blocksCount
	if endCycle < blocksCount {
		startCycle = 0
	}

	queryCycles := squirrel.
		Select(
			"MIN(block) AS first",
			"MAX(block) AS last",
			"signer",
			"COALESCE(alias, signer) AS alias",
			"COUNT(*) AS approvals",
		).
		From("approvals").
		Where(squirrel.Expr("block BETWEEN ? AND ?", startCycle, endCycle)).
		GroupBy("signer, COALESCE(alias, signer)")

	queryCyclesSub, queryCyclesArgs, _ := queryCycles.ToSql()

	query := squirrel.
		Select(
			fmt.Sprintf("COALESCE(cycles.first, %d) AS first", startCycle),
			fmt.Sprintf("COALESCE(cycles.last, %d) AS last", endCycle),
			"COALESCE(cycles.signer, nap.main_account) AS signer",
			"COALESCE(cycles.alias, nap.node) AS alias",
			"COALESCE(cycles.approvals, 0) AS approvals",
		).
		From("layer_zero_lists lzl").
		Join("root_nodes rn on lzl.id = rn.list_id").
		Join("node_account_pairs nap on rn.node = nap.node").
		LeftJoin(fmt.Sprintf("(%s) cycles ON LOWER(nap.main_account) = cycles.signer", queryCyclesSub), queryCyclesArgs...).
		Where(squirrel.And{
			squirrel.Eq{
				"nap.active": true,
				"lzl.type":   "root-list",
				"lzl.status": "active",
			},
			squirrel.Or{
				squirrel.Expr("lzl.timestamp BETWEEN (now() - (? * 5) * interval '1 second') AND (now() - (? * 5) * interval '1 second')", endCycle, startCycle),
				squirrel.Expr("lzl.timestamp = (SELECT MAX(timestamp) FROM layer_zero_lists WHERE type = 'root-list' AND status = 'active')"),
			},
		})

	order, sign := "ASC", ">"
	if params.IsDesc {
		order, sign = "DESC", "<"
	}

	if params.RootAddress != nil {
		query = query.Where("cycles.signer = ?", strings.ToLower(*params.RootAddress))
	}

	if params.Cursor != nil {
		query = query.Where(
			fmt.Sprintf("cycles.signer %s ?", sign),
			strings.ToLower(*params.Cursor),
		)
	}

	query = query.OrderBy(fmt.Sprintf("signer %s", order))

	if params.Limit != 0 {
		query = query.Limit(params.Limit)
	}

	var approvals []data.FirstAndLastBlocksByAccount
	if err := s.db.Select(&approvals, query); err != nil {
		return data.RootNodeBreakdown{}, errors.Wrap(err, "failed to select root node approvals")
	}

	rootNodeBreakdown := s.calculateApprovals(approvals)
	if err != nil {
		return data.RootNodeBreakdown{}, errors.Wrap(err, "failed to calculate root node approvals")
	}

	return rootNodeBreakdown, nil
}

func (s *RootNodeApprovalsStorage) calculateApprovals(blocks []data.FirstAndLastBlocksByAccount) data.RootNodeBreakdown {
	if len(blocks) == 0 {
		return data.RootNodeBreakdown{}
	}

	rootNodeBreakdown := data.RootNodeBreakdown{}
	firstBlock := uint64(math.MaxUint64)
	lastBlock := uint64(0)

	mapped := map[string][]data.ObservedApprovals{}
	for _, elem := range blocks {
		if elem.First < firstBlock {
			firstBlock = elem.First
		}
		if elem.Last > lastBlock {
			lastBlock = elem.Last
		}
	}

	rootNodeBreakdown.FirstTransitionBlock = firstBlock
	rootNodeBreakdown.LastTransitionBlock = lastBlock
	dueBlocks := (lastBlock-firstBlock)/EpochLength + 1

	var order []string
	for _, elem := range blocks {
		offlineBlocks := dueBlocks - elem.Approvals

		if offlineBlocks == dueBlocks {
			elem.Last = 0
		}

		order = append(order, elem.Account)
		mapped[elem.Account] = append(mapped[elem.Account], data.ObservedApprovals{
			Alias: elem.Alias,
			FirstObservedApproval: data.ObservedApproval{
				Block:  elem.First,
				Cycles: dueBlocks,
			},
			LastObservedApproval: data.ObservedApproval{
				Block:  elem.Last,
				Cycles: offlineBlocks,
			},
		})
	}

	for _, address := range order {
		rootNodeBreakdown.ByAddress = append(rootNodeBreakdown.ByAddress, data.ByAddress{
			Account:           address,
			ObservedApprovals: mapped[address],
		})
	}

	return rootNodeBreakdown
}

func (s *RootNodeApprovalsStorage) queryBlock(query squirrel.SelectBuilder) (*data.RootNodeApproval, error) {
	var rootNodeApproval data.RootNodeApproval
	switch err := s.db.Get(&rootNodeApproval, query); err {
	case nil:
		return &rootNodeApproval, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get root node approval")
	}
}
