package data

import "database/sql"

type Voting struct {
	ProposalId    uint64 `json:"proposal_id" db:"proposal_id"`
	VotingType    string `json:"voting_type" db:"voting_type"`
	VotingStatus  uint8  `json:"voting_status" db:"voting_status"`
	VotingEndTime uint64 `json:"voting_end_time" db:"voting_end_time"`
}

// VotingsStorage stores votings.
type VotingStorage interface {
	Select(params *SelectVotingsParams, currentRootNodesCount uint64) ([]GeneralVoting, error)
}

type SelectVotingsParams struct {
	IsDesc bool
	Cursor *uint64
	Limit  uint64

	Type     string
	Status   []string
	Finished *bool
}

type GeneralVoting struct {
	ProposalId      uint64 `db:"proposal_id"`
	VotingType      string `db:"voting_type"`
	VotingStatus    uint8  `db:"voting_status"`
	VotingStartTime uint64 `db:"voting_start_time"`
	VotingEndTime   uint64 `db:"voting_end_time"`
	RootNodesNumber uint64 `db:"root_nodes_number"`

	CurrentMajority  sql.NullString `db:"current_majority"`
	RequiredMajority sql.NullString `db:"required_majority"`
	CurrentQuorum    sql.NullString `db:"current_quorum"`
	RequiredQuorum   sql.NullString `db:"required_quorum"`
	VotesNumber      sql.NullInt64  `db:"votes_number"`
	VetosNumber      sql.NullInt64  `db:"vetos_number"`
	VetoEndTime      sql.NullInt64  `db:"veto_end_time"`

	VotingSubType sql.NullByte `db:"voting_subtype"`

	Candidate   sql.NullString `db:"candidate"`
	ReplaceDest sql.NullString `db:"replace_dest"`
}

type BaseVoting struct {
	ProposalId      uint64 `db:"proposal_id"`
	VotingType      string `db:"voting_type"`
	VotingStatus    uint8  `db:"voting_status"`
	VotingStartTime uint64 `db:"voting_start_time"`
	CurrentQuorum   string `db:"current_quorum"`
	RequiredQuorum  string `db:"required_quorum"`
	RootNodesNumber uint64 `db:"root_nodes_number"`
	VetosNumber     uint64 `db:"vetos_number"` // is not used in each voting, so we simply skip it during insert to db
	VotingEndTime   uint64 `db:"voting_end_time"`
	VetoEndTime     uint64 `db:"veto_end_time"`
}

type ContractRegistryVoting struct {
	ProposalId       uint64 `db:"proposal_id"`
	VotingType       string `db:"voting_type"`
	VotingStatus     uint8  `db:"voting_status"`
	VotingStartTime  uint64 `db:"voting_start_time"`
	CurrentMajority  string `db:"current_majority"`
	RequiredMajority string `db:"required_majority"`
	RootNodesNumber  uint64 `db:"root_nodes_number"`
	VotesNumber      uint64 `db:"votes_number"`
	VotingEndTime    uint64 `db:"voting_end_time"`
}
