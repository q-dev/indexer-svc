-- +migrate Up
CREATE TABLE rn_membership_periods
(
    period_id      BIGSERIAL   PRIMARY KEY,
    root_address   CHAR(42)    NOT NULL,
    start_block    BIGINT      NOT NULL,
    end_block      BIGINT      DEFAULT NULL,
    start_time     TIMESTAMP   NOT NULL,
    end_time       TIMESTAMP   NOT NULL
);

-- +migrate Down
DROP TABLE rn_membership_periods;
