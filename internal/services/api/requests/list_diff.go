package requests

import (
	"net/http"

	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
)

type GetDiffListRequest struct {
	NameA string `url:"nameA"`
	NameB string `url:"nameB"`

	Type string
}

func NewGetDiffListRequest(r *http.Request) (GetDiffListRequest, error) {
	var request GetDiffListRequest

	err := schema.NewDecoder().Decode(&request, r.URL.Query())
	if err != nil {
		return request, err
	}

	request.Type = chi.URLParam(r, "type")

	return request, request.Validate()
}

func (r *GetDiffListRequest) Validate() error {
	return validation.Errors{
		"nameA":   validation.Validate(r.NameA, validation.Required, validation.In(listActive, listProposed)),
		"nameB":   validation.Validate(r.NameB, validation.Required, validation.In(listActive, listProposed)),
		"/{type}": validation.Validate(r.Type, validation.Required, validation.In(ListRoot, ListExclusion)),
	}.Filter()
}
