package data

// EpdrMembershipVoting voting.
type EpdrMembershipVoting BaseVoting

// EpdrMembershipVotingStorage stores epdr membership votings.
type EpdrMembershipVotingStorage interface {
	Insert(votings ...EpdrMembershipVoting) error
	GetLatest() (*EpdrMembershipVoting, error)

	GetEarliestUnfinishedVotings() ([]EpdrMembershipVoting, error)
	Update(voting *EpdrMembershipVoting) error
}
