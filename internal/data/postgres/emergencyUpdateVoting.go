package postgres

import (
	"database/sql"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// EmergencyUpdateVotingStorage implements data.EmergencyUpdateVotingStorage.
type EmergencyUpdateVotingStorage struct {
	db *pgdb.DB
}

var (
	eUVotingColumns = []string{
		"proposal_id",
		"voting_type",
		"voting_status",
		"current_quorum",
		"required_quorum",
		"cast(extract(epoch from voting_end_time) as integer) as voting_end_time",
		"cast(extract(epoch from voting_start_time) as integer) as voting_start_time",
		"cast(extract(epoch from veto_end_time) as integer) as veto_end_time",
	}
)

func (s *EmergencyUpdateVotingStorage) Insert(votings ...data.EmergencyUpdateVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert("emergency_update_votings").Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_quorum",
		"required_quorum",
		"root_nodes_number",
		"voting_end_time",
		"veto_end_time",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentQuorum,
			voting.RequiredQuorum,
			voting.RootNodesNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
			time.Unix(int64(voting.VetoEndTime), 0),
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_quorum=EXCLUDED.current_quorum," +
		"root_nodes_number=EXCLUDED.root_nodes_number",
	)

	return errors.Wrap(s.db.Exec(query), "failed to insert emergency update voting")
}

// GetLatest block.
func (s *EmergencyUpdateVotingStorage) GetLatest() (*data.EmergencyUpdateVoting, error) {
	return s.queryBlock(squirrel.Select(eUVotingColumns...).From("emergency_update_votings").
		Where("proposal_id = (SELECT max(proposal_id) from emergency_update_votings)"))
}

func (s *EmergencyUpdateVotingStorage) GetEarliestUnfinishedVotings() ([]data.EmergencyUpdateVoting, error) {
	var votings []data.EmergencyUpdateVoting
	query := squirrel.Select(eUVotingColumns...).From("emergency_update_votings").
		Where("voting_status IN (1, 3, 4)").OrderBy("proposal_id ASC")
	if err := s.db.Select(&votings, query); err != nil {
		return nil, errors.Wrap(err, "failed to select emergency update votings")
	}
	return votings, nil
}

func (s *EmergencyUpdateVotingStorage) Update(voting *data.EmergencyUpdateVoting) error {
	query := squirrel.Update("emergency_update_votings").SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_quorum":    voting.CurrentQuorum,
		"root_nodes_number": voting.RootNodesNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query), "failed to update emergency update voting")
}

func (s *EmergencyUpdateVotingStorage) queryBlock(query squirrel.SelectBuilder) (*data.EmergencyUpdateVoting, error) {
	var voting data.EmergencyUpdateVoting
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get emergency update voting")
	}
}
