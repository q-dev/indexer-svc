package postgres

import (
	"database/sql"
	"fmt"
	"strings"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// VotingEventsStorage implements data.VotingEvent.
type VotingEventsStorage struct {
	db       *pgdb.DB
	database string
}

func (s *VotingEventsStorage) Insert(votingEvents ...data.VotingEvent) error {
	if len(votingEvents) == 0 {
		return nil
	}

	query := squirrel.Insert(s.database).
		Columns("voting_type", "account_address", "proposal_id", "block_id")
	for _, event := range votingEvents {
		query = query.Values(event.VotingType, event.AccountAddress, event.ProposalId, event.BlockId)
	}

	return errors.Wrap(s.db.Exec(query), "failed to insert root votes")
}

func (s *VotingEventsStorage) GetByLatestByType(votingType string) (*data.VotingEvent, error) {
	return s.queryBlock(squirrel.Select("*").From(s.database).
		Where("voting_type = ?", votingType).OrderBy("id DESC"))
}

func (s *VotingEventsStorage) selectEventsByAddress(eventsByAddress *[]data.VotingEventByAddress, query squirrel.SelectBuilder) error {
	if err := s.db.Select(eventsByAddress, query); err != nil {
		return errors.Wrap(err, "failed to select aggregated q token holder votes")
	}
	return nil
}

// Select current onchain root nodes votes with params.
func (s *VotingEventsStorage) Select(params data.SelectVotingEventsParams, addresses []string) ([]data.VotingEvent, error) {
	query := squirrel.Select("*").From(s.database)

	order, sign := "ASC", ">"
	if params.IsDesc {
		order, sign = "DESC", "<"
	}

	query = query.OrderBy(fmt.Sprintf("id %s", order)).
		Limit(params.Limit)

	if params.Cursor != nil {
		query = query.Where(
			fmt.Sprintf("id %s ?", sign),
			*params.Cursor,
		)
	}

	query = query.Where("(account_address IN (" + "'" + strings.Join(addresses, "','") + "'" + "))")

	if params.StartBlock != 0 {
		query = query.Where("block_id > ?)", params.StartBlock)
	}

	if params.AccountAddress != "" {
		query = query.Where("(account_address = ?)", params.AccountAddress)
	}

	var votingEvents []data.VotingEvent
	if err := s.db.Select(&votingEvents, query); err != nil {
		return nil, errors.Wrap(err, "failed to select q token holder votes")
	}

	return votingEvents, nil
}

// selectAggregated current root node aggregated votes with params.
func (s *VotingEventsStorage) selectAggregated(params data.SelectVotingEventsParams, totalQuery squirrel.SelectBuilder) (data.CountVotingEvents, error) {
	query := squirrel.Select("account_address, voting_type, count(*) as amount").From(s.database)

	order, sign := "ASC", ">"
	if params.IsDesc {
		order, sign = "DESC", "<"
	}

	if params.Cursor != nil {
		query = query.Where(
			fmt.Sprintf("account_address %s ?", sign),
			*params.Cursor,
		)
	}

	query = query.OrderBy(fmt.Sprintf("account_address %s", order)).
		Limit(params.Limit)

	if params.StartBlock != 0 {
		query = query.Where("(block_id > ?)", params.StartBlock)
	}

	if params.AccountAddress != "" {
		query = query.Where("(account_address = ?)", params.AccountAddress)
	} else if len(params.AddressList) > 0 {
		query = query.Where(squirrel.Eq{"account_address": params.AddressList})
	}

	query = query.GroupBy("account_address, voting_type")

	var eventsByAddress []data.VotingEventByAddress
	if err := s.selectEventsByAddress(&eventsByAddress, query); err != nil {
		return data.CountVotingEvents{}, errors.Wrap(err, "failed to select aggregated q token holder votes")
	}

	countEventsByAddress := make(map[string]map[string]uint64)
	var ordered []string
	for _, count := range eventsByAddress {
		if countEventsByAddress[count.AccountAddress] == nil {
			countEventsByAddress[count.AccountAddress] = make(map[string]uint64)
			countEventsByAddress[count.AccountAddress]["all"] = 0
			ordered = append(ordered, count.AccountAddress)
		}
		countEventsByAddress[count.AccountAddress][count.VotingType] = count.Amount
		if count.VotingType != "delegatedVotes" {
			countEventsByAddress[count.AccountAddress]["all"] += count.Amount
		}
	}

	var total []data.VotingEventByAddress
	if err := s.selectEventsByAddress(&total, totalQuery); err != nil {
		return data.CountVotingEvents{}, errors.Wrap(err, "failed to select amount from votings")
	}

	totalCounts := make(map[string]uint64)
	for _, t := range total {
		totalCounts[t.VotingType] = t.Amount
		totalCounts["all"] += t.Amount
	}

	var countsByAddress []data.CountEventsByAddress
	for _, address := range ordered {
		countsByAddress = append(countsByAddress, data.CountEventsByAddress{
			AccountAddress: address,
			Counts:         countEventsByAddress[address],
		})
	}

	var counts data.CountVotingEvents
	counts.Total = totalCounts
	counts.ByAddress = countsByAddress
	return counts, nil
}

func (s *VotingEventsStorage) queryBlock(query squirrel.SelectBuilder) (*data.VotingEvent, error) {
	var vote data.VotingEvent
	switch err := s.db.Get(&vote, query); err {
	case nil:
		return &vote, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get root membership period")
	}
}

// Erase all history starting fromID inclusively.
func (s *VotingEventsStorage) Erase(fromID uint64) error {
	query := squirrel.Delete("q_token_holder_votes").
		Where("id >= ?", fromID)

	return errors.Wrap(s.db.Exec(query), "failed to erase root votes")
}

func UnionAll(mainQuery *squirrel.SelectBuilder, query squirrel.SelectBuilder) error {
	sql, args, err := query.ToSql()
	if err != nil {
		return err
	}
	*mainQuery = mainQuery.Suffix("UNION ALL "+sql, args...)
	return nil
}
