package postgres

import (
	"time"

	"github.com/Masterminds/squirrel"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// RootNodeProposalsStorage implements data.VoteStorage.
type RootNodeProposalsStorage struct {
	*VotingEventsStorage
}

// SelectAggregated root node aggregated proposals with params.
func (s *RootNodeProposalsStorage) SelectAggregated(params data.SelectVotingEventsParams) (data.CountVotingEvents, error) {
	queryUnion := squirrel.Select("voting_type, COUNT(*) as amount").From("emergency_update_votings").GroupBy("voting_type")

	err := UnionAll(&queryUnion, squirrel.Select("voting_type, COUNT(*) as amount").From("validators_slashing_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	err = UnionAll(&queryUnion, squirrel.Select("voting_type, COUNT(*) as amount").From("rootnodes_slashing_votings").
		Where("voting_end_time > ?", time.Unix(int64(params.StartBlockTimestamp), 0)).GroupBy("voting_type"))
	if err != nil {
		return data.CountVotingEvents{}, err
	}

	return s.selectAggregated(params, queryUnion)
}
