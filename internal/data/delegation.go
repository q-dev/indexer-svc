package data

type Delegation struct {
	ID        uint64 `db:"id"`
	Delegator string `db:"delegator"`
	Validator string `db:"validator"`
}

type DelegationsStorage interface {
	Insert(delegations ...Delegation) error
	Delete(delegations ...Delegation) error
	CountDelegators() (uint64, error)
}
