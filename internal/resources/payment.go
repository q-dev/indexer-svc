package resources

import (
	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type PaymentsList struct {
	Data     []Payment   `json:"data"`
	Links    *Links      `json:"links,omitempty"`
	Included RelationMap `json:"included,omitempty"`
}

type Payment struct {
	ID         string       `json:"id"`
	Type       string       `json:"type"`
	Attributes PaymentAttrs `json:"attributes"`

	Relationships *PaymentRelationships `json:"relationships,omitempty"`
}

type PaymentRelationships struct {
	Transaction *Relationship `json:"transaction,omitempty"`
}

type PaymentAttrs struct {
	Sender    string `json:"sender"`
	Recipient string `json:"recipient"`
	Value     string `json:"value"`

	Token *string `json:"token"`

	Timestamp int64 `json:"timestamp"`
}

// PopulatePaymentsList creates a tx list response.
func PopulatePaymentsList(payments []data.Payment) *PaymentsList {
	list := &PaymentsList{
		Data:     make([]Payment, len(payments)),
		Included: make(RelationMap),
	}

	for i := range payments {
		list.Data[i] = newPayment(&payments[i])
		if tx := payments[i].Tx; tx != nil {
			list.Included[newTxKey(tx)] = newTransaction(tx)
		}
	}

	return list
}

func newPayment(payment *data.Payment) Payment {
	model := Payment{
		ID:   payment.TxHash,
		Type: "payments",
		Attributes: PaymentAttrs{
			Sender:    payment.Sender,
			Recipient: payment.Recipient,
			Value:     payment.Value.Unwrap().String(),
			Token:     payment.Token,
			Timestamp: payment.Timestamp,
		},
	}

	if tx := payment.Tx; tx != nil {
		model.Relationships = &PaymentRelationships{
			Transaction: &Relationship{
				Data: newTxKey(tx),
			},
		}
	}

	return model
}
