package data

// Block of txs.
type Block struct {
	Number       uint64 `db:"id"`
	Hash         string `db:"hash"`
	Parent       string `db:"parent"`
	Signer       string `db:"signer"`
	MainAccount  string `db:"mainaccount"`
	Timestamp    int64  `db:"timestamp"`
	InTurnSigner string `db:"inturn_signer"`
	Difficulty   int64  `db:"difficulty"`
}

// BlockStorage stores blocks.
type BlockStorage interface {
	Insert(block *Block) error
	GetByNumber(number uint64) (*Block, error)
	GetLatest() (*Block, error)

	Select(params *SelectBlocksParams) ([]Block, error)

	// Erase all history starting fromID inclusively.
	Erase(fromID uint64) error
}

type SelectBlocksParams struct {
	IsDesc bool
	Cursor *uint64
	Limit  uint64

	From *uint64
	To   *uint64

	Signer       *string
	MainAccount  *string
	InTurnSigner *string
	AnySigner    *string
	OutOfTurn    bool
}
