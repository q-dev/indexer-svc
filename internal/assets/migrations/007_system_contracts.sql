-- +migrate Up

CREATE TABLE system_contracts (
    id             BIGSERIAL PRIMARY KEY,
    proxy          CHAR(42) UNIQUE,
    implementation CHAR(42) DEFAULT NULL,
    key            VARCHAR DEFAULT NULL
);

-- +migrate Down

DROP TABLE system_contracts;
