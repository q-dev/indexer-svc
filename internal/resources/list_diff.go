package resources

import "fmt"

type DiffEntry struct {
	Name string        `json:"name"`
	Diff []AccountPair `json:"diff"`
}

type ListDiffAttributes struct {
	Result []DiffEntry `json:"result"`
}

type ListDiffData struct {
	ID   string `json:"id"`
	Type string `json:"type"`

	Attributes ListDiffAttributes `json:"attributes"`
}

type ListDiff struct {
	Data ListDiffData `json:"data"`
}

func PopulateListDiff(listA, listB []AccountPair, listType, listAName, listBName string) ListDiff {
	return ListDiff{
		Data: ListDiffData{
			ID:   fmt.Sprintf("%s:%s:%s", listType, listAName, listBName),
			Type: fmt.Sprintf("diff-%s", listType),
			Attributes: ListDiffAttributes{
				[]DiffEntry{
					{
						Name: listAName,
						Diff: addressDiff(listA, listB),
					},
					{
						Name: listBName,
						Diff: addressDiff(listB, listA),
					},
				},
			},
		},
	}
}

func addressDiff(listA, listB []AccountPair) []AccountPair {
	diff := make([]AccountPair, 0)

	setB := make(map[string]struct{})
	for _, account := range listB {
		setB[account.MainAccount] = struct{}{}
	}

	for _, account := range listA {
		if _, ok := setB[account.MainAccount]; !ok {
			diff = append(diff, account)
		}
	}

	return diff
}
