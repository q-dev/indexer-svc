package postgres

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/kit/pgdb"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

const (
	layerZeroListsTableName   = "layer_zero_lists"
	nodeAccountPairsTableName = "node_account_pairs"
	nodeExclusionTableName    = "node_exclusion"
	listSignersTableName      = "list_signers"
	rootNodesTableName        = "root_nodes"
)

var (
	excludedListColumns = []string{
		"lzl.hash",
		"lzl.timestamp",
		"lzl.type",
		"lzl.status",
		"(SELECT JSONB_AGG((sub.json_build_object_text)::jsonb) FROM (SELECT DISTINCT (json_build_object('main_account', nap_e.main_account))::text as json_build_object_text FROM list_signers ls JOIN node_account_pairs nap_e ON ls.signer = nap_e.node WHERE lzl.id = ls.list_id AND nap_e.active = true) sub) AS signers",
		"(SELECT JSONB_AGG((sub.json_build_object_text)::jsonb) FROM (SELECT DISTINCT (json_build_object('main_account', nap_e.main_account, 'start_block', ne.start_block, 'end_block', ne.end_block, 'list_hash', lzl.hash))::text as json_build_object_text FROM node_exclusion ne JOIN node_account_pairs nap_e ON ne.node = nap_e.node WHERE lzl.id = ne.list_id AND nap_e.active = true) sub) AS exclusions",
	}

	rootNodeListColumns = []string{
		"lzl.hash",
		"lzl.timestamp",
		"lzl.type",
		"lzl.status",
		"(SELECT JSONB_AGG((sub.json_build_object_text)::jsonb) FROM (SELECT DISTINCT (json_build_object('main_account', nap_e.main_account))::text as json_build_object_text FROM list_signers ls JOIN node_account_pairs nap_e ON ls.signer = nap_e.node WHERE lzl.id = ls.list_id AND nap_e.active = true) sub) AS signers",
		"(SELECT JSONB_AGG((sub.json_build_object_text)::jsonb) FROM (SELECT DISTINCT (json_build_object('main_account', nap_e.main_account))::text as json_build_object_text FROM root_nodes rn JOIN node_account_pairs nap_e ON rn.node = nap_e.node WHERE lzl.id = rn.list_id AND nap_e.active = true) sub) AS roots",
	}
)

type layerZeroListQ struct {
	db *pgdb.DB
}

func NewLayerZeroListQ(db *pgdb.DB) data.LayerZeroListStorage {
	return &layerZeroListQ{
		db: db,
	}
}

type accountNodePair struct {
	Node        string  `json:"node"`
	MainAccount *string `json:"main_account"`
}

type accountNodePairs []accountNodePair

type exclusion struct {
	accountNodePair
	StartBlock uint64 `json:"start_block"`
	EndBlock   uint64 `json:"end_block"`
	ListHash   string `json:"list_hash"`
}
type exclusions []exclusion
type rawExclusionList struct {
	Hash       string           `db:"hash"`
	Timestamp  time.Time        `db:"timestamp"`
	Type       string           `db:"type"`
	Status     string           `db:"status"`
	Signers    accountNodePairs `db:"signers"`
	Exclusions exclusions       `db:"exclusions"`
}

type rawRootNodeList struct {
	Hash      string           `db:"hash"`
	Timestamp time.Time        `db:"timestamp"`
	Type      string           `db:"type"`
	Status    string           `db:"status"`
	Signers   accountNodePairs `db:"signers"`
	RootNodes accountNodePairs `db:"roots"`
}

func (q *layerZeroListQ) GetExclusionList(status string) (data.NodeExclusionList, error) {
	result := rawExclusionList{}

	query := sq.
		Select(excludedListColumns...).
		From("layer_zero_lists lzl").
		Join("list_signers ls ON lzl.id = ls.list_id").
		Join("node_exclusion ne ON lzl.id = ne.list_id").
		Join("node_account_pairs nap_s ON ls.signer = nap_s.node").
		Join("node_account_pairs nap_e ON ne.node = nap_e.node").
		Where(sq.Eq{
			"lzl.status":   status,
			"lzl.type":     data.ListTypeExclusion,
			"lzl.outdated": false,
		}).
		GroupBy("lzl.id", "lzl.hash", "lzl.timestamp", "lzl.type", "lzl.status").
		OrderBy("lzl.timestamp DESC").
		Limit(1)

	err := q.db.Get(&result, query)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return data.NodeExclusionList{}, nil
		}

		return data.NodeExclusionList{}, err
	}

	return result.toData()
}

func (q *layerZeroListQ) GetRootList(status string) (data.RootNodeList, error) {
	result := rawRootNodeList{}

	query := sq.
		Select(rootNodeListColumns...).
		From("layer_zero_lists lzl").
		Join("list_signers ls ON lzl.id = ls.list_id").
		Join("node_account_pairs nap_s ON ls.signer = nap_s.node").
		Where(sq.Eq{
			"lzl.status":   status,
			"lzl.type":     data.ListTypeRoot,
			"lzl.outdated": false,
		}).
		GroupBy("lzl.id", "lzl.hash", "lzl.timestamp", "lzl.type").
		OrderBy("lzl.timestamp DESC").
		Limit(1)

	err := q.db.Get(&result, query)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return data.RootNodeList{}, nil
		}

		return data.RootNodeList{}, err
	}

	return result.toData()
}

func (q *layerZeroListQ) GetCurrentRoots() ([]data.NodeAccountPair, error) {
	var result []data.NodeAccountPair

	query := sq.
		Select("rn.node node", "nap_e.main_account main_account").
		From("root_nodes rn").
		Join("node_account_pairs nap_e ON rn.node = nap_e.node").
		Where("rn.list_id = (SELECT id FROM layer_zero_lists WHERE type = 'root-list' AND status = 'active' ORDER BY timestamp DESC LIMIT 1)")

	err := q.db.Select(&result, query)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get current roots")
	}

	return result, nil
}

func (q *layerZeroListQ) SaveRootList(list data.RootNodeList) error {
	var queryBuilder strings.Builder
	var args []interface{}

	// List insert
	layerZeroListIns := sq.
		Insert(layerZeroListsTableName).
		Columns("hash", "timestamp", "type", "status").
		Values(list.LayerZeroList.Hash, list.LayerZeroList.Timestamp, data.ListTypeRoot, list.Status).
		Suffix("ON CONFLICT (hash, status) DO UPDATE SET hash = EXCLUDED.hash").
		Suffix("RETURNING id")

	var id uint64
	err := q.db.Get(&id, layerZeroListIns)
	if err != nil {
		return err
	}

	// Prepare list's signers insert
	listSignersIns := sq.
		Insert(listSignersTableName).
		Columns("list_id", "signer")
	for _, signer := range list.Signers {
		listSignersIns = listSignersIns.Values(id, signer.Alias)
	}
	listSignersIns = listSignersIns.
		Suffix("ON CONFLICT (list_id, signer) DO NOTHING").
		Prefix("WITH list_signers AS (").
		Suffix(")")

	// Prepare exclusion list insert
	rootNodeListIns := sq.
		Insert(rootNodesTableName).
		Columns("list_id", "node")
	for _, root := range list.RootList {
		rootNodeListIns = rootNodeListIns.Values(id, root.Alias)
	}
	rootNodeListIns = rootNodeListIns.Suffix("ON CONFLICT (list_id, node) DO NOTHING")

	signersQuery, signersArgs, _ := listSignersIns.ToSql()
	queryBuilder.WriteString(signersQuery)
	args = append(args, signersArgs...)
	exclusionQuery, exclusionArgs, _ := rootNodeListIns.ToSql()
	queryBuilder.WriteString(exclusionQuery)
	args = append(args, exclusionArgs...)

	return q.db.ExecRaw(queryBuilder.String(), args...)
}

func removeDuplicate[T string | int](sliceList []T) []T {
	allKeys := make(map[T]bool)
	var list []T
	for _, item := range sliceList {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

// SaveNodeAliases must be executed in a transaction
func (q *layerZeroListQ) SaveNodeAliases(nodes []data.NodeAccountPair) error {
	// TODO find a better solution without fetching pairs and all the mess

	// Main account -> Alias
	newNodesMap := make(map[string]string)

	nodeMainAccounts := make([]string, 0, len(nodes))
	for _, node := range nodes {
		mainAccount := node.MainAccount.String
		if !node.MainAccount.Valid {
			mainAccount = node.Alias
			continue
		}

		newNodesMap[mainAccount] = node.Alias
		nodeMainAccounts = append(nodeMainAccounts, mainAccount)
	}

	// Remove duplicates
	{
		allKeys := make(map[string]struct{})
		var list []string
		for _, item := range nodeMainAccounts {
			if _, value := allKeys[item]; !value {
				allKeys[item] = struct{}{}
				list = append(list, item)
			}
		}

		nodeMainAccounts = list
	}

	// Fetch all active node pairs with aliases from given list
	selectQuery := sq.Select("node", "main_account").
		Distinct().
		From(nodeAccountPairsTableName).
		Where(sq.Eq{
			"main_account": nodeMainAccounts,
			"active":       true,
		})

	var pairs []data.NodeAccountPair
	if err := q.db.Select(&pairs, selectQuery); err != nil {
		return err
	}

	// Map saved pairs into the map Main account -> Alias
	savedNodesMap := make(map[string]string)
	for _, pair := range pairs {
		savedNodesMap[pair.MainAccount.String] = pair.Alias
	}

	// Mark the pair as inactive for new pairs where main account is a duplicate of a saved pair,
	// but with different alias.
	// Also, insert the new pair
	toUpdate := make([]data.NodeAccountPair, 0)
	toInsert := make([]data.NodeAccountPair, 0)
	for _, mainAccount := range nodeMainAccounts {
		if savedAlias, ok := savedNodesMap[mainAccount]; ok &&
			savedAlias != newNodesMap[mainAccount] {
			toUpdate = append(toUpdate, data.NodeAccountPair{
				Alias: savedAlias,
				MainAccount: sql.NullString{
					String: mainAccount,
					Valid:  true,
				},
			})

			toInsert = append(toInsert, data.NodeAccountPair{
				Alias: newNodesMap[mainAccount],
				MainAccount: sql.NullString{
					String: mainAccount,
					Valid:  true,
				},
			})

			continue
		}

		// If the pair doesn't appear in the saved pairs list, mark it for insert
		if _, ok := savedNodesMap[mainAccount]; !ok {
			toInsert = append(toInsert, data.NodeAccountPair{
				Alias: newNodesMap[mainAccount],
				MainAccount: sql.NullString{
					String: mainAccount,
					Valid:  true,
				},
			})
		}
	}

	if len(toUpdate) > 0 {
		orConditions := make(sq.Or, 0, len(toUpdate))
		for _, marked := range toUpdate {
			condition := sq.Eq{
				"node":         marked.Alias,
				"main_account": marked.MainAccount.String,
			}
			orConditions = append(orConditions, condition)
		}

		updateQuery := sq.Update(nodeAccountPairsTableName).
			Set("active", false).
			Where(orConditions)
		if err := q.db.Exec(updateQuery); err != nil {
			return err
		}
	}

	if len(toInsert) > 0 {
		insertQuery := sq.Insert(nodeAccountPairsTableName).
			Columns("node", "main_account").
			Suffix("ON CONFLICT (node) DO UPDATE SET main_account = excluded.main_account, active = TRUE")
		for _, entry := range toInsert {
			insertQuery = insertQuery.Values(entry.Alias, entry.MainAccount)
		}

		if err := q.db.Exec(insertQuery); err != nil {
			return err
		}
	}

	return nil
}

func (q *layerZeroListQ) SaveExclusionList(list data.NodeExclusionList) error {
	var queryBuilder strings.Builder
	var args []interface{}

	// List insert
	layerZeroListIns := sq.
		Insert(layerZeroListsTableName).
		Columns("hash", "timestamp", "type", "status").
		Values(list.LayerZeroList.Hash, list.LayerZeroList.Timestamp, data.ListTypeExclusion, list.Status).
		Suffix("ON CONFLICT (hash, status) DO UPDATE SET hash = EXCLUDED.hash").
		Suffix("RETURNING id")

	var id uint64
	err := q.db.Get(&id, layerZeroListIns)
	if err != nil {
		return err
	}

	// Prepare list's signers insert
	listSignersIns := sq.
		Insert(listSignersTableName).
		Columns("list_id", "signer")
	for _, signer := range list.Signers {
		listSignersIns = listSignersIns.Values(id, signer.Alias)
	}
	listSignersIns = listSignersIns.
		Suffix("ON CONFLICT (list_id, signer) DO NOTHING").
		Prefix("WITH list_signers AS (").
		Suffix(")")

	// Prepare exclusion list insert
	exclusionSetIns := sq.
		Insert(nodeExclusionTableName).
		Columns("list_id", "node", "start_block", "end_block")
	for _, exclusion := range list.ExclusionSet {
		exclusionSetIns = exclusionSetIns.Values(id, exclusion.Alias, exclusion.StartBlock, exclusion.EndBlock)
	}
	exclusionSetIns = exclusionSetIns.Suffix("ON CONFLICT (list_id, node, start_block, end_block) DO NOTHING")

	signersQuery, signersArgs, _ := listSignersIns.ToSql()
	queryBuilder.WriteString(signersQuery)
	args = append(args, signersArgs...)
	exclusionQuery, exclusionArgs, _ := exclusionSetIns.ToSql()
	queryBuilder.WriteString(exclusionQuery)
	args = append(args, exclusionArgs...)

	return q.db.ExecRaw(queryBuilder.String(), args...)
}

func (q *layerZeroListQ) OutdateList(listType, hash string) error {
	stmt := sq.
		Update(layerZeroListsTableName).
		Set("outdated", true).
		Where(sq.Eq{
			"hash": hash,
			"type": listType,
		})

	return q.db.Exec(stmt)
}

func (q *layerZeroListQ) Tx(fn func() error) error {
	return q.db.Transaction(fn)
}

func (r *rawExclusionList) toData() (data.NodeExclusionList, error) {
	list := data.NodeExclusionList{}
	list.Hash = r.Hash
	list.Type = r.Type
	list.Timestamp = r.Timestamp
	list.Status = r.Status

	list.ExclusionSet = make([]data.NodeExclusion, 0, len(r.Exclusions))
	for _, exclusion := range r.Exclusions {
		mainAccount := ""
		if exclusion.MainAccount != nil {
			mainAccount = *exclusion.MainAccount
		}

		list.ExclusionSet = append(list.ExclusionSet, data.NodeExclusion{
			NodeAccountPair: data.NodeAccountPair{
				Alias: exclusion.Node,
				MainAccount: sql.NullString{
					String: mainAccount,
					Valid:  mainAccount != "",
				},
			},
			StartBlock: exclusion.StartBlock,
			EndBlock: sql.NullInt64{
				Int64: int64(exclusion.EndBlock),
				Valid: exclusion.EndBlock != 0,
			},
		})
	}

	list.Signers = make([]data.NodeAccountPair, 0, len(r.Signers))
	for _, signer := range r.Signers {
		mainAccount := ""
		if signer.MainAccount != nil {
			mainAccount = *signer.MainAccount
		}

		list.Signers = append(list.Signers, data.NodeAccountPair{
			Alias: signer.Node,
			MainAccount: sql.NullString{
				String: mainAccount,
				Valid:  mainAccount != "",
			},
		})
	}

	return list, nil
}

func (r *rawRootNodeList) toData() (data.RootNodeList, error) {
	list := data.RootNodeList{}
	list.Hash = r.Hash
	list.Type = r.Type
	list.Timestamp = r.Timestamp
	list.Status = r.Status

	list.RootList = make([]data.NodeAccountPair, 0, len(r.RootNodes))
	for _, exclusion := range r.RootNodes {
		mainAccount := ""
		if exclusion.MainAccount != nil {
			mainAccount = *exclusion.MainAccount
		}

		list.RootList = append(list.RootList, data.NodeAccountPair{
			Alias: exclusion.Node,
			MainAccount: sql.NullString{
				String: mainAccount,
				Valid:  mainAccount != "",
			},
		})
	}

	list.Signers = make([]data.NodeAccountPair, 0, len(r.Signers))
	for _, signer := range r.Signers {
		mainAccount := ""
		if signer.MainAccount != nil {
			mainAccount = *signer.MainAccount
		}

		list.Signers = append(list.Signers, data.NodeAccountPair{
			Alias: signer.Node,
			MainAccount: sql.NullString{
				String: mainAccount,
				Valid:  mainAccount != "",
			},
		})
	}

	return list, nil
}

func (e *exclusions) Scan(value any) error {
	bytes, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("failed to unmarshal JSONB value: %v", value)
	}

	err := json.Unmarshal(bytes, e)
	if err != nil {
		return err
	}

	return nil
}

func (e *accountNodePairs) Scan(value any) error {
	bytes, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("failed to unmarshal JSONB value: %v", value)
	}

	err := json.Unmarshal(bytes, e)
	if err != nil {
		return err
	}

	return nil
}
