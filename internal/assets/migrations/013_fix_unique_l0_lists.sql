-- +migrate Up

CREATE TABLE layer_zero_lists_v2 (
    id        BIGSERIAL PRIMARY KEY,
    hash      CHAR(66)               NOT NULL,
    timestamp TIMESTAMP              NOT NULL,
    type      LAYER_ZERO_LIST_TYPE   NOT NULL,
    status    LAYER_ZERO_LIST_STATUS NOT NULL,
    outdated  BOOLEAN                NOT NULL DEFAULT FALSE
);

CREATE UNIQUE INDEX layer_zero_lists_hash_idx ON layer_zero_lists_v2(hash, status);

INSERT INTO layer_zero_lists_v2 (hash, timestamp, type, status, outdated)
SELECT hash, timestamp, type, status, outdated
FROM layer_zero_lists;

ALTER TABLE node_exclusion DROP CONSTRAINT node_exclusion_list_hash_fkey;
ALTER TABLE list_signers DROP CONSTRAINT list_signers_list_hash_fkey;
ALTER TABLE root_nodes DROP CONSTRAINT root_nodes_list_hash_fkey;

DROP TABLE layer_zero_lists;

ALTER TABLE layer_zero_lists_v2 RENAME TO layer_zero_lists;

-- Modify node_exclusions
ALTER TABLE node_exclusion
    ADD COLUMN list_id BIGINT;
UPDATE node_exclusion
    SET list_id = (SELECT id FROM layer_zero_lists lzl WHERE lzl.hash = node_exclusion.list_hash);
ALTER TABLE node_exclusion
    ALTER COLUMN list_id SET NOT NULL;
ALTER TABLE node_exclusion
    ADD CONSTRAINT node_exclusion_list_id_fk FOREIGN KEY (list_id) REFERENCES layer_zero_lists (id);
ALTER TABLE node_exclusion
    DROP COLUMN list_hash;
ALTER TABLE node_exclusion
    ADD CONSTRAINT node_exclusion_pk
        PRIMARY KEY (list_id, node, start_block, end_block);

-- Modify list_signers
ALTER TABLE list_signers
    ADD COLUMN list_id BIGINT;
UPDATE list_signers
    SET list_id = (SELECT id FROM layer_zero_lists lzl WHERE lzl.hash = list_signers.list_hash);
ALTER TABLE list_signers
    ALTER COLUMN list_id SET NOT NULL;
ALTER TABLE list_signers
    ADD CONSTRAINT list_signers_list_id_fk FOREIGN KEY (list_id) REFERENCES layer_zero_lists (id);
ALTER TABLE list_signers
    DROP COLUMN list_hash;
ALTER TABLE list_signers
    ADD CONSTRAINT list_signers_pk
        PRIMARY KEY (list_id, signer);

-- Modify root_nodes
ALTER TABLE root_nodes
    ADD COLUMN list_id BIGINT;
UPDATE root_nodes
    SET list_id = (SELECT id FROM layer_zero_lists lzl WHERE lzl.hash = root_nodes.list_hash);
ALTER TABLE root_nodes
    ALTER COLUMN list_id SET NOT NULL;
ALTER TABLE root_nodes
    ADD CONSTRAINT root_nodes_list_id_fk FOREIGN KEY (list_id) REFERENCES layer_zero_lists (id);
ALTER TABLE root_nodes
    DROP COLUMN list_hash;
ALTER TABLE root_nodes
    ADD CONSTRAINT root_nodes_pk
        PRIMARY KEY (list_id, node);

-- +migrate Down

-- Remove root_nodes list id fk
ALTER TABLE root_nodes
    ADD COLUMN list_hash CHAR(66);
UPDATE root_nodes
    SET list_hash = (SELECT hash FROM layer_zero_lists lzl WHERE lzl.id = root_nodes.list_id);
ALTER TABLE root_nodes
    ALTER COLUMN list_id SET NOT NULL;
ALTER TABLE root_nodes
    DROP CONSTRAINT root_nodes_list_id_fk;

-- Remove list_signers list id fk
ALTER TABLE list_signers
    ADD COLUMN list_hash CHAR(66);
UPDATE list_signers
    SET list_hash = (SELECT hash FROM layer_zero_lists lzl WHERE lzl.id = list_signers.list_id);
ALTER TABLE list_signers
    ALTER COLUMN list_id SET NOT NULL;
ALTER TABLE list_signers
    DROP CONSTRAINT list_signers_list_id_fk;

-- Remove node_exclusion list id fk
ALTER TABLE node_exclusion
    ADD COLUMN list_hash CHAR(66);
UPDATE node_exclusion
    SET list_hash = (SELECT hash FROM layer_zero_lists lzl WHERE lzl.id = node_exclusion.list_id);
ALTER TABLE node_exclusion
    ALTER COLUMN list_id SET NOT NULL;
ALTER TABLE node_exclusion
    DROP CONSTRAINT node_exclusion_list_id_fk;

DELETE FROM layer_zero_lists lzl1 USING (
    SELECT MIN(ctid) as ctid, hash
    FROM layer_zero_lists
    GROUP BY hash HAVING COUNT(*) > 1
) lzl2
WHERE lzl1.hash = lzl2.hash;

CREATE TABLE layer_zero_lists_v1 (
    hash      CHAR(66)               PRIMARY KEY,
    timestamp TIMESTAMP              NOT NULL,
    type      LAYER_ZERO_LIST_TYPE   NOT NULL,
    status    LAYER_ZERO_LIST_STATUS NOT NULL,
    outdated  BOOLEAN                NOT NULL DEFAULT FALSE
);

INSERT INTO layer_zero_lists_v1 (hash, timestamp, type, status, outdated)
SELECT hash, timestamp, type, status, outdated
FROM layer_zero_lists;

DROP TABLE layer_zero_lists;

ALTER TABLE layer_zero_lists_v1 RENAME TO layer_zero_lists;

DELETE FROM root_nodes WHERE list_hash NOT IN (SELECT hash FROM layer_zero_lists);
ALTER TABLE root_nodes
    ADD CONSTRAINT root_nodes_list_id_fk FOREIGN KEY (list_hash) REFERENCES layer_zero_lists (hash);
ALTER TABLE root_nodes
    DROP CONSTRAINT root_nodes_pk;
ALTER TABLE root_nodes
    ADD CONSTRAINT root_nodes_pk
        PRIMARY KEY (list_hash, node);

DELETE FROM list_signers WHERE list_hash NOT IN (SELECT hash FROM layer_zero_lists);
ALTER TABLE list_signers
    ADD CONSTRAINT list_signers_list_id_fk FOREIGN KEY (list_hash) REFERENCES layer_zero_lists (hash);
ALTER TABLE list_signers
    DROP CONSTRAINT list_signers_pk;
ALTER TABLE list_signers
    ADD CONSTRAINT list_signers_pk
        PRIMARY KEY (list_hash, signer);

DELETE FROM node_exclusion WHERE list_hash NOT IN (SELECT hash FROM layer_zero_lists);
ALTER TABLE node_exclusion
    ADD CONSTRAINT node_exclusion_list_id_fk FOREIGN KEY (list_hash) REFERENCES layer_zero_lists (hash);
ALTER TABLE node_exclusion
    DROP CONSTRAINT node_exclusion_pk;
ALTER TABLE node_exclusion
    ADD CONSTRAINT node_exclusion_pk
        PRIMARY KEY (list_hash, node, start_block, end_block);
