package data

// VotingAgent .
type VotingAgent struct {
	Id          uint64 `db:"id"           structs:"-"`
	Address     string `db:"address"      structs:"address"`
	VotingAgent string `db:"voting_agent" structs:"voting_agent"`
	StartBlock  uint64 `db:"start_block"  structs:"start_block"`
	EndBlock    uint64 `db:"end_block"    structs:"end_block"`
}

// VotingAgentStorage stores voting agents.
type VotingAgentStorage interface {
	Insert(agents ...VotingAgent) error
	GetByVotingAgents(address ...string) ([]VotingAgent, error)
	GetLatestBlock() (int64, error)
}
