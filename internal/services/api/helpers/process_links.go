package helpers

import (
	"net/url"
	"strconv"
	"strings"

	"github.com/gorilla/schema"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/requests"
)

func GetSystemContractsLinks(
	contracts []data.SystemContract,
	request requests.GetSystemContractListRequest,
	endpoint url.URL,
) (*resources.Links, error) {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(request, params); err != nil {
		return nil, errors.Wrap(err, "failed to encode query")
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(contracts) == 0 {
		return links, nil
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", strconv.FormatUint(contracts[len(contracts)-1].ID, 10))
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", strconv.FormatUint(contracts[0].ID, 10))
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)

	return links, nil
}

func GetVotingEventsLinks(
	votingEvents []data.VotingEvent,
	request requests.GetVotingEventsRequest,
	endpoint url.URL,
) (*resources.Links, error) {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(request, params); err != nil {
		return nil, errors.Wrap(err, "failed to encode query")
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(votingEvents) == 0 {
		return links, nil
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", strconv.FormatUint(votingEvents[len(votingEvents)-1].Id, 10))
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", strconv.FormatUint(votingEvents[0].Id, 10))
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)

	return links, nil
}

func GetAggregatedVotingEventsList(
	aggregatedVotingEvents data.CountVotingEvents,
	request requests.GetVotingEventsRequest,
	endpoint url.URL,
) (*resources.Links, error) {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(request, params); err != nil {
		return nil, errors.Wrap(err, "failed to encode query")
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(aggregatedVotingEvents.ByAddress) == 0 {
		return links, nil
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", aggregatedVotingEvents.ByAddress[len(aggregatedVotingEvents.ByAddress)-1].AccountAddress)
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", aggregatedVotingEvents.ByAddress[0].AccountAddress)
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)

	return links, nil
}

func GetRootNodeApprovalsLinks(
	approvals data.RootNodeBreakdown,
	request requests.GetRootNodeApprovalsRequest,
	endpoint url.URL,
) (*resources.Links, error) {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(request, params); err != nil {
		return nil, errors.Wrap(err, "failed to encode query")
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(approvals.ByAddress) == 0 {
		return links, nil
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", approvals.ByAddress[len(approvals.ByAddress)-1].Account)
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", approvals.ByAddress[0].Account)
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)

	return links, nil
}
