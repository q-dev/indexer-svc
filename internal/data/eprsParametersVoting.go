package data

// EprsParametersVoting voting.
type EprsParametersVoting BaseVoting

// EprsParametersVotingStorage stores eprs parameters votings.
type EprsParametersVotingStorage interface {
	Insert(votings ...EprsParametersVoting) error
	GetLatest() (*EprsParametersVoting, error)

	GetEarliestUnfinishedVotings() ([]EprsParametersVoting, error)
	Update(voting *EprsParametersVoting) error
}
