package handlers

import (
	"math/big"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/contracts"
	"gitlab.com/q-dev/q-client/ethclient"

	"gitlab.com/q-dev/indexer-svc/internal/config"
	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/helpers"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/requests"
	"gitlab.com/q-dev/indexer-svc/internal/services/ingest"
	"gitlab.com/q-dev/indexer-svc/internal/vars"
)

type RootNodeApprovalsHandler struct {
	log              *logan.Entry
	client           *ethclient.Client
	storage          data.RootNodeApprovalsStorage
	contractRegistry *contracts.Registry
}

func NewRootNodeApprovalsHandler(log *logan.Entry, client *ethclient.Client, storage data.RootNodeApprovalsStorage,
	registryCfg config.ContractsRegistry,
) RootNodeApprovalsHandler {
	return RootNodeApprovalsHandler{
		log:              log,
		storage:          storage,
		client:           client,
		contractRegistry: ingest.NewContractsRegistry(client, registryCfg),
	}
}

// GetRootNodeApprovals returns root node metrics.
// @Summary   	Get root node metrics
// @Description Returns root node metrics (approvals of transition blocks aka co-sign).
// @ID       	root-node-metrics
// @Accept    	json
// @Produce    	json
// @Param     	filter[cycles] query int false "Filter by amount of cycles. Zero is not allowed"
// @Param     	filter[rootAddress] query string false "Filter by address of root node."
// @Param     	filter[blocksDelay] query int false "Exclude `blocksDelay` blocks before the latest."
// @Param     	sort query string false "Sorting order. Default - `desc`"
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.RootNodeApprovalsList
// @Failure   	400 "Bad request"
// @Failure   	500 "Internal Server Error"
// @Router     	/root-node-metrics [get]
func (h *RootNodeApprovalsHandler) GetRootNodeApprovals(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetRootNodeApprovalsRequest(r)
	if err != nil {
		ape.Render(w, problems.BadRequest(err))
		return
	}

	header, err := h.client.HeaderByNumber(r.Context(), nil)
	if err != nil {
		ape.Render(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get latest block header")
		return
	}

	rootNodeApprovals, err := h.storage.Select(data.SelectRootNodeApprovalParams{
		IsDesc:      request.Sort == "desc",
		Cursor:      request.Cursor,
		Limit:       request.Limit,
		BlocksDelay: request.BlocksDelay,
		RootAddress: request.RootAddress,
		Cycles:      request.Cycles,
	}, header.Number.Uint64())
	if err != nil {
		ape.Render(w, problems.InternalError())
		h.log.WithError(err).Error("failed to query the DB")
		return
	}

	var rootNodePairs []resources.RootNodePair
	if len(rootNodeApprovals.ByAddress) != 0 && request.RootAddress == nil {
		rootNodePairs, err = h.getOnchainRootsWithAliases(rootNodeApprovals)
		if err != nil {
			ape.Render(w, problems.InternalError())
			h.log.WithError(err).Error("failed to figure out aliases")
			return
		}
	}

	resp := resources.PopulateRootNodeApprovalsList(rootNodeApprovals, rootNodePairs)
	resp.Links, err = helpers.GetRootNodeApprovalsLinks(rootNodeApprovals, request, *r.URL)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to build links")
		return
	}

	ape.Render(w, resp)
}

func getOnchainWithoutApprovals(rootNodeApprovals data.RootNodeBreakdown, onchainRoots []common.Address) []common.Address {
	approvals := make(map[string]struct{})
	for _, approval := range rootNodeApprovals.ByAddress {
		approvals[strings.ToLower(approval.Account)] = struct{}{}
	}

	onchainWithoutApprovals := make([]common.Address, 0)
	for _, root := range onchainRoots {
		addr := strings.ToLower(root.String())
		if _, ok := approvals[addr]; !ok {
			onchainWithoutApprovals = append(onchainWithoutApprovals, common.HexToAddress(addr))
		}
	}

	return onchainWithoutApprovals
}

func (h *RootNodeApprovalsHandler) getOnchainRootsWithAliases(rootNodeApprovals data.RootNodeBreakdown,
) ([]resources.RootNodePair, error) {
	onchainRoots, err := h.contractRegistry.Roots().GetMembers(nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get onchain root list")
	}

	onchainWithoutApprovals := getOnchainWithoutApprovals(rootNodeApprovals, onchainRoots)
	purposes := make([]*big.Int, 0, len(onchainWithoutApprovals))
	for range onchainWithoutApprovals {
		purposes = append(purposes, vars.RNOperationPurpose)
	}
	onchainRootsAliases, err := h.contractRegistry.AccountAliases(nil).
		ResolveBatchReverse(nil, onchainWithoutApprovals, purposes)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get onchain roots aliases")
	}

	rootNodePairs := make([]resources.RootNodePair, len(onchainWithoutApprovals))
	for i, root := range onchainWithoutApprovals {
		rootNodePairs[i] = resources.RootNodePair{
			Alias:       onchainRootsAliases[i].String(),
			MainAccount: root.String(),
		}
	}

	return rootNodePairs, nil
}
