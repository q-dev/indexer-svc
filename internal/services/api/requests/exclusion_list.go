package requests

import (
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
)

const (
	listActive   = "active"
	listProposed = "proposed"

	ListRoot      = "root"
	ListExclusion = "exclusion"
)

type GetExclusionListRequest struct {
	Status string `url:"status"`
}

func NewGetExclusionListRequest(r *http.Request) (GetExclusionListRequest, error) {
	status := r.URL.Query().Get("status")
	request := GetExclusionListRequest{status}

	return request, request.Validate()
}

func (r *GetExclusionListRequest) Validate() error {
	return validation.Errors{
		"status": validation.Validate(r.Status, validation.Required, validation.In(listActive, listProposed)),
	}.Filter()
}
