package postgres

import (
	"database/sql"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// RootsVotingStorage implements data.RootsVotingStorage.
type RootsVotingStorage struct {
	db *pgdb.DB
}

var (
	rMVotingColumns = []string{
		"proposal_id",
		"voting_type",
		"voting_status",
		"current_quorum",
		"required_quorum",
		"vetos_number",
		"candidate",
		"replace_dest",
		"cast(extract(epoch from voting_end_time) as integer) as voting_end_time",
		"cast(extract(epoch from voting_start_time) as integer) as voting_start_time",
		"cast(extract(epoch from veto_end_time) as integer) as veto_end_time",
	}
)

func (s *RootsVotingStorage) Insert(votings ...data.RootsVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert("roots_votings").Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_quorum",
		"required_quorum",
		"root_nodes_number",
		"vetos_number",
		"voting_end_time",
		"veto_end_time",
		"candidate",
		"replace_dest",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentQuorum,
			voting.RequiredQuorum,
			voting.RootNodesNumber,
			voting.VetosNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
			time.Unix(int64(voting.VetoEndTime), 0),
			voting.Candidate,
			voting.ReplaceDest,
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_quorum=EXCLUDED.current_quorum," +
		"root_nodes_number=EXCLUDED.root_nodes_number," +
		"vetos_number=EXCLUDED.vetos_number",
	)

	return errors.Wrap(s.db.Exec(query), "failed to insert roots voting")
}

// GetLatest roots voting.
func (s *RootsVotingStorage) GetLatest() (*data.RootsVoting, error) {
	return s.queryBlock(squirrel.Select(rMVotingColumns...).From("roots_votings").
		Where("proposal_id = (SELECT max(proposal_id) from roots_votings)"))
}

func (s *RootsVotingStorage) GetEarliestUnfinishedVotingIds() ([]uint64, error) {
	var votingIds []uint64
	query := squirrel.Select("roots_votings.proposal_id").From("roots_votings").
		Where("voting_status IN (1, 3, 4)").OrderBy("proposal_id ASC")
	if err := s.db.Select(&votingIds, query); err != nil {
		return nil, errors.Wrap(err, "failed to select roots votings")
	}
	return votingIds, nil
}

func (s *RootsVotingStorage) Update(voting *data.RootsVoting) error {
	query := squirrel.Update("roots_votings").SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_quorum":    voting.CurrentQuorum,
		"root_nodes_number": voting.RootNodesNumber,
		"vetos_number":      voting.VetosNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query), "failed to update roots voting")
}

func (s *RootsVotingStorage) queryBlock(query squirrel.SelectBuilder) (*data.RootsVoting, error) {
	var voting data.RootsVoting
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get roots voting")
	}
}
