package api

import (
	"net"
	"net/http"

	"github.com/go-chi/chi"
	httpSwagger "github.com/swaggo/http-swagger"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/logan/v3"

	"gitlab.com/q-dev/indexer-svc/docs"
	"gitlab.com/q-dev/indexer-svc/internal/config"
	"gitlab.com/q-dev/indexer-svc/internal/data/postgres"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/handlers"
	"gitlab.com/q-dev/indexer-svc/internal/services/ingest"
)

// API provides rest api.
type API struct {
	log    *logan.Entry
	router chi.Router
}

// @title Indexer API
// @version 1.0
// @description Indexer service that indexes data from q-client.
// @termsOfService http://swagger.io/terms/

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// New API.
func New(cfg config.Config) *API {
	r := chi.NewRouter()
	r.Use(Cors)

	log := cfg.Log().WithField("svc", "api")
	ape.DefaultMiddlewares(r, log)

	docs.SwaggerInfo.Host = cfg.Listener().Addr().String()
	r.Mount("/swagger", httpSwagger.WrapHandler)

	storage := postgres.New(cfg.DB())
	txHandler := txHandler{
		log:     log,
		storage: storage.Clone().Transactions(),
	}
	r.Get("/transactions", txHandler.getTxHistory)

	paymentsHandler := paymentsHandler{
		log:     log,
		storage: storage.Clone(),
	}
	r.Get("/payments", paymentsHandler.getPayments)

	delegationsHandler := delegationsHandler{
		log:     log,
		storage: storage.Clone(),
	}
	r.Get("/delegator-count", delegationsHandler.getDelegatorCount)

	blocksHandler := blocksHandler{
		log:     log,
		storage: storage.Clone().Blocks(),
	}
	r.Get("/blocks", blocksHandler.getBlocks)

	oldMetricsHandler := summaryMetricsHandler{
		log:     log,
		storage: storage.Clone().SummaryMetrics(),
	}
	r.Get("/metrics", oldMetricsHandler.getMetrics)

	metricsHandler := summaryMetricsHandler{
		log:     log,
		storage: storage.Clone().SummaryMetrics(),
	}
	r.Get("/validator-metrics", metricsHandler.getValidatorMetrics)

	votingsHandler := votingsHandler{
		log:              log,
		storage:          storage.Clone().Votings(),
		contractRegistry: ingest.NewContractsRegistry(cfg.Client(), cfg.ContractsRegistry()),
	}
	r.Get("/votings", votingsHandler.getVotings)

	versionHandler := versionHandler{
		log: log,
	}
	r.Get("/version", versionHandler.getVersion)

	rootMembershipPeriodHandler := rootMembershipPeriodHandler{
		log:     log,
		storage: storage.Clone().RootsMembership(),
	}
	r.Get("/root-node-metrics/onchain-membership", rootMembershipPeriodHandler.getRootMemberships)

	eventHandler := handlers.NewEventHandler(log, cfg.Client(),
		ingest.NewContractsRegistry(cfg.Client(), cfg.ContractsRegistry()),
		storage.Clone().RootsMembership(),
	)

	qTokenHolderVotesHandler := handlers.NewQTokenHolderVotesHandler(eventHandler, storage.Clone().QTokenHolderVotes())
	r.Get("/root-node-metrics/q-token-holder-votings", qTokenHolderVotesHandler.GetQTokenHolderVotes)
	r.Get("/root-node-metrics/q-token-holder-votings/aggregated", qTokenHolderVotesHandler.GetQTokenHolderAggregatedVotes)

	rootNodeVotesHandler := handlers.NewRootNodeVotesHandler(eventHandler, storage.Clone().RootNodeVotes())
	r.Get("/root-node-metrics/rootnode-votings", rootNodeVotesHandler.GetRootNodeVotes)
	r.Get("/root-node-metrics/rootnode-votings/aggregated", rootNodeVotesHandler.GetRootNodeAggregatedVotes)

	rootNodeProposalsHandler := handlers.NewRootNodeProposalsHandler(eventHandler, storage.Clone().RootNodeProposals())
	r.Get("/root-node-metrics/rootnode-proposals", rootNodeProposalsHandler.GetRootNodeProposals)
	r.Get("/root-node-metrics/rootnode-proposals/aggregated", rootNodeProposalsHandler.GetRootNodeAggregatedProposals)

	layerZeroListsHandler := handlers.NewLayerZeroListsHandler(
		log,
		storage.Clone().LayerZeroLists(),
		cfg.LayerZeroLists().CachePeriod,
		cfg.Client(),
		ingest.NewContractsRegistry(cfg.Client(), cfg.ContractsRegistry()),
	)
	r.Get("/exclusion-list", layerZeroListsHandler.GetExclusionList)
	r.Get("/root-list", layerZeroListsHandler.GetRootNodesList)
	r.Get("/list-diff/{type}", layerZeroListsHandler.GetDiff)
	r.Get("/layer-zero-lists", layerZeroListsHandler.GetLayerZeroLists)

	systemContractsHandler := handlers.NewSystemContractsHandler(log, storage.Clone().SystemContracts())
	r.Get("/system-contracts", systemContractsHandler.GetSystemContract)
	r.Get("/contract-registry", systemContractsHandler.GetContractRegistry)

	rootNodeApprovalsHandler := handlers.NewRootNodeApprovalsHandler(
		log,
		cfg.Client(),
		storage.Clone().RootNodeApprovals(),
		cfg.ContractsRegistry(),
	)
	r.Get("/root-node-metrics", rootNodeApprovalsHandler.GetRootNodeApprovals)

	return &API{
		log:    log,
		router: r,
	}
}

func Cors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		next.ServeHTTP(w, r)
	})
}

// Run API.
func (api *API) Run(listener net.Listener) error {
	api.log.WithField("addr", listener.Addr()).Info("starting api")
	return http.Serve(listener, api.router)
}
