package handlers

import (
	"net/http"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type QTokenHolderVotesHandler struct {
	EventHandler
	storage data.QTokenHolderVotesStorage
}

func NewQTokenHolderVotesHandler(eventHandler EventHandler, storage data.QTokenHolderVotesStorage) QTokenHolderVotesHandler {
	return QTokenHolderVotesHandler{
		EventHandler: eventHandler,
		storage:      storage,
	}
}

// GetQTokenHolderVotes returns q token holder votes.
// @Summary   	Get q token holder votes
// @Description  Returns q token holder votes.
// @ID       	q-token-holder-votings
// @Accept    	json
// @Produce    	json
// @Param     	filter[rootAddress] query string false "Returns ALL root node proposals for `rootAddress`."
// @Param     	filter[startBlock] query int false "Returns root node proposals after `startBlock`."
// @Param     	sort query string false "Sorting order. Supported values: `account_address` (sort by account_address, asc), `-block` (sort by account_address, desc). Default - `-account_address`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.EventsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/root-node-metrics/q-token-holder-votings [get]
func (h *QTokenHolderVotesHandler) GetQTokenHolderVotes(w http.ResponseWriter, r *http.Request) {
	h.getEvents(h.storage.Select, w, r)
}

// GetQTokenHolderAggregatedVotes returns aggregated q token holder votes.
// @Summary   	Get aggregated q token holder votes
// @Description Returns aggregated q token holder votes.
// @ID       	q-token-holder-votings-aggregated
// @Accept    	json
// @Produce    	json
// @Param     	filter[rootAddress] query string false "Returns ALL root node proposals for `rootAddress`."
// @Param     	filter[startBlock] query int false "Returns root node proposals after `startBlock`."
// @Param     	sort query string false "Sorting order. Supported values: `account_address` (sort by account_address, asc), `-block` (sort by account_address, desc). Default - `-account_address`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.AggregatedEventsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/root-node-metrics/q-token-holder-votings/aggregated [get]
func (h *QTokenHolderVotesHandler) GetQTokenHolderAggregatedVotes(w http.ResponseWriter, r *http.Request) {
	h.getAggregatedEvents(h.storage.SelectAggregated, w, r)
}
