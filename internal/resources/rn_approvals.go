package resources

import (
	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type RootNodeApprovalsList struct {
	Data  RootNodeApprovals `json:"data"`
	Links *Links            `json:"links,omitempty"`
}

type RootNodeApprovals struct {
	ID         string                 `json:"id"`
	Type       string                 `json:"type"`
	Attributes RootNodeApprovalsAttrs `json:"attributes"`
}

type RootNodeApprovalsAttrs struct {
	FirstTransitionBlock uint64      `json:"firstTransitionBlock"`
	LastTransitionBlock  uint64      `json:"lastTransitionBlock"`
	ByAddress            []ByAddress `json:"byAddress"`
}

type ByAddress struct {
	Account               string                `json:"mainAccount"`
	FirstObservedApproval FirstObservedApproval `json:"firstObservedApproval"`
	LastObservedApproval  LastObservedApproval  `json:"lastObservedApproval"`
	ObservedApprovals     []ObservedApprovals   `json:"observedApprovals"`
}

type ObservedApprovals struct {
	Alias                 *string               `json:"alias"`
	FirstObservedApproval FirstObservedApproval `json:"firstObservedApproval"`
	LastObservedApproval  LastObservedApproval  `json:"lastObservedApproval"`
}

type FirstObservedApproval struct {
	Block     uint64 `json:"block"`
	DueCycles uint64 `json:"dueCycles"`
}

type LastObservedApproval struct {
	Block         uint64 `json:"block"`
	OfflineCycles uint64 `json:"offlineCycles"`
}

type RootNodeBreakdown struct {
	FirstTransitionBlock uint64
	LastTransitionBlock  uint64
	ObservedApprovals    []ByAddress
}

type RootNodePair struct {
	Alias       string
	MainAccount string
}

func PopulateRootNodeApprovalsList(approvals data.RootNodeBreakdown, onchainWithoutApprovals []RootNodePair,
) *RootNodeApprovalsList {
	resp := &RootNodeApprovalsList{
		Data: RootNodeApprovals{
			ID:   "0",
			Type: "rootNodeApprovals",
			Attributes: RootNodeApprovalsAttrs{
				FirstTransitionBlock: approvals.FirstTransitionBlock,
				LastTransitionBlock:  approvals.LastTransitionBlock,
				ByAddress:            make([]ByAddress, len(approvals.ByAddress)),
			},
		},
	}

	for i, byAddress := range approvals.ByAddress {
		resp.Data.Attributes.ByAddress[i] = ByAddress{
			Account:           byAddress.Account,
			ObservedApprovals: make([]ObservedApprovals, len(byAddress.ObservedApprovals)),
		}
		approval := byAddress.ObservedApprovals[0]
		minFirst := FirstObservedApproval{
			Block:     approval.FirstObservedApproval.Block,
			DueCycles: approval.FirstObservedApproval.Cycles,
		}
		maxLast := LastObservedApproval{
			Block:         approval.LastObservedApproval.Block,
			OfflineCycles: approval.LastObservedApproval.Cycles,
		}
		for y, observedApproval := range byAddress.ObservedApprovals {
			first := FirstObservedApproval{
				Block:     observedApproval.FirstObservedApproval.Block,
				DueCycles: observedApproval.FirstObservedApproval.Cycles,
			}
			last := LastObservedApproval{
				Block:         observedApproval.LastObservedApproval.Block,
				OfflineCycles: observedApproval.LastObservedApproval.Cycles,
			}
			resp.Data.Attributes.ByAddress[i].ObservedApprovals[y] = ObservedApprovals{
				Alias:                 observedApproval.Alias,
				FirstObservedApproval: first,
				LastObservedApproval:  last,
			}

			if first.Block < minFirst.Block {
				minFirst = first
			}
			if last.Block > maxLast.Block {
				maxLast = last
			}
		}

		resp.Data.Attributes.ByAddress[i].FirstObservedApproval = minFirst
		resp.Data.Attributes.ByAddress[i].LastObservedApproval = maxLast
	}

	for _, pair := range onchainWithoutApprovals {
		alias := pair.Alias
		resp.Data.Attributes.ByAddress = append(resp.Data.Attributes.ByAddress, ByAddress{
			Account: pair.MainAccount,
			ObservedApprovals: []ObservedApprovals{
				{
					Alias: &alias,
				},
			},
		})
	}

	return resp
}
