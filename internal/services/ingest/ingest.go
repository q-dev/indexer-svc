package ingest

import (
	"bytes"
	"context"
	"fmt"
	"math/big"
	"strings"
	"sync"
	"time"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/distributed_lab/running"
	ethereum "gitlab.com/q-dev/q-client"
	"gitlab.com/q-dev/q-client/accounts/abi/bind"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/contracts"
	"gitlab.com/q-dev/q-client/core/types"
	"gitlab.com/q-dev/q-client/crypto"
	"gitlab.com/q-dev/q-client/ethclient"
	"gitlab.com/q-dev/q-client/indexer"

	"gitlab.com/q-dev/indexer-svc/internal/config"
	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/data/postgres"
	"gitlab.com/q-dev/indexer-svc/internal/erc20"
	"gitlab.com/q-dev/indexer-svc/internal/vars"
)

const (
	ProposalStatusExecuted         = 5
	finishedStatuses        string = "0,2,5,6,7"
	voluntaryExitCheckpoint uint64 = 8096
)

var topicsDelegation = [][]common.Hash{{crypto.Keccak256Hash([]byte("DelegatedStakeUpdated(address,uint256)"))}}

type Notifier interface {
	NewTransaction(tx *data.Transaction)
	NewPayment(pmnt *data.Payment)
	SendNotifications(int64, context.Context) error
}

// Service ingests history.
type Service struct {
	log                     *logan.Entry
	client                  *ethclient.Client
	db                      data.Storage
	notifier                Notifier
	chainID                 *big.Int
	epochLength             uint64
	ignoreNotificationBlock int64
	contractRegistry        *contracts.Registry
	lastApprovedNodes       map[common.Address]bool
	lastCheckedBlock        uint64
	approvedNodesMutex      sync.RWMutex
}

// New Service.
func New(ctx context.Context, cfg config.Config) (*Service, error) {
	chainID, err := cfg.Client().ChainID(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get chain id from q-client")
	}

	esEpochLength, err := cfg.Client().EpochLength(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get epoch length")
	}

	return &Service{
		log:                     cfg.Log().WithField("svc", "ingest"),
		client:                  cfg.Client(),
		db:                      postgres.New(cfg.DB()),
		notifier:                cfg.Notifier(ctx),
		chainID:                 chainID,
		epochLength:             esEpochLength,
		ignoreNotificationBlock: cfg.IgnoreNotificationsCfg(),
		contractRegistry:        NewContractsRegistry(cfg.Client(), cfg.ContractsRegistry()),
	}, nil
}

func NewContractsRegistry(client *ethclient.Client, cfg config.ContractsRegistry) *contracts.Registry {
	return contracts.NewRegistry(*cfg.Registry, *cfg.RewardReceiver, client)
}

// Run service.
func (s *Service) Run(ctx context.Context) error {
	t := time.NewTicker(time.Second)
	s.log.Info("Current version of indexer: " + config.IndexerVersionWithMeta)

	time.Sleep(2 * time.Second)

	err := s.insertInitialRootMembershipPeriods()
	if err != nil {
		return err
	}

	go func() {
		localCtx, localCancel := context.WithCancel(ctx)
		defer localCancel()
		if err := s.updateSystemContracts(localCtx); err != nil {
			s.log.WithError(err).Error("failed to update system contracts")
		}
	}()

	child, cancel := context.WithCancel(ctx)
	defer cancel()
	go s.syncStateInThreads(child)
	go running.WithBackOff(child,
		s.log,
		"delegations sync",
		s.syncDelegations,
		time.Hour*12,
		time.Hour*12,
		time.Hour*24,
	)

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-t.C:
			remote, err := s.client.HeaderByNumber(ctx, nil)
			if err != nil {
				s.log.WithError(err).Error("failed to get latest block from q node, cool down for one second")
				<-t.C // cool down
				continue
			}

			lastKnown, err := s.db.Blocks().GetLatest()
			if err != nil {
				s.log.WithError(err).Error("failed to get last known block from db")
				continue
			}

			if lastKnown != nil && remote.Number.Uint64() <= lastKnown.Number {
				s.log.Debug("no changes detected, cool down for one second")
				<-t.C // cool down
				continue
			}

			var fromID uint64
			if lastKnown != nil {
				fromID = lastKnown.Number + 1
			}

			err = s.syncState(ctx, lastKnown, fromID, remote.Number.Uint64())
			if err != nil {
				s.log.WithError(err).Error("failed to sync state, cool down for one second")
				<-t.C // cool down
			}
		}
	}
}

func (s *Service) syncDelegations(ctx context.Context) error {
	validatorsContract := s.contractRegistry.GetAddr("governance.validators")
	input, err := erc20.ValidatorsABI.Pack("longList")
	if err != nil {
		return errors.Wrap(err, "abi error")
	}
	res, err := s.client.CallContract(ctx, ethereum.CallMsg{
		To:   &validatorsContract,
		Data: input,
	}, nil)
	if err != nil {
		return errors.Wrap(err, "couldn't get longList address")
	}
	contractAddress := common.BytesToAddress(res)

	currentBlock, err := s.client.BlockNumber(ctx)
	if err != nil {
		return errors.Wrap(err, "couldn't get current block number")
	}
	lastIndexed, err := s.db.DelegationBlock().GetNumber()
	if err != nil {
		s.log.Warn(err)
	}
	blockNum := big.NewInt(int64(lastIndexed))
	newBlockNum := big.NewInt(0)
	incr := big.NewInt(10000)
	bigOne := big.NewInt(1)

	for blockNum.Uint64() < currentBlock {
		query := ethereum.FilterQuery{
			FromBlock: blockNum,
			ToBlock:   newBlockNum.Add(blockNum, incr),
			Addresses: []common.Address{contractAddress},
			Topics:    topicsDelegation,
		}
		var logArray []types.Log
		logArray, err = s.client.FilterLogs(ctx, query)
		if err != nil {
			return errors.Wrap(err, "couldn't get event logs")
		}
		blockNum.Add(newBlockNum, bigOne)
		var (
			txHash                                common.Hash
			sender                                common.Address
			delegationsToAdd, delegationsToDelete []data.Delegation
		)
		qVaultContractAddress := strings.ToLower(s.contractRegistry.GetAddr("tokeneconomics.qVault").String())

		for _, log := range logArray {
			if log.Removed || (log.TxHash == txHash) {
				continue
			}
			txHash = log.TxHash
			tx, _, err := s.client.TransactionByHash(ctx, txHash)
			if err != nil {
				s.log.WithError(err)
			}
			if tx == nil {
				continue
			}
			sender, err = s.client.TransactionSender(ctx, tx, log.BlockHash, log.Index)
			if err != nil {
				s.log.WithError(err)
				continue
			}
			delegationsToAdd, delegationsToDelete = s.parseDelegations(qVaultContractAddress, sender, tx, delegationsToAdd, delegationsToDelete)
		}

		if err := s.db.Delegations().Insert(delegationsToAdd...); err != nil {
			return err
		}
		if err := s.db.Delegations().Delete(delegationsToDelete...); err != nil {
			return err
		}

		err = s.db.DelegationBlock().Update(min(currentBlock, blockNum.Uint64()))
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Service) syncStateInThreads(ctx context.Context) {
	wg := &sync.WaitGroup{}
	t := time.NewTicker(5 * time.Second)

	notificationBlock, err := s.client.BlockByNumber(ctx, big.NewInt(s.ignoreNotificationBlock))
	if err != nil {
		s.log.WithError(err).WithField("number", s.ignoreNotificationBlock).Error("failed to get block from q client")
	}

	s.spawnSyncApprovals(ctx)

	for {
		select {
		case <-ctx.Done():
			s.log.Info("Pending notifications have been sent")
			return
		case <-t.C:
			latest, err := s.client.HeaderByNumber(ctx, nil)
			if err != nil {
				s.log.WithError(err).Error("failed to get latest block from q node, skipping...")
				continue
			}

			s.executeConcurrentVotingFunc(s.processVotings, ctx, latest.Number.Uint64(), latest.Time, wg, "cannot process votings")
			s.executeConcurrentVotingFunc(s.updateVotings, ctx, latest.Number.Uint64(), latest.Time, wg, "cannot update votings")

			wg.Add(1)
			go func() {
				defer wg.Done()
				childCtx, cancel := context.WithCancel(ctx)
				err := s.notifier.SendNotifications(int64(notificationBlock.Time()), childCtx)
				if err != nil {
					s.log.WithError(err).Error("cannot process notifications")
				}
				cancel()
			}()
		}
		wg.Wait()
	}
}

func (s *Service) syncState(ctx context.Context, lastKnown *data.Block, from, to uint64) error {
	log := s.log.WithField("range", fmt.Sprintf("%d-%d", from, to))
	wg := &sync.WaitGroup{}

	for idx := from; idx <= to; idx++ {
		qBlock, err := s.client.BlockByNumber(ctx, big.NewInt(int64(idx)))
		if err != nil {
			return errors.Wrap(err, "failed to get block from q client", logan.F{"number": idx})
		}

		if isReorg(lastKnown, qBlock) {
			log.WithField("expected-parent", lastKnown.Hash).
				WithField("actual-parent", qBlock.Header().ParentHash.Hex()).
				WithField("number", qBlock.Number().Uint64()).
				Info("detected chain reorg")

			if err := s.handleReorg(ctx, lastKnown.Number); err != nil {
				return errors.Wrap(err, "failed to handle chain reorg")
			}

			// force re-init sync session
			return nil
		}

		var ootstats *indexer.OutOfTurnStats
		if idx != 0 {
			ootstats, err = s.client.OotStats(ctx, big.NewInt(int64(idx)))
		} else {
			ootstats = newOotStats(qBlock)
		}
		if err != nil {
			return errors.Wrap(err, "failed to get out of turn stats from q client", logan.F{"number": idx})
		}

		lastKnown, err = s.processBlock(ctx, qBlock, ootstats)
		if err != nil {
			return errors.Wrap(err, "failed to process block")
		}

		log.WithFields(logan.F{"number": idx, "count": len(qBlock.Transactions())}).
			Info("processed block")
	}

	wg.Wait()
	return nil
}

func (s *Service) spawnSyncApprovals(ctx context.Context) {
	go running.WithBackOff(ctx,
		s.log,
		"approvals sync",
		s.syncApprovals,
		time.Second*5,
		time.Second*20,
		time.Minute,
	)
}

func (s *Service) syncApprovals(ctx context.Context) error {
	db := s.db.Clone()

	remote, err := s.client.HeaderByNumber(ctx, nil)
	if err != nil {
		s.log.WithError(err).Error("failed to get latest block from q node")
		return err
	}

	latestApproval, err := db.RootNodeApprovals().GetLatest()
	if err != nil {
		s.log.WithError(err).Error("failed to get latest root node approval")
		return err
	}
	start := int64(0)
	if latestApproval != nil {
		start = int64(latestApproval.Block)
	}

	approvalLists := make([]common.RootNodeApproval, 0)
	for i := start; i < remote.Number.Int64(); i += int64(s.epochLength) {
		blockNum := big.NewInt(i)

		list, err := s.client.GetRootNodeApprovalList(ctx, blockNum, nil)
		if err != nil {
			s.log.WithError(err).WithFields(logan.F{
				"block_to_process": i,
				"start_block":      start,
				"remote_block":     remote.Number.Int64(),
			}).Error("failed to get root node approvals from q node")
			if err == context.Canceled {
				return err
			}
			continue
		}
		if list != nil {
			approvalLists = append(approvalLists, *list...)
		}

		// Process lists every epoch
		if i%int64(s.epochLength) == 0 && len(approvalLists) != 0 {
			var signers []common.Address
			var purposes []*big.Int

			for _, approval := range approvalLists {
				signers = append(signers, approval.Signer)
				purposes = append(purposes, vars.RNOperationPurpose)
			}

			mainAccounts := signers
			aliasesProvider := s.contractRegistry.AccountAliases(&bind.CallOpts{
				BlockNumber: blockNum,
			})
			if aliasesProvider != nil {
				mainAccounts, err = aliasesProvider.ResolveBatchReverse(&bind.CallOpts{
					BlockNumber: blockNum,
				}, signers, purposes)
				if err != nil {
					s.log.WithError(err).Error("failed to resolve aliases")
				}
			}

			approvals := make([]data.RootNodeApproval, 0, len(*list))
			for index, appr := range *list {
				approval := data.RootNodeApproval{
					Signature: appr.Signature,
					Block:     appr.BlockNumber.Uint64(),
					Signer:    strings.ToLower(mainAccounts[index].String()),
				}
				if mainAccounts[index] != signers[index] {
					alias := strings.ToLower(signers[index].String())
					approval.Alias = &alias
				}

				approvals = append(approvals, approval)
			}

			err := db.RootNodeApprovals().Insert(approvals...)
			if err != nil {
				s.log.WithError(err).Error("failed to insert root node approval")
			}

			approvalLists = approvalLists[:0]
		}
	}

	return nil
}

func (s *Service) processBlock(ctx context.Context, qBlock *types.Block, ootstats *indexer.OutOfTurnStats) (*data.Block, error) {
	signer, err := recoverBlockSigner(qBlock)
	if err != nil && qBlock.Number().Int64() != 0 { // not relevant for genesis
		s.log.WithError(err).WithFields(logan.F{
			"number": qBlock.NumberU64(),
			"hash":   qBlock.Hash().Hex(),
		}).Warn("failed to recover block's signature")
	}
	block := &data.Block{
		Number:       qBlock.NumberU64(),
		Hash:         qBlock.Hash().Hex(),
		Parent:       qBlock.ParentHash().Hex(),
		Signer:       signer,
		MainAccount:  strings.ToLower(ootstats.MainAccount.String()),
		Timestamp:    int64(qBlock.Time()),
		InTurnSigner: strings.ToLower(ootstats.InTurnSigner.String()),
		Difficulty:   ootstats.Difficulty.Int64(),
	}
	if err := s.checkVoluntaryExit(ctx, block.Number); err != nil {
		s.log.WithError(err).Error("failed to check voluntary exit")
	}
	db := s.db.Clone()
	if err := db.ExecInTx(func() error {
		if err := db.Blocks().Insert(block); err != nil {
			return err
		}

		var (
			txs      []data.Transaction
			payments []data.Payment
		)

		for _, tx := range qBlock.Transactions() {
			msg, err := guessSigner(tx)
			if err != nil {
				if isSystemTx(s.chainID, tx) {
					s.log.WithField("hash", tx.Hash().Hex()).Debug("skipping system tx")
					continue
				}

				return errors.Wrap(err, "failed to make signer", logan.F{"hash": tx.Hash().Hex()})
			}

			var to *string
			if tx.To() != nil {
				str := strings.ToLower(tx.To().Hex())
				to = &str
			}

			tx := data.Transaction{
				Hash:      tx.Hash().Hex(),
				Block:     block.Number,
				Sender:    strings.ToLower(msg.From().Hex()),
				Recipient: to,
				Value:     data.NewBigInt(tx.Value()),
				Nonce:     tx.Nonce(),
				GasPrice:  data.NewBigInt(tx.GasPrice()),
				Gas:       tx.Gas(),
				Input:     tx.Data(),
				Timestamp: int64(qBlock.Time()),
			}
			txs = append(txs, tx)

			payments = parsePayment(&tx, payments)

		}

		if err := db.Transactions().Insert(txs...); err != nil {
			return errors.Wrap(err, "failed to insert txs into db")
		}

		if err := db.Payments().Insert(payments...); err != nil {
			return errors.Wrap(err, "failed to insert payments into db")
		}

		if qBlock.NumberU64() > uint64(s.ignoreNotificationBlock) {
			go func() {
				for _, tx := range txs {
					s.notifier.NewTransaction(&tx)
				}
			}()

			go func() {
				for _, pmnt := range payments {
					s.notifier.NewPayment(&pmnt)
				}
			}()
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return block, nil
}

func (s *Service) checkVoluntaryExit(ctx context.Context, blockNumber uint64) error {
	currentApprovedNodes, err := s.getNodesByBlock(ctx, blockNumber)
	if err != nil {
		return errors.Wrap(err, "failed to get current approved nodes")
	}

	s.approvedNodesMutex.RLock()
	previousApprovedNodes := s.lastApprovedNodes
	lastCheckedBlock := s.lastCheckedBlock
	s.approvedNodesMutex.RUnlock()

	if lastCheckedBlock != blockNumber-1 {
		previousApprovedNodes, err = s.getNodesByBlock(ctx, blockNumber-1)
		if err != nil {
			return errors.Wrap(err, "failed to get previous approved nodes")
		}
	}

	exitedNodes := make([]common.Address, 0, len(previousApprovedNodes))
	for node := range previousApprovedNodes {
		if _, stillApproved := currentApprovedNodes[node]; !stillApproved {
			exitedNodes = append(exitedNodes, node)
		}
	}

	if len(exitedNodes) > 0 {
		if err := s.handleVoluntaryExit(ctx, exitedNodes, blockNumber); err != nil {
			return errors.Wrap(err, "failed to handle voluntary exit")
		}
	}

	s.approvedNodesMutex.Lock()
	s.lastApprovedNodes = currentApprovedNodes
	s.lastCheckedBlock = blockNumber
	s.approvedNodesMutex.Unlock()

	return nil
}

func (s *Service) getNodesByBlock(ctx context.Context, blockNumber uint64) (map[common.Address]bool, error) {
	roots := s.contractRegistry.Roots()
	if roots == nil {
		return nil, errors.New("Failed to create roots contract instance")
	}

	callOpts := &bind.CallOpts{
		BlockNumber: big.NewInt(int64(blockNumber)),
		Context:     ctx,
	}

	members, err := roots.GetMembers(callOpts)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get members from Roots contract")
	}

	approvedNodes := make(map[common.Address]bool)
	for _, member := range members {
		approvedNodes[member] = true
	}

	return approvedNodes, nil
}

func (s *Service) parseDelegations(contractAddress string, sender common.Address, tx *types.Transaction, toAdd []data.Delegation, toDelete []data.Delegation) ([]data.Delegation, []data.Delegation) {
	method := erc20.QVaultABI.Methods["delegateStake"]
	if tx.To() == nil ||
		strings.ToLower(tx.To().String()) != contractAddress ||
		!bytes.HasPrefix(tx.Data(), method.ID) {
		return toAdd, toDelete
	}

	receipt, err := s.client.TransactionReceipt(context.Background(), common.HexToHash(tx.Hash().String()))
	if receipt.Status == types.ReceiptStatusFailed {
		return toAdd, toDelete
	}

	input := bytes.TrimPrefix(tx.Data(), method.ID)
	args, err := method.Inputs.UnpackValues(input)
	if err != nil {
		return toAdd, toDelete
	}

	if len(args) != 2 {
		return toAdd, toDelete
	}
	addresses, ok := args[0].([]common.Address)
	validators := make([]string, len(addresses))
	for i, address := range addresses {
		validators[i] = address.String()
	}
	if !ok {
		return toAdd, toDelete
	}
	amounts := args[1].([]*big.Int)
	if !ok {
		return toAdd, toDelete
	}

	if len(validators) != len(amounts) {
		return toAdd, toDelete
	}

	for i, validator := range validators {
		if len(amounts[i].Bits()) == 0 {
			toDelete = append(toDelete, data.Delegation{Validator: validator, Delegator: sender.String()})
			continue
		}
		toAdd = append(toAdd, data.Delegation{Validator: validator, Delegator: sender.String()})
	}
	return toAdd, toDelete
}

func parsePayment(tx *data.Transaction, payments []data.Payment) []data.Payment {
	// contract creation
	if tx.Recipient == nil {
		return payments
	}

	if tx.Value.Unwrap().Cmp(big.NewInt(0)) != 0 {
		return append(payments, data.Payment{
			TxHash:    tx.Hash,
			Sender:    tx.Sender,
			Recipient: *tx.Recipient,
			Value:     tx.Value,
			Timestamp: tx.Timestamp,
			Tx:        tx,
		})
	}

	transfer, err := erc20.ParseTransfer(tx.Input)
	if err != nil {
		// it's just some other call then
		return payments
	}

	return append(payments, data.Payment{
		TxHash:    tx.Hash,
		Sender:    tx.Sender,
		Recipient: transfer.To,
		Value:     data.NewBigInt(transfer.Amount),
		// it's contract address
		Token:     tx.Recipient,
		Timestamp: tx.Timestamp,
		Tx:        tx,
	})
}

func (s *Service) handleReorg(ctx context.Context, latest uint64) error {
	// find common block
	var commonIdx uint64
	for idx := latest; idx >= 0; idx-- {
		local, err := s.db.Blocks().GetByNumber(idx)
		if err != nil {
			return errors.Wrap(err, "failed to get block from db")
		}

		remote, err := s.client.HeaderByNumber(ctx, new(big.Int).SetUint64(idx))
		if err != nil {
			return errors.Wrap(err, "failed to get block from q client")
		}

		if remote.Hash().Hex() != local.Hash {
			if idx == 0 {
				panic("genesis mismatch")
			}

			continue
		}

		commonIdx = idx
		break
	}

	s.log.Infof("rewinding history to block number %d", commonIdx)
	return errors.Wrap(
		s.db.Blocks().Erase(commonIdx+1),
		"failed to rollback to latest common state",
	)
}

func isReorg(lastKnown *data.Block, next *types.Block) bool {
	// 'next' should be genesis in this case
	if lastKnown == nil {
		if next.Number().Uint64() != 0 {
			panic("expected genesis block on fresh state")
		}

		return false
	}

	return next.Header().ParentHash.Hex() != lastKnown.Hash
}

func guessSigner(tx *types.Transaction) (types.Message, error) {
	signers := []types.Signer{
		types.NewEIP155Signer(tx.ChainId()),
		types.NewLondonSigner(tx.ChainId()),
		types.HomesteadSigner{},
		types.FrontierSigner{},
	}

	for _, sig := range signers {
		msg, err := tx.AsMessage(sig, big.NewInt(0))
		if err == nil {
			return msg, nil
		}
	}

	return types.Message{}, errors.New("failed to guess signer of tx")
}

// tmp hack because of q-client bag (?) with system tx signature
func isSystemTx(chainID *big.Int, tx *types.Transaction) bool {
	if tx.ChainId().Cmp(big.NewInt(0)) == 0 {
		return false
	}

	return chainID.Cmp(tx.ChainId()) != 0
}

func recoverBlockSigner(block *types.Block) (string, error) {
	header := block.Header()
	extra := header.Extra
	if len(extra) < crypto.SignatureLength {
		return "", errors.New("missing signature")
	}

	sig := extra[len(extra)-crypto.SignatureLength:]
	pubkey, err := crypto.SigToPub(sealHash(header).Bytes(), sig)
	if err != nil {
		return "", errors.Wrap(err, "failed to recover signer")
	}

	return strings.ToLower(crypto.PubkeyToAddress(*pubkey).Hex()), nil
}
