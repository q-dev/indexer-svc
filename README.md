# indexer-svc
This service is a [q-client](https://gitlab.com/q-dev/q-client) transactions indexer.

## Documentation 
Swagger documentation can be accessed at the endpoint `/swagger/index.html`.

In order to generate documentation use:

```
swag init -g internal/services/api/api.go
```

## Starting up

```
cp config.yaml.example config.yaml
# Then check config variables and start docker dompose
docker-compose up -d
```

## License
Indexer is released under the [LGPLv3 License](./LICENSE)
