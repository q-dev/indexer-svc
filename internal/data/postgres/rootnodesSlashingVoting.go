package postgres

import (
	"database/sql"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// RootnodesSlashingVotingStorage implements data.RootnodesSlashingVotingStorage.
type RootnodesSlashingVotingStorage struct {
	db *pgdb.DB
}

var (
	rSVotingColumns = []string{
		"proposal_id",
		"voting_type",
		"voting_status",
		"current_quorum",
		"required_quorum",
		"vetos_number",
		"candidate",
		"cast(extract(epoch from voting_end_time) as integer) as voting_end_time",
		"cast(extract(epoch from voting_start_time) as integer) as voting_start_time",
		"cast(extract(epoch from veto_end_time) as integer) as veto_end_time",
	}
)

func (s *RootnodesSlashingVotingStorage) Insert(votings ...data.RootnodesSlashingVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert("rootnodes_slashing_votings").Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_quorum",
		"required_quorum",
		"root_nodes_number",
		"vetos_number",
		"voting_end_time",
		"veto_end_time",
		"candidate",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentQuorum,
			voting.RequiredQuorum,
			voting.RootNodesNumber,
			voting.VetosNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
			time.Unix(int64(voting.VetoEndTime), 0),
			voting.Candidate,
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_quorum=EXCLUDED.current_quorum," +
		"root_nodes_number=EXCLUDED.root_nodes_number," +
		"vetos_number=EXCLUDED.vetos_number",
	)

	return errors.Wrap(s.db.Exec(query), "failed to insert rootnodes slashing voting")
}

// GetLatest rootnodes slashing voting.
func (s *RootnodesSlashingVotingStorage) GetLatest() (*data.RootnodesSlashingVoting, error) {
	return s.queryBlock(squirrel.Select(rSVotingColumns...).From("rootnodes_slashing_votings").
		Where("proposal_id = (SELECT max(proposal_id) from rootnodes_slashing_votings)"))
}

func (s *RootnodesSlashingVotingStorage) GetEarliestUnfinishedVotings() ([]data.RootnodesSlashingVoting, error) {
	var votings []data.RootnodesSlashingVoting
	query := squirrel.Select(rSVotingColumns...).From("rootnodes_slashing_votings").
		Where("voting_status IN (1, 3, 4)").OrderBy("proposal_id ASC")
	if err := s.db.Select(&votings, query); err != nil {
		return nil, errors.Wrap(err, "failed to select rootnodes slashing votings")
	}
	return votings, nil
}

func (s *RootnodesSlashingVotingStorage) Update(voting *data.RootnodesSlashingVoting) error {
	query := squirrel.Update("rootnodes_slashing_votings").SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_quorum":    voting.CurrentQuorum,
		"root_nodes_number": voting.RootNodesNumber,
		"vetos_number":      voting.VetosNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query), "failed to update rootnodes slashing voting")
}

func (s *RootnodesSlashingVotingStorage) queryBlock(query squirrel.SelectBuilder) (*data.RootnodesSlashingVoting, error) {
	var voting data.RootnodesSlashingVoting
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get rootnodes slashing voting")
	}
}
