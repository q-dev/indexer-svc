package resources

import (
	"encoding/json"
)

type Key struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}

type Relationship struct {
	Data Key `json:"data"`
}

type RelationMap map[Key]interface{}

func (m RelationMap) MarshalJSON() ([]byte, error) {
	var included []interface{}
	for _, obj := range m {
		included = append(included, obj)
	}

	return json.Marshal(included)
}
