package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	migrate "github.com/rubenv/sql-migrate"
	"github.com/urfave/cli/v2"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/assets"
	"gitlab.com/q-dev/indexer-svc/internal/config"
	"gitlab.com/q-dev/indexer-svc/internal/services/api"
	"gitlab.com/q-dev/indexer-svc/internal/services/ingest"
)

//nolint:gochecknoglobals
var migrations = &migrate.EmbedFileSystemMigrationSource{
	FileSystem: assets.Migrations,
	Root:       "migrations",
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			logan.New().WithError(errors.FromPanic(r)).Fatal("app paniced")
		}
	}()

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	app := initApp()
	if err := app.RunContext(ctx, os.Args); err != nil {
		logan.New().WithError(err).Fatal("app failed")
	}
}

func run(c *cli.Context) error {
	cfg := config.New(kv.MustFromEnv())
	errCh := make(chan error)

	go func() {
		child, cancel := context.WithCancel(c.Context)
		defer cancel()

		ingester, err := ingest.New(child, cfg)
		if err != nil {
			panic(err)
		}

		err = ingester.Run(child)
		if err != nil {
			cfg.Log().WithError(err).Error("Ingester returned error")
			errCh <- err
		}
		cfg.Log().Warn("Ingester exited without error")
	}()

	// run server in another goroutine (for gracefully shutting down, see below)
	go func() {
		srv := api.New(cfg)
		errCh <- errors.Wrap(srv.Run(cfg.Listener()), "failed to start api")
	}()

	select {
	case <-c.Done():
		cfg.Log().WithError(c.Err()).Info("Interrupt signal received, waiting 5 seconds...")
		time.Sleep(5 * time.Second)
		return nil
	case err := <-errCh:
		return errors.Wrap(err, "failed to run server")
	}
}

func migrateCmd(dir migrate.MigrationDirection) func(ctx *cli.Context) error {
	return func(c *cli.Context) error {
		cfg := config.New(kv.MustFromEnv())

		count, err := migrate.Exec(cfg.RawDB(), "postgres", migrations, dir)
		if err != nil {
			return errors.Wrap(err, "failed to run migrations")
		}

		cfg.Log().WithField("count", count).
			Info("applied migrations")

		return nil
	}
}

func initApp() *cli.App {
	return &cli.App{
		Usage:  "run indexer service",
		Action: run,
		Commands: []*cli.Command{
			{
				Name:  "migrate",
				Usage: "run db migration",
				Subcommands: []*cli.Command{
					{
						Name:   "up",
						Usage:  "apply new migrations",
						Action: migrateCmd(migrate.Up),
					},
					{
						Name:   "down",
						Usage:  "rollback migrations",
						Action: migrateCmd(migrate.Down),
					},
				},
			},
		},
	}
}
