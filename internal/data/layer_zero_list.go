package data

import (
	"database/sql"
	"time"
)

const (
	ListTypeExclusion = "exclusion-list"
	ListTypeRoot      = "root-list"
)

type NodeAccountPair struct {
	Alias       string         `db:"node"`
	MainAccount sql.NullString `db:"main_account"`
}

type LayerZeroList struct {
	Hash      string
	Timestamp time.Time
	Type      string
	Status    string
	Signers   []NodeAccountPair
}

type NodeExclusion struct {
	NodeAccountPair
	StartBlock uint64
	EndBlock   sql.NullInt64
}

type NodeExclusionList struct {
	LayerZeroList
	ExclusionSet []NodeExclusion
}

type RootNodeList struct {
	LayerZeroList
	RootList []NodeAccountPair
}

type LayerZeroListStorage interface {
	GetExclusionList(status string) (NodeExclusionList, error)
	GetRootList(status string) (RootNodeList, error)

	GetCurrentRoots() ([]NodeAccountPair, error)

	SaveRootList(list RootNodeList) error
	SaveExclusionList(list NodeExclusionList) error

	SaveNodeAliases(nodes []NodeAccountPair) error

	OutdateList(listType, hash string) error

	Tx(fn func() error) error
}
