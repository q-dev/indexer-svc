package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/helpers"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/requests"
)

type systemContractsHandler struct {
	log     *logan.Entry
	storage data.SystemContractsStorage
}

func NewSystemContractsHandler(log *logan.Entry, storage data.SystemContractsStorage) systemContractsHandler {
	return systemContractsHandler{
		log:     log,
		storage: storage,
	}
}

// @Summary   	Get system contracts
// @Description Returns system contracts and their implementations.
// @ID       	system-contracts
// @Accept    	json
// @Produce    	json
// @Param     	filter[contracts] query int false "Filter system contracts by proxy addresses."
// @Param     	filter[keys] query int false "Filter by system contract keys."
// @Param     	sort query string false "Sorting order. Default - `desc`"
// @Param     	page[cursor] query int false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.SystemContractList
// @Failure   	400 "Bad request"
// @Failure   	500 "Internal Server Error"
// @Router     	/system-contracts [get]
func (h *systemContractsHandler) GetSystemContract(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetSystemContractListRequest(r)
	if err != nil {
		ape.Render(w, problems.BadRequest(err))
		return
	}

	systemContracts, err := h.storage.Select(data.SelectSysContractsParams{
		IsDesc:    request.Sort == "desc",
		Cursor:    request.Cursor,
		Limit:     request.Limit,
		Contracts: request.Contracts,
		Keys:      request.Keys,
	})
	if err != nil {
		ape.Render(w, problems.InternalError())
		h.log.WithError(err).Error("failed to query the DB")
		return
	}

	resp := resources.PopulateSysContractsList(systemContracts)
	resp.Links, err = helpers.GetSystemContractsLinks(systemContracts, request, *r.URL)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to build links")
		return
	}

	ape.Render(w, resp)
}
