package data

type Payment struct {
	TxHash    string  `db:"tx_hash"`
	Sender    string  `db:"sender"`
	Recipient string  `db:"recipient"`
	Value     *BigInt `db:"value"`

	Token *string `db:"token"`

	Timestamp int64 `db:"timestamp"`

	// comes from 'includes', not from db
	Tx *Transaction
}

type SelectPaymentsParams struct {
	Cursor *string
	Limit  uint64
	IsDesc bool

	Sender    *string
	Recipient *string

	// Counterparty is either Sender or Recipient
	// and mutually exclusive to those filters
	Counterparty *string

	Token    *string
	IsNative *bool
}

type PaymentsStorage interface {
	Insert(payments ...Payment) error
	Select(params *SelectPaymentsParams) ([]Payment, error)
}
