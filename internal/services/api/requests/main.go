package requests

const (
	defaultListLimit = uint64(20)
	maxListLimit     = uint64(100)
)
