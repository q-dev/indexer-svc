package ingest

import (
	"context"
	"math/big"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/q-dev/q-client/contracts"
	"gitlab.com/q-dev/q-client/params"
	"gitlab.com/q-dev/system-contracts/generated"

	"gitlab.com/distributed_lab/logan/v3/errors"
	ethereum "gitlab.com/q-dev/q-client"
	"gitlab.com/q-dev/q-client/accounts/abi/bind"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/core/types"
	"gitlab.com/q-dev/q-client/crypto"
	"gitlab.com/q-dev/q-client/indexer"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

var VotingEvents = map[string]int64{
	"qTokenHolderVote": 0,
	"rootNodeVote":     1,
	"rootNodeProposal": 2,
}

const maxBlocksPerRequest = 1000

var votingDecimals = new(big.Int).Exp(big.NewInt(10), big.NewInt(27), nil)

func calculateMajority(votesCount, rootNodesCount *big.Int) *big.Int {
	// (votesCount * 10^27) / rootNodesCount
	return new(big.Int).Div(new(big.Int).Mul(votesCount, votingDecimals), rootNodesCount)
}

type concurrentFunc func(context.Context, uint64, uint64) error

type Provider interface {
	GetStatus(opts *bind.CallOpts, _id *big.Int) (uint8, error)
	GetProposal(opts *bind.CallOpts, _id *big.Int) (generated.IVotingBaseProposal, error)
	GetProposalStats(opts *bind.CallOpts, _proposalId *big.Int) (generated.IVotingVotingStats, error)
}

func (s *Service) updateVotings(ctx context.Context, latest uint64, timestamp uint64) error {
	var wg sync.WaitGroup

	s.executeConcurrentVotingFunc(s.updateConstitutionVotings, ctx, latest, timestamp, &wg, "failed to update constitution votings")
	s.executeConcurrentVotingFunc(s.updateGeneralUpdateVotings, ctx, latest, timestamp, &wg, "failed to update general update votings")
	s.executeConcurrentVotingFunc(s.updateEmergencyUpdateVotings, ctx, latest, timestamp, &wg, "failed to update emergency update votings")
	s.executeConcurrentVotingFunc(s.updateRootsVotings, ctx, latest, timestamp, &wg, "failed to update root nodes membership votings")
	s.executeConcurrentVotingFunc(s.updateRootnodesSlashingVotings, ctx, latest, timestamp, &wg, "failed to update root nodes slashing votings")
	s.executeConcurrentVotingFunc(s.updateValidatorsSlashingVotings, ctx, latest, timestamp, &wg, "failed to update validators slashing votings")
	s.executeConcurrentVotingFunc(s.updateEpqfiMembershipVotings, ctx, latest, timestamp, &wg, "failed to update epqfi membership votings")
	s.executeConcurrentVotingFunc(s.updateEpqfiParametersVotings, ctx, latest, timestamp, &wg, "failed to update epqfi parameters votings")
	s.executeConcurrentVotingFunc(s.updateEpdrMembershipVotings, ctx, latest, timestamp, &wg, "failed to update epdr membership votings")
	s.executeConcurrentVotingFunc(s.updateEpdrParametersVotings, ctx, latest, timestamp, &wg, "failed to update epdr parameters votings")
	s.executeConcurrentVotingFunc(s.updateEprsMembershipVotings, ctx, latest, timestamp, &wg, "failed to update eprs membership votings")
	s.executeConcurrentVotingFunc(s.updateEprsParametersVotings, ctx, latest, timestamp, &wg, "failed to update eprs parameters votings")
	s.executeConcurrentVotingFunc(s.updateContractRegistryAddressVotings, ctx, timestamp, latest, &wg, "failed to update contract registry address votings")
	s.executeConcurrentVotingFunc(s.updateContractRegistryUpgradeVotings, ctx, timestamp, latest, &wg, "failed to update contract registry upgrade votings")
	s.executeConcurrentVotingFunc(s.updateContractRegistryVotings, ctx, timestamp, latest, &wg, "failed to update contract registry votings")

	wg.Wait()

	return nil
}

func (s *Service) processVotings(ctx context.Context, latest uint64, timestamp uint64) error {
	var wg sync.WaitGroup

	err := s.insertVotingAgents(ctx)
	if err != nil {
		return err
	}

	s.executeConcurrentVotingFunc(s.insertConstitutionVotingBatch, ctx, latest, timestamp, &wg, "failed to process constitution votings")
	s.executeConcurrentVotingFunc(s.insertGeneralUpdateVotingBatch, ctx, latest, timestamp, &wg, "failed to process general update votings")
	s.executeConcurrentVotingFunc(s.insertEmergencyUpdateVotingBatch, ctx, latest, timestamp, &wg, "failed to process emergency update votings")
	s.executeConcurrentVotingFunc(s.insertRootsVotingBatch, ctx, latest, timestamp, &wg, "failed to process root nodes membership votings")
	s.executeConcurrentVotingFunc(s.insertRootnodesSlashingVotingBatch, ctx, latest, timestamp, &wg, "failed to process root nodes slashing votings")
	s.executeConcurrentVotingFunc(s.insertValidatorsSlashingVotingBatch, ctx, latest, timestamp, &wg, "failed to process validators slashing votings")
	s.executeConcurrentVotingFunc(s.insertEpqfiMembershipVotingBatch, ctx, latest, timestamp, &wg, "failed to process epqfi membership votings")
	s.executeConcurrentVotingFunc(s.insertEpqfiParametersVotingBatch, ctx, latest, timestamp, &wg, "failed to process epqfi parameters votings")
	s.executeConcurrentVotingFunc(s.insertEpdrMembershipVotingBatch, ctx, latest, timestamp, &wg, "failed to process epdr membership votings")
	s.executeConcurrentVotingFunc(s.insertEpdrParametersVotingBatch, ctx, latest, timestamp, &wg, "failed to process epdr parameters votings")
	s.executeConcurrentVotingFunc(s.insertEprsMembershipVotingBatch, ctx, latest, timestamp, &wg, "failed to process eprs membership votings")
	s.executeConcurrentVotingFunc(s.insertEprsParametersVotingBatch, ctx, latest, timestamp, &wg, "failed to process eprs parameters votings")
	s.executeConcurrentVotingFunc(s.insertContractRegistryAddressVotingBatch, ctx, latest, timestamp, &wg, "failed to process contract registry address votings")
	s.executeConcurrentVotingFunc(s.insertContractRegistryUpgradeVotingBatch, ctx, latest, timestamp, &wg, "failed to process contract registry upgrade votings")
	s.executeConcurrentVotingFunc(s.insertContractRegistryVotingBatch, ctx, latest, timestamp, &wg, "failed to process contract registry votings")

	wg.Wait()

	return nil
}

func (s *Service) executeConcurrentVotingFunc(fn concurrentFunc, ctx context.Context, latest uint64, timestamp uint64, wg *sync.WaitGroup, errText string) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		err := fn(ctx, latest, timestamp)
		if err != nil {
			s.log.WithError(err).Error(errText)
		}
	}()
}

func (s *Service) getPanelSizeByBlock(ctx context.Context, blockNumber uint64) (uint64, error) {
	panelSize, err := s.contractRegistry.Roots().GetSize(&bind.CallOpts{
		BlockNumber: big.NewInt(int64(blockNumber)),
		Context:     ctx,
	})
	if err != nil {
		return 0, err
	}
	return panelSize.Uint64(), nil
}

func (s *Service) searchBlockByTimestamp(ctx context.Context, targetTimestamp int64) (uint64, error) {
	latestBlock, err := s.client.HeaderByNumber(ctx, nil)
	if err != nil {
		return 0, errors.Wrap(err, "failed to get latest block")
	}

	latestBlockTime := latestBlock.Time
	avgBlockTime := latestBlockTime / (latestBlock.Number.Uint64() - 1)
	low, high := uint64(targetTimestamp/(int64(avgBlockTime)+1)), min(latestBlock.Number.Uint64(), uint64(targetTimestamp/(int64(avgBlockTime)-1)))

	for low <= high {
		select {
		case <-ctx.Done():
			return 0, ctx.Err()
		default:
			mid := (low + high) / 2
			block, err := s.client.HeaderByNumber(ctx, big.NewInt(int64(mid)))
			if err != nil {
				return 0, errors.Wrap(err, "failed to get block")
			}
			blockTime := int64(block.Time)
			if blockTime == targetTimestamp {
				return mid, nil
			} else if blockTime < targetTimestamp {
				low = mid + 1
			} else {
				high = mid - 1
			}
		}
	}

	return high, nil
}

func (s *Service) getPanelSize(ctx context.Context, timestamp int64) (uint64, error) {
	blockNumber, err := s.searchBlockByTimestamp(ctx, timestamp)
	if err != nil {
		return 0, errors.Wrap(err, "failed to find block by timestamp")
	}

	size, err := s.getPanelSizeByBlock(ctx, blockNumber)
	if err != nil {
		return 0, errors.Wrap(err, "failed to get panel size")
	}

	return size, nil
}

func (s *Service) insertVotingAgents(ctx context.Context) error {
	db := s.db.Clone()

	addr := s.contractRegistry.GetAddr("governance.votingWeightProxy")
	if (addr != common.Address{}) {
		latestBlock, err := db.VotingAgents().GetLatestBlock()
		if err != nil {
			return errors.Wrap(err, "failed to get latest voting agent block")
		}

		logs, err := s.client.FilterLogs(ctx, ethereum.FilterQuery{
			FromBlock: big.NewInt(latestBlock + 1),
			Addresses: []common.Address{addr},
			Topics:    [][]common.Hash{{crypto.Keccak256Hash([]byte("VotingAgentChanged(address,address,uint256)"))}},
		})
		if err != nil {
			return err
		}

		agents, err := s.parseVotingAgentLogs(logs)
		if err != nil {
			return errors.Wrap(err, "failed to parse voting agent changed logs")
		}

		err = db.VotingAgents().Insert(agents...)
		if err != nil {
			return errors.Wrap(err, "failed to save voting agents")
		}
	}
	return nil
}

func (s *Service) parseVotingAgentLogs(logs []types.Log) ([]data.VotingAgent, error) {
	snapshot := make(map[string][]data.VotingAgent)
	for _, log := range logs {
		event, err := s.contractRegistry.VotingWeightProxy().ParseVotingAgentChanged(log)
		if err != nil {
			return nil, err
		}

		addressKey := strings.ToLower(event.Who.String())
		newAgent := data.VotingAgent{
			Address:     addressKey,
			VotingAgent: strings.ToLower(event.VotingAgent.String()),
			StartBlock:  event.Raw.BlockNumber,
			EndBlock:    0,
		}

		if agents, exists := snapshot[addressKey]; exists {
			lastAgentIndex := len(agents) - 1
			agents[lastAgentIndex].EndBlock = event.Raw.BlockNumber

			agents = append(agents, newAgent)
			snapshot[addressKey] = agents
		} else {
			snapshot[addressKey] = []data.VotingAgent{newAgent}
		}
	}

	result := make([]data.VotingAgent, 0, len(snapshot))
	for _, agents := range snapshot {
		result = append(result, agents...)
	}

	return result, nil
}

func (s *Service) insertConstitutionVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.ConstitutionVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.ConstitutionVoting()
	votingType := "constitution_voting"
	currentProposalCounter, err := provider.ProposalCounter(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.ConstitutionVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		baseVoting, err := s.prepareVoting(ctx, cycleId, provider, timestamp, votingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id": id,
			})
		}

		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(baseVoting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}

		votings = append(votings, data.ConstitutionVoting{
			BaseVoting:    *baseVoting,
			VotingSubType: proposal.Classification,
		})
	}

	err = db.ConstitutionVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed constitution votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["qTokenHolderVote"],
		"governance.constitution.parametersVoting", "UserVoted(uint256,uint8,uint256)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) prepareVoting(
	ctx context.Context,
	id *big.Int,
	provider Provider,
	latestBlockTimestamp uint64,
	votingType string,
) (*data.BaseVoting, error) {
	callOpts := &bind.CallOpts{
		Context: ctx,
	}
	proposalStatus, err := provider.GetStatus(callOpts, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get status")
	}
	proposal, err := provider.GetProposal(callOpts, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get base proposal")
	}
	proposalStats, err := provider.GetProposalStats(callOpts, id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get proposal stats")
	}

	voting := data.BaseVoting{
		ProposalId:      id.Uint64(),
		VotingType:      votingType,
		VotingStatus:    proposalStatus,
		CurrentQuorum:   proposalStats.CurrentQuorum.String(),
		RequiredQuorum:  proposalStats.RequiredQuorum.String(),
		VotingStartTime: proposal.Params.VotingStartTime.Uint64(),
		VotingEndTime:   proposal.Params.VotingEndTime.Uint64(),
		VetoEndTime:     proposal.Params.VetoEndTime.Uint64(),
		VetosNumber:     proposal.Counters.VetosCount.Uint64(),
	}
	if proposal.Params.VotingEndTime.Uint64() < latestBlockTimestamp {
		voting.RootNodesNumber, err = s.getPanelSize(ctx, proposal.Params.VotingEndTime.Int64())
		if err != nil {
			return nil, errors.Wrap(err, "failed to get panel size")
		}
	}

	return &voting, nil
}

func (s *Service) insertGeneralUpdateVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.GeneralUpdateVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.GeneralUpdateVoting()
	votingType := "general_update_voting"
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.GeneralUpdateVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		voting, err := s.prepareVoting(ctx, big.NewInt(id), provider, timestamp, votingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id": id,
			})
		}

		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(voting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, voting.VotingEndTime)
		}
		voting.VotingType = votingType

		votings = append(votings, data.GeneralUpdateVoting(*voting))
	}

	err = db.GeneralUpdateVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed general update votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["qTokenHolderVote"],
		"governance.generalUpdateVoting", "UserVoted(uint256,uint8,uint256)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertEmergencyUpdateVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.EmergencyUpdateVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.EmergencyUpdateVoting()
	votingType := "emergency_update_voting"
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.EmergencyUpdateVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		voting, err := s.prepareVoting(ctx, big.NewInt(id), provider, timestamp, votingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id":          id,
				"voting_type": votingType,
			})
		}

		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(voting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, voting.VotingEndTime)
		}

		votings = append(votings, data.EmergencyUpdateVoting(*voting))
	}

	err = db.EmergencyUpdateVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed emergency update votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeVote"],
		"governance.emergencyUpdateVoting", "UserVoted(uint256,uint8)", provider)
	if err != nil {
		return err
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeProposal"],
		"governance.emergencyUpdateVoting",
		"ProposalCreated(uint256,(string,(uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256),(uint256,uint256,uint256),bool))", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertRootnodesSlashingVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.RootnodesSlashingVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	votingType := "root_nodes_slashing_voting"
	provider := s.contractRegistry.RootNodesSlashingVoting()
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.RootnodesSlashingVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		baseVoting, err := s.prepareVoting(ctx, cycleId, provider, timestamp, votingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id":          id,
				"voting_type": votingType,
			})
		}
		baseVoting.VotingType = votingType // just to make sure we are not breaking compatibility
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}

		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(baseVoting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}

		votings = append(votings, data.RootnodesSlashingVoting{
			BaseVoting: *baseVoting,
			Candidate:  proposal.Candidate.String(),
		})
	}

	err = db.RootnodesSlashingVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed rootnodes slashing votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["qTokenHolderVote"],
		"governance.rootNodes.slashingVoting", "UserVoted(uint256,uint8,uint256)", provider)
	if err != nil {
		return err
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeProposal"],
		"governance.rootNodes.slashingVoting", "ProposalCreated(uint256,(string,(uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256),(uint256,uint256,uint256),bool))", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertValidatorsSlashingVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.ValidatorsSlashingVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.ValidatorsSlashingVoting()
	votingType := "validators_slashing_voting"
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votings := make([]data.ValidatorsSlashingVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		voting := data.ValidatorsSlashingVoting{
			ProposalId:      cycleId.Uint64(),
			VotingType:      votingType,
			VotingStatus:    proposalStatus,
			CurrentQuorum:   proposalStats.CurrentQuorum.String(),
			RequiredQuorum:  proposalStats.RequiredQuorum.String(),
			VotingStartTime: proposal.Base.Params.VotingStartTime.Uint64(),
			VotingEndTime:   proposal.Base.Params.VotingEndTime.Uint64(),
			VetoEndTime:     proposal.Base.Params.VetoEndTime.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}

		votings = append(votings, voting)
	}

	err = db.ValidatorsSlashingVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed validators slashing votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeVote"],
		"governance.validators.slashingVoting", "UserVoted(uint256,uint8)", provider)
	if err != nil {
		return err
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeProposal"],
		"governance.validators.slashingVoting", "ProposalCreated(uint256,(string,(uint256,uint256,uint256,uint256,uint256,uint256,uint256,uint256),(uint256,uint256,uint256),bool))", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertEpqfiMembershipVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.EpqfiMembershipVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.EpqfiMembershipVoting()
	votingType := "epqfi_membership_voting"
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.EpqfiMembershipVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.EpqfiMembershipVoting{
			ProposalId:      cycleId.Uint64(),
			VotingType:      votingType,
			VotingStatus:    proposalStatus,
			CurrentQuorum:   proposalStats.CurrentQuorum.String(),
			RequiredQuorum:  proposalStats.RequiredQuorum.String(),
			VotingStartTime: proposal.Base.Params.VotingStartTime.Uint64(),
			VotingEndTime:   proposal.Base.Params.VotingEndTime.Uint64(),
			VetoEndTime:     proposal.Base.Params.VetoEndTime.Uint64(),
			VetosNumber:     proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}

		votings = append(votings, voting)
	}

	err = db.EpqfiMembershipVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed epqfi membership votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["qTokenHolderVote"],
		"governance.experts.EPQFI.membershipVoting", "UserVoted(uint256,uint8,uint256)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertEpqfiParametersVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.EpqfiParametersVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.EpqfiParametersVoting()
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.EpqfiParametersVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.EpqfiParametersVoting{
			ProposalId:      cycleId.Uint64(),
			VotingType:      "epqfi_parameters_voting",
			VotingStatus:    proposalStatus,
			CurrentQuorum:   proposalStats.CurrentQuorum.String(),
			RequiredQuorum:  proposalStats.RequiredQuorum.String(),
			VotingStartTime: proposal.Base.Params.VotingStartTime.Uint64(),
			VotingEndTime:   proposal.Base.Params.VotingEndTime.Uint64(),
			VetoEndTime:     proposal.Base.Params.VetoEndTime.Uint64(),
			VetosNumber:     proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}

		votings = append(votings, voting)
	}

	err = db.EpqfiParametersVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed epqfi parameters votings")
	}

	return nil
}

func (s *Service) insertEprsMembershipVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.EprsMembershipVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.EprsMembershipVoting()
	votingType := "eprs_membership_voting"
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.EprsMembershipVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.EprsMembershipVoting{
			ProposalId:      cycleId.Uint64(),
			VotingType:      votingType,
			VotingStatus:    proposalStatus,
			CurrentQuorum:   proposalStats.CurrentQuorum.String(),
			RequiredQuorum:  proposalStats.RequiredQuorum.String(),
			VotingStartTime: proposal.Base.Params.VotingStartTime.Uint64(),
			VotingEndTime:   proposal.Base.Params.VotingEndTime.Uint64(),
			VetoEndTime:     proposal.Base.Params.VetoEndTime.Uint64(),
			VetosNumber:     proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}

		votings = append(votings, voting)
	}

	err = db.EprsMembershipVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed eprs membership votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["qTokenHolderVote"],
		"governance.experts.EPRS.membershipVoting", "UserVoted(uint256,uint8,uint256)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertEprsParametersVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.EprsParametersVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.EprsParametersVoting()
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votingType := "eprs_parameters_voting"

	votings := make([]data.EprsParametersVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		voting, err := s.prepareVoting(ctx, big.NewInt(id), provider, timestamp, votingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id": id,
			})
		}
		voting.VotingType = votingType // just to make sure we are not changing old names
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(voting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, voting.VotingEndTime)
		}

		votings = append(votings, data.EprsParametersVoting(*voting))
	}

	err = db.EprsParametersVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed eprs parameters votings")
	}

	return nil
}

func (s *Service) insertEpdrMembershipVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.EpdrMembershipVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.EpdrMembershipVoting()
	votingType := "epdr_membership_voting"
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.EpdrMembershipVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.EpdrMembershipVoting{
			ProposalId:      cycleId.Uint64(),
			VotingType:      votingType,
			VotingStatus:    proposalStatus,
			CurrentQuorum:   proposalStats.CurrentQuorum.String(),
			RequiredQuorum:  proposalStats.RequiredQuorum.String(),
			VotingStartTime: proposal.Base.Params.VotingStartTime.Uint64(),
			VotingEndTime:   proposal.Base.Params.VotingEndTime.Uint64(),
			VetoEndTime:     proposal.Base.Params.VetoEndTime.Uint64(),
			VetosNumber:     proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}

		votings = append(votings, voting)
	}

	err = db.EpdrMembershipVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed epdr membership votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["qTokenHolderVote"],
		"governance.experts.EPDR.membershipVoting", "UserVoted(uint256,uint8,uint256)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertEpdrParametersVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.EpdrParametersVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.EpdrParametersVoting()
	currentProposalCounter, err := provider.ProposalCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.EpdrParametersVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.EpdrParametersVoting{
			ProposalId:      cycleId.Uint64(),
			VotingType:      "epdr_parameters_voting",
			VotingStatus:    proposalStatus,
			CurrentQuorum:   proposalStats.CurrentQuorum.String(),
			RequiredQuorum:  proposalStats.RequiredQuorum.String(),
			VotingStartTime: proposal.Base.Params.VotingStartTime.Uint64(),
			VotingEndTime:   proposal.Base.Params.VotingEndTime.Uint64(),
			VetoEndTime:     proposal.Base.Params.VetoEndTime.Uint64(),
			VetosNumber:     proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}

		votings = append(votings, voting)
	}

	err = db.EpdrParametersVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed epdr parameters votings")
	}

	return nil
}

func (s *Service) insertContractRegistryAddressVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.ContractRegistryAddressVotings().GetLatestId()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = *latestVoting
	}

	provider := s.contractRegistry.ContractRegistryAddressVoting()
	if provider == nil {
		s.log.Warn("Contract registry address voting is no longer supported")
		return nil
	}
	votingType := "address_voting"
	currentProposalCounter, err := provider.ProposalsCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.ContractRegistryVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.GetProposal(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		votesCount, err := provider.VoteCount(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.VotingExpiredTime.Uint64())
		}
		voting := data.ContractRegistryVoting{
			ProposalId:       cycleId.Uint64(),
			VotingType:       votingType,
			VotingStatus:     proposalStatus,
			CurrentMajority:  proposalStats.CurrentMajority.String(),
			RequiredMajority: proposalStats.RequiredMajority.String(),
			VotingStartTime:  proposal.VotingStartTime.Uint64(),
			VotingEndTime:    proposal.VotingExpiredTime.Uint64(),
			VotesNumber:      votesCount.Uint64(),
		}
		if proposal.VotingExpiredTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.VotingExpiredTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
			voting.CurrentMajority = calculateMajority(votesCount, big.NewInt(int64(rootNodesNumber))).String()
		}

		votings = append(votings, voting)
	}

	err = db.ContractRegistryAddressVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed contract registry address votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeVote"],
		"governance.address.contractRegistryVoting", "RootNodeApproved(uint256,address)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertContractRegistryUpgradeVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.ContractRegistryUpgradeVotings().GetLatestId()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = *latestVoting
	}

	provider := s.contractRegistry.ContractRegistryUpgradeVoting()
	if provider == nil {
		s.log.Warn("Contract registry upgrade voting is no longer supported")
		return nil
	}
	votingType := "upgrade_voting"
	currentProposalCounter, err := provider.ProposalsCount(nil)
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.ContractRegistryVoting, 0, currentProposalCounter.Int64()-latest)
	for id := latest + 1; id < currentProposalCounter.Int64(); id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return err
		}
		proposal, err := provider.GetProposal(nil, cycleId)
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return err
		}
		votesCount, err := provider.VoteCount(nil, cycleId)
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.VotingExpiredTime.Uint64())
		}
		voting := data.ContractRegistryVoting{
			ProposalId:       cycleId.Uint64(),
			VotingType:       votingType,
			VotingStatus:     proposalStatus,
			CurrentMajority:  proposalStats.CurrentMajority.String(),
			RequiredMajority: proposalStats.RequiredMajority.String(),
			VotingStartTime:  proposal.VotingStartTime.Uint64(),
			VotingEndTime:    proposal.VotingExpiredTime.Uint64(),
			VotesNumber:      votesCount.Uint64(),
		}
		if proposal.VotingExpiredTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.VotingExpiredTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
			voting.CurrentMajority = calculateMajority(votesCount, big.NewInt(int64(rootNodesNumber))).String()
		}

		votings = append(votings, voting)
	}

	err = db.ContractRegistryUpgradeVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter.Int64() - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed contract registry upgrade votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeVote"],
		"governance.upgrade.contractRegistryVoting", "RootNodeApproved(uint256,address)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) insertContractRegistryVotingBatch(ctx context.Context, latestBlockNumber, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.ContractRegistryVotings().GetLatestId()
	if err != nil {
		return errors.Wrap(err, "failed to get latest contract registry voting")
	}

	firstVotingToProcess := int64(0)
	if latestVoting != nil {
		firstVotingToProcess = *latestVoting + 1
	}

	provider := s.contractRegistry.ContractRegistryVoting()
	if provider == nil {
		s.log.WithField("latest_block_number", latestBlockNumber).Warn("ContractRegistryVoting not found")
		return nil
	}

	callOpts := &bind.CallOpts{Context: ctx}
	currentProposalCounter, err := provider.ProposalsCount(callOpts)
	if err != nil {
		return errors.Wrap(err, "failed to get proposal count")
	}

	votingType := "contract_registry_voting"
	votings := make([]data.ContractRegistryVoting, 0, currentProposalCounter.Int64()-firstVotingToProcess)
	for id := firstVotingToProcess; id < currentProposalCounter.Int64(); id++ {
		proposalID := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(callOpts, proposalID)
		if err != nil {
			return errors.Wrap(err, "failed to get status")
		}
		proposal, err := provider.GetProposal(callOpts, proposalID)
		if err != nil {
			return errors.Wrap(err, "failed to get proposal")
		}
		proposalStats, err := provider.GetProposalStats(callOpts, proposalID)
		if err != nil {
			return errors.Wrap(err, "failed to get proposal stats")
		}
		votesCount, err := provider.VoteCount(callOpts, proposalID)
		if err != nil {
			return errors.Wrap(err, "failed to get vote count")
		}

		voting := data.ContractRegistryVoting{
			ProposalId:       uint64(id),
			VotingType:       votingType,
			VotingStatus:     proposalStatus,
			CurrentMajority:  proposalStats.CurrentMajority.String(),
			RequiredMajority: proposalStats.RequiredMajority.String(),
			VotingStartTime:  proposal.VotingStartTime.Uint64(),
			VotingEndTime:    proposal.VotingExpiredTime.Uint64(),
			VotesNumber:      votesCount.Uint64(),
		}
		if voting.VotingEndTime < timestamp {
			voting.RootNodesNumber, err = s.getPanelSize(ctx, int64(voting.VotingEndTime))
			if err != nil {
				return errors.Wrap(err, "failed to get root nodes number")
			}
			voting.CurrentMajority = calculateMajority(votesCount, big.NewInt(int64(voting.RootNodesNumber))).String()
		}

		votings = append(votings, voting)
	}
	// We do not need to handle system contracts updates here, because they were handled during start of the ingester
	err = db.ContractRegistryVotings().Insert(votings...)
	if err != nil {
		return errors.Wrap(err, "failed to insert votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["rootNodeVote"],
		contracts.ContractRegistryVotingKey, "RootNodeApproved(uint256,address)", provider)
	if err != nil {
		return errors.Wrap(err, "failed to insert events")
	}

	return nil
}

func (s *Service) insertRootsVotingBatch(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	latestVoting, err := db.RootsVotings().GetLatest()
	if err != nil {
		return err
	}

	latest := int64(-1)
	if latestVoting != nil {
		latest = int64(latestVoting.ProposalId)
	}

	provider := s.contractRegistry.RootsVoting()
	votingType := "root_nodes_membership_voting"

	currentProposalCounter, err := s.findLastCounter(latestBlockNumber)
	if err != nil {
		return errors.Wrap(err, "failed to find last counter", logan.F{
			"latest_block": latestBlockNumber,
		})
	}

	votingEndBlockTime := uint64(0)

	votings := make([]data.RootsVoting, 0, currentProposalCounter-latest)
	for id := latest + 1; id <= currentProposalCounter; id++ {
		cycleId := big.NewInt(id)
		proposalStatus, err := provider.GetStatus(nil, cycleId)
		if err != nil {
			return errors.Wrap(err, "failed to get proposal status", logan.F{
				"proposal_id": id,
			})
		}
		proposal, err := provider.Proposals(nil, cycleId)
		if err != nil {
			return errors.Wrap(err, "failed to get proposal", logan.F{
				"proposal_id": id,
			})
		}
		proposalStats, err := provider.GetProposalStats(nil, cycleId)
		if err != nil {
			return errors.Wrap(err, "failed to proposal stats", logan.F{
				"proposal_id": id,
			})
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.RootsVoting{
			BaseVoting: data.BaseVoting{
				ProposalId:      cycleId.Uint64(),
				VotingType:      votingType,
				VotingStatus:    proposalStatus,
				CurrentQuorum:   proposalStats.CurrentQuorum.String(),
				RequiredQuorum:  proposalStats.RequiredQuorum.String(),
				VotingStartTime: proposal.Base.Params.VotingStartTime.Uint64(),
				VotingEndTime:   proposal.Base.Params.VotingEndTime.Uint64(),
				VetoEndTime:     proposal.Base.Params.VetoEndTime.Uint64(),
				VetosNumber:     proposal.Base.Counters.VetosCount.Uint64(),
			},
			ReplaceDest: proposal.ReplaceDest.String(),
			Candidate:   proposal.Candidate.String(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return errors.Wrap(err, "failed to get panel size", logan.F{
					"proposal_id": id,
				})
			}
			voting.RootNodesNumber = rootNodesNumber
		}

		votings = append(votings, voting)

		emptyAddress := common.Address{}
		if proposal.Base.Executed {
			if proposal.Candidate != emptyAddress && proposal.ReplaceDest == emptyAddress {
				err := s.newRootMembershipPeriod(ctx, voting)
				if err != nil {
					return err
				}
			} else if proposal.Candidate != emptyAddress && proposal.ReplaceDest != emptyAddress {
				err := s.replaceRootMember(ctx, voting)
				if err != nil {
					return err
				}
			} else if proposal.Candidate == emptyAddress && proposal.ReplaceDest != emptyAddress {
				err := s.removeRootMember(ctx, voting)
				if err != nil {
					return err
				}
			}
		}
	}

	err = db.RootsVotings().Insert(votings...)
	if err != nil {
		return err
	}

	amount := currentProposalCounter - latest - 1
	if amount > 0 {
		s.log.WithField("amount", amount).Info("processed roots votings")
	}

	err = s.insertEvents(ctx, latestBlockNumber, votingType, VotingEvents["qTokenHolderVote"],
		"governance.rootNodes.membershipVoting", "UserVoted(uint256,uint8,uint256)", provider)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) GetStartBlockByEvent(voteOrProposal int64, votingType string) (int64, error) {
	var proposal *data.VotingEvent
	var err error
	switch voteOrProposal {
	case 0:
		proposal, err = s.db.QTokenHolderVotes().GetByLatestByType(votingType)
	case 1:
		proposal, err = s.db.RootNodeVotes().GetByLatestByType(votingType)
	case 2:
		proposal, err = s.db.RootNodeProposals().GetByLatestByType(votingType)
	}
	if err != nil {
		return 0, err
	}
	if proposal != nil {
		return int64(proposal.BlockId) + 1, nil
	}
	return 0, nil
}

func (s *Service) insertEvents(ctx context.Context, latestBlockNumber uint64, votingType string,
	voteOrProposal int64, contractAddress, method string, provider interface{},
) error {
	if provider == nil || voteOrProposal > int64(len(VotingEvents)) || voteOrProposal < 0 {
		return nil
	}

	db := s.db.Clone()

	addr := s.contractRegistry.GetAddr(contractAddress)
	if (addr == common.Address{}) {
		return errors.Errorf("contract address is missing!")
	}

	start, err := s.GetStartBlockByEvent(voteOrProposal, votingType)
	if err != nil {
		return err
	}

	var logs []types.Log
	notFound := true // we need to find at least one to make progress and search in next period during next iteration
	for notFound {
		end := int64(latestBlockNumber)
		if start+maxBlocksPerRequest < end {
			end = start + maxBlocksPerRequest
		}
		maybeEmptyLogs, err := s.contractRegistry.Backend.FilterLogs(ctx, ethereum.FilterQuery{
			FromBlock: big.NewInt(start + 1),
			ToBlock:   big.NewInt(end),
			Addresses: []common.Address{addr},
			Topics:    [][]common.Hash{{crypto.Keccak256Hash([]byte(method))}},
		})
		if err != nil {
			return errors.Wrap(err, "failed to filter logs", logan.F{
				"start_block": start + 1,
				"address":     addr.Hex(),
				"topic":       method,
			})
		}

		if len(maybeEmptyLogs) != 0 {
			logs = append(logs, maybeEmptyLogs...)
			s.log.WithFields(logan.F{
				"logs_length":   len(maybeEmptyLogs),
				"contract_name": contractAddress,
				"topic":         method,
			}).Debug("Logs were found")
			notFound = false
		}

		if start+maxBlocksPerRequest > int64(latestBlockNumber) {
			break // we reached the latest block
		}

		start += maxBlocksPerRequest
	}

	votes := make([]data.VotingEvent, 0)
	for _, log := range logs {
		event, err := s.parseEvent(log, provider, voteOrProposal)
		if err != nil {
			return err
		}

		if event != nil && !reflect.ValueOf(event).IsNil() {
			tx, _, err := s.client.TransactionByHash(ctx, reflect.ValueOf(event).Elem().FieldByName("Raw").FieldByName("TxHash").Interface().(common.Hash))
			if err != nil {
				return err
			}

			msg, err := tx.AsMessage(types.NewEIP155Signer(tx.ChainId()), nil)
			proposalId := uint64(0)
			_, ok := reflect.TypeOf(event).Elem().FieldByName("ProposalId")
			if !ok {
				proposalId = reflect.ValueOf(event).Elem().FieldByName("Id").Interface().(*big.Int).Uint64()
			} else {
				proposalId = reflect.ValueOf(event).Elem().FieldByName("ProposalId").Interface().(*big.Int).Uint64()
			}

			votes = append(votes, data.VotingEvent{
				VotingType:     votingType,
				AccountAddress: strings.ToLower(msg.From().String()),
				ProposalId:     proposalId,
				BlockId:        reflect.ValueOf(event).Elem().FieldByName("Raw").FieldByName("BlockNumber").Interface().(uint64),
			})
		}
	}

	switch voteOrProposal {
	case 0:
		err = s.handleQTokenHolderVotes(votes, db)
		if err != nil {
			return err
		}
	case 1:
		err = db.RootNodeVotes().Insert(votes...)
		if err != nil {
			return err
		}
	case 2:
		err = db.RootNodeProposals().Insert(votes...)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) handleQTokenHolderVotes(votes []data.VotingEvent, db data.Storage) error {
	accountAddresses := make([]string, 0)
	for _, vote := range votes {
		accountAddresses = append(accountAddresses, vote.AccountAddress)
	}
	agents, err := db.VotingAgents().GetByVotingAgents(accountAddresses...)
	if err != nil {
		return errors.Wrap(err, "failed to get by voting agents")
	}

	agentsMap := make(map[string]string)
	for _, agent := range agents {
		agentsMap[agent.VotingAgent] = agent.Address
	}

	delegated := make([]data.VotingEvent, 0)
	for _, vote := range votes {
		if address, ok := agentsMap[vote.AccountAddress]; ok {
			delegated = append(delegated, data.VotingEvent{
				VotingType:     "delegated_votes",
				AccountAddress: address,
				ProposalId:     vote.ProposalId,
				BlockId:        vote.BlockId,
			})
		}
	}

	err = db.QTokenHolderVotes().Insert(append(votes, delegated...)...)
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) updateConstitutionVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.ConstitutionVoting()

	votings, err := db.ConstitutionVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		baseVoting, err := s.prepareVoting(ctx, big.NewInt(int64(voting.ProposalId)), provider, timestamp, voting.VotingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id":          voting.ProposalId,
				"voting_type": voting.VotingType,
			})
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(baseVoting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, baseVoting.VotingEndTime)
		}

		err = db.ConstitutionVotings().Update(&data.ConstitutionVoting{
			BaseVoting: *baseVoting,
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateGeneralUpdateVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.GeneralUpdateVoting()

	votings, err := db.GeneralUpdateVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Params.VotingEndTime.Uint64())
		}
		voting := data.GeneralUpdateVoting{
			ProposalId:    voting.ProposalId,
			VotingStatus:  proposalStatus,
			CurrentQuorum: proposalStats.CurrentQuorum.String(),
			VetosNumber:   proposal.Counters.VetosCount.Uint64(),
		}
		if proposal.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}
		err = db.GeneralUpdateVotings().Update(&voting)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateEmergencyUpdateVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.EmergencyUpdateVoting()

	votings, err := db.EmergencyUpdateVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Params.VotingEndTime.Uint64())
		}
		voting := data.EmergencyUpdateVoting{
			ProposalId:    voting.ProposalId,
			VotingStatus:  proposalStatus,
			CurrentQuorum: proposalStats.CurrentQuorum.String(),
		}
		if proposal.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}
		err = db.EmergencyUpdateVotings().Update(&voting)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateRootsVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.RootsVoting()

	proposalIds, err := db.RootsVotings().GetEarliestUnfinishedVotingIds()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, proposalId := range proposalIds {
		proposal, err := provider.Proposals(nil, big.NewInt(int64(proposalId)))
		if err != nil {
			return err
		}
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(proposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(proposalId)))
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.RootsVoting{
			BaseVoting: data.BaseVoting{
				ProposalId:    proposalId,
				VotingStatus:  proposalStatus,
				CurrentQuorum: proposalStats.CurrentQuorum.String(),
				VetosNumber:   proposal.Base.Counters.VetosCount.Uint64(),
			},
			Candidate: proposal.Candidate.Hex(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}
		err = db.RootsVotings().Update(&voting)
		if err != nil {
			return err
		}
		emptyAddress := common.Address{}
		if proposal.Base.Executed {
			if proposal.Candidate != emptyAddress && proposal.ReplaceDest == emptyAddress {
				err := s.newRootMembershipPeriod(ctx, voting)
				if err != nil {
					return err
				}
			} else if proposal.Candidate != emptyAddress && proposal.ReplaceDest != emptyAddress {
				err := s.replaceRootMember(ctx, voting)
				if err != nil {
					return err
				}
			} else if proposal.Candidate == emptyAddress && proposal.ReplaceDest != emptyAddress {
				err := s.removeRootMember(ctx, voting)
				if err != nil {
					return err
				}
			}
		}

	}

	return nil
}

func (s *Service) updateRootnodesSlashingVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.RootNodesSlashingVoting()

	votings, err := db.RootnodesSlashingVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		baseVoting, err := s.prepareVoting(ctx, big.NewInt(int64(voting.ProposalId)), provider, timestamp, voting.VotingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id":          voting.ProposalId,
				"voting_type": voting.VotingType,
			})
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(baseVoting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, baseVoting.VotingEndTime)
		}
		err = db.RootnodesSlashingVotings().Update(&data.RootnodesSlashingVoting{
			BaseVoting: *baseVoting,
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateValidatorsSlashingVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.ValidatorsSlashingVoting()

	votings, err := db.ValidatorsSlashingVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		baseVoting, err := s.prepareVoting(ctx, big.NewInt(int64(voting.ProposalId)), provider, timestamp, voting.VotingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id":          voting.ProposalId,
				"voting_type": voting.VotingType,
			})
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(baseVoting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, baseVoting.VotingEndTime)
		}
		err = db.ValidatorsSlashingVotings().Update((*data.ValidatorsSlashingVoting)(baseVoting))
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateEpqfiMembershipVotings(ctx context.Context, _, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.EpqfiMembershipVoting()

	votings, err := db.EpqfiMembershipVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	for _, voting := range votings {
		baseVoting, err := s.prepareVoting(ctx, big.NewInt(int64(voting.ProposalId)), provider, timestamp, voting.VotingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id":          voting.ProposalId,
				"voting_type": voting.VotingType,
			})
		}

		err = db.EpqfiMembershipVotings().Update((*data.EpqfiMembershipVoting)(baseVoting))
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateEpqfiParametersVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.EpqfiParametersVoting()

	votings, err := db.EpqfiParametersVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		baseVoting, err := s.prepareVoting(ctx, big.NewInt(int64(voting.ProposalId)), provider, timestamp, voting.VotingType)
		if err != nil {
			return errors.Wrap(err, "failed to prepare voting", logan.F{
				"id":          voting.ProposalId,
				"voting_type": voting.VotingType,
			})
		}

		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(baseVoting.VotingStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, baseVoting.VotingEndTime)
		}

		err = db.EpqfiParametersVotings().Update((*data.EpqfiParametersVoting)(baseVoting))
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateEprsMembershipVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.EprsMembershipVoting()

	votings, err := db.EprsMembershipVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.Base.Params.VotingEndTime.Uint64())
		}
		voting := data.EprsMembershipVoting{
			ProposalId:    voting.ProposalId,
			VotingStatus:  proposalStatus,
			CurrentQuorum: proposalStats.CurrentQuorum.String(),
			VetosNumber:   proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}
		err = db.EprsMembershipVotings().Update(&voting)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateEprsParametersVotings(ctx context.Context, _, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.EprsParametersVoting()

	votings, err := db.EprsParametersVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		voting := data.EprsParametersVoting{
			ProposalId:    voting.ProposalId,
			VotingStatus:  proposalStatus,
			CurrentQuorum: proposalStats.CurrentQuorum.String(),
			VetosNumber:   proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}
		err = db.EprsParametersVotings().Update(&voting)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateEpdrMembershipVotings(ctx context.Context, _, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.EpdrMembershipVoting()

	votings, err := db.EpdrMembershipVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		voting := data.EpdrMembershipVoting{
			ProposalId:    voting.ProposalId,
			VotingStatus:  proposalStatus,
			CurrentQuorum: proposalStats.CurrentQuorum.String(),
			VetosNumber:   proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}
		err = db.EpdrMembershipVotings().Update(&voting)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateEpdrParametersVotings(ctx context.Context, _, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.EpdrParametersVoting()

	votings, err := db.EpdrParametersVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.Proposals(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		voting := data.EpdrParametersVoting{
			ProposalId:    voting.ProposalId,
			VotingStatus:  proposalStatus,
			CurrentQuorum: proposalStats.CurrentQuorum.String(),
			VetosNumber:   proposal.Base.Counters.VetosCount.Uint64(),
		}
		if proposal.Base.Params.VotingEndTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.Base.Params.VotingEndTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
		}
		err = db.EpdrParametersVotings().Update(&voting)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateContractRegistryAddressVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.ContractRegistryAddressVoting()

	votings, err := db.ContractRegistryAddressVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)
	// cannot apply prepareVoting here because in the registry voting majority is used instead of quorum
	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.GetProposal(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		votesCount, err := provider.VoteCount(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.VotingExpiredTime.Uint64())
		}
		voting := data.ContractRegistryVoting{
			ProposalId:      voting.ProposalId,
			VotingStatus:    proposalStatus,
			CurrentMajority: proposalStats.CurrentMajority.String(),
			VotesNumber:     votesCount.Uint64(),
		}
		if proposal.VotingExpiredTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.VotingExpiredTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
			voting.CurrentMajority = calculateMajority(votesCount, big.NewInt(int64(rootNodesNumber))).String()
		}
		err = db.ExecInTx(func() error {
			err := db.ContractRegistryAddressVotings().Update(&voting)
			if err != nil {
				return err
			}

			if voting.VotingStatus == ProposalStatusExecuted {
				proposal, err := provider.GetProposal(nil, new(big.Int).SetUint64(voting.ProposalId))
				if err != nil {
					return err
				}

				impl, err := s.getSystemContractImpl(ctx, proposal.Proxy, proposal.Key)
				if err != nil {
					return err
				}

				return db.SystemContracts().Upsert(impl.toData())
			}

			return nil
		})
		if err != nil {
			return nil
		}
	}

	return nil
}

func (s *Service) updateContractRegistryUpgradeVotings(ctx context.Context, latestBlockNumber uint64, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.ContractRegistryUpgradeVoting()

	votings, err := db.ContractRegistryUpgradeVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return err
	}

	votingEndBlockTime := uint64(0)

	for _, voting := range votings {
		proposalStatus, err := provider.GetStatus(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposalStats, err := provider.GetProposalStats(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		proposal, err := provider.GetProposal(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		votesCount, err := provider.VoteCount(nil, big.NewInt(int64(voting.ProposalId)))
		if err != nil {
			return err
		}
		if strings.Contains(finishedStatuses, strconv.FormatUint(uint64(proposalStatus), 10)) {
			votingEndBlockTime = max(votingEndBlockTime, proposal.VotingExpiredTime.Uint64())
		}
		voting := data.ContractRegistryVoting{
			ProposalId:      voting.ProposalId,
			VotingStatus:    proposalStatus,
			CurrentMajority: proposalStats.CurrentMajority.String(),
			VotesNumber:     votesCount.Uint64(),
		}
		if proposal.VotingExpiredTime.Uint64() < timestamp {
			rootNodesNumber, err := s.getPanelSize(ctx, proposal.VotingExpiredTime.Int64())
			if err != nil {
				return err
			}
			voting.RootNodesNumber = rootNodesNumber
			voting.CurrentMajority = calculateMajority(votesCount, big.NewInt(int64(rootNodesNumber))).String()
		}
		err = db.ExecInTx(func() error {
			err = db.ContractRegistryUpgradeVotings().Update(&voting)
			if err != nil {
				return err
			}

			if voting.VotingStatus == ProposalStatusExecuted {
				proposal, err := provider.GetProposal(nil, new(big.Int).SetUint64(voting.ProposalId))
				if err != nil {
					return err
				}

				impl, err := s.getSystemContractImpl(ctx, proposal.Proxy, "")
				if err != nil {
					return err
				}

				return db.SystemContracts().UpdateImplAddress(impl.toData())
			}

			return nil
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) updateContractRegistryVotings(ctx context.Context, latestBlockNumber, timestamp uint64) error {
	db := s.db.Clone()
	provider := s.contractRegistry.ContractRegistryVoting()

	votings, err := db.ContractRegistryVotings().GetEarliestUnfinishedVotings()
	if err != nil {
		return errors.Wrap(err, "failed to get pending votings")
	}
	callOpts := &bind.CallOpts{Context: ctx}

	for _, voting := range votings {
		id := big.NewInt(int64(voting.ProposalId))
		proposalStatus, err := provider.GetStatus(callOpts, id)
		if err != nil {
			return errors.Wrap(err, "failed to get proposal status", logan.F{"id": voting.ProposalId})
		}
		proposalStats, err := provider.GetProposalStats(callOpts, id)
		if err != nil {
			return errors.Wrap(err, "failed to get proposal stats", logan.F{"id": voting.ProposalId})
		}
		proposal, err := provider.GetProposal(callOpts, id)
		if err != nil {
			return errors.Wrap(err, "failed to get proposal", logan.F{"id": voting.ProposalId})
		}
		votesCount, err := provider.VoteCount(callOpts, id)
		if err != nil {
			return errors.Wrap(err, "failed to get vote count", logan.F{"id": voting.ProposalId})
		}

		updatedVoting := data.ContractRegistryVoting{
			ProposalId:      voting.ProposalId,
			VotingStatus:    proposalStatus,
			CurrentMajority: proposalStats.CurrentMajority.String(),
			VotesNumber:     votesCount.Uint64(),
		}
		if proposal.VotingExpiredTime.Uint64() < timestamp {
			updatedVoting.RootNodesNumber, err = s.getPanelSize(ctx, proposal.VotingExpiredTime.Int64())
			if err != nil {
				return errors.Wrap(err, "failed to get panel size", logan.F{"id": voting.ProposalId})
			}
			voting.CurrentMajority = calculateMajority(votesCount, big.NewInt(int64(updatedVoting.RootNodesNumber))).String()
		}
		err = db.ExecInTx(func() error {
			err = db.ContractRegistryVotings().Update(&updatedVoting)
			if err != nil {
				return errors.Wrap(err, "failed to update voting", logan.F{"id": voting.ProposalId})
			}

			if proposalStatus == ProposalStatusExecuted {
				// TODO parse call data instead of updating all system contracts
				err = s.updateSystemContracts(ctx)
				if err != nil {
					return errors.Wrap(err, "failed to update system contracts", logan.F{
						"proposal_id": voting.ProposalId,
					})
				}
			}

			return nil
		})
		if err != nil {
			return errors.Wrap(err, "failed to exec db tx")
		}
	}

	return nil
}

func (s *Service) findLastCounter(highestBlock uint64) (int64, error) {
	provider := s.contractRegistry.RootsVoting()

	filterOpts := &bind.FilterOpts{}
	// search range of proposal created event for roots membership voting
	step := uint64(1)
	last := new(big.Int)
	for filterOpts.Start >= 0 {
		filterOpts.Start = highestBlock - maxBlocksPerRequest*step
		endBlock := highestBlock - maxBlocksPerRequest*(step-1)
		filterOpts.End = &endBlock
		filterIter, err := provider.FilterProposalCreated(filterOpts)
		if err != nil {
			return 0, errors.Wrap(err, "failed to filter created proposals", logan.F{
				"filter_start": filterOpts.Start,
				"filter_end":   *filterOpts.End,
			})
		}
		for filterIter.Next() {
			if filterIter.Event != nil {
				if filterIter.Event.Id.Cmp(last) > 0 {
					last = last.Set(filterIter.Event.Id)
					continue
				}
				return last.Int64(), nil
			}
		}
		if last.Int64() != 0 {
			return last.Int64(), nil
		}
		step++
	}
	return 0, nil
}

func (s *Service) insertInitialRootMembershipPeriods() error {
	db := s.db.Clone()
	latest, err := db.RootsMembership().GetLatest()
	if err != nil {
		return err
	}
	if latest == nil {
		var initialRoots []string
		var startBlock int64
		switch s.chainID {
		case params.MainnetChainConfig.ChainID:
			initialRoots = []string{
				"0xbada551878e60b7d9173452695c1b3d190c3a3dc",
				"0x0ab8d42796bc11a0c028a25a79cf31d8eabc65cd",
				"0xfd3ba4c7ebda55c038316c776f2479b2909da7a5",
				"0x532c69263800e1f1cdb72acae555a85864146986",
			}
			startBlock = 219
		case params.TestnetChainConfig.ChainID:
			initialRoots = []string{
				"0x64D4edeFE8bA86d3588B213b0A053e7B910Cad68",
				"0x4a14D788D86D021670EBcecE1196631d66595984",
				"0x532c69263800e1f1cdb72acae555a85864146986",
			}
			startBlock = 20640
		default: // assuming it's devnet
			initialRoots = []string{
				"0x64D4edeFE8bA86d3588B213b0A053e7B910Cad68",
				"0x4a14D788D86D021670EBcecE1196631d66595984",
				"0x532c69263800e1f1cdb72acae555a85864146986",
			}
			startBlock = 329
		}

		startHeader, err := s.client.HeaderByNumber(context.Background(), big.NewInt(startBlock))
		if err != nil {
			return err
		}
		startTime := startHeader.Time
		for i, rootAddress := range initialRoots {
			rnMembership := &data.RootsMembership{
				PeriodId:    uint64(i),
				RootAddress: rootAddress,
				StartBlock:  startBlock,
				EndBlock:    0,
				StartTime:   time.Unix(int64(startTime), 0).UTC(),
				EndTime:     time.Unix(0, 0).UTC(),
			}
			err := db.RootsMembership().Insert(rnMembership)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Service) newRootMembershipPeriod(ctx context.Context, rootsVoting data.RootsVoting) error {
	db := s.db.Clone()
	provider := s.contractRegistry.RootsVoting()

	filterIter, err := provider.FilterProposalExecuted(nil, []*big.Int{big.NewInt(int64(rootsVoting.ProposalId))})
	if err != nil {
		return err
	}
	proposalBlockNumber := int64(0)
	proposalBlockNumberTimestamp := time.Unix(0, 0)
	for filterIter.Next() {
		proposalBlockNumber = int64(filterIter.Event.Raw.BlockNumber)
		header, err := s.client.HeaderByNumber(ctx, big.NewInt(proposalBlockNumber))
		if err != nil {
			return err
		}
		proposalBlockNumberTimestamp = time.Unix(int64(header.Time), 0)
	}

	// Adding new period
	latest, err := db.RootsMembership().GetLatest()
	if latest != nil {
		err := s.insertRootMembershipPeriod(latest.PeriodId+1, rootsVoting.Candidate, proposalBlockNumber, proposalBlockNumberTimestamp)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) replaceRootMember(ctx context.Context, rootsVoting data.RootsVoting) error {
	db := s.db.Clone()
	provider := s.contractRegistry.RootsVoting()

	filterIter, err := provider.FilterProposalExecuted(nil, []*big.Int{big.NewInt(int64(rootsVoting.ProposalId))})
	if err != nil {
		return err
	}
	proposalBlockNumber := int64(0)
	proposalBlockNumberTimestamp := time.Unix(0, 0)
	for filterIter.Next() {
		proposalBlockNumber = int64(filterIter.Event.Raw.BlockNumber)
		header, err := s.client.HeaderByNumber(ctx, big.NewInt(proposalBlockNumber))
		if err != nil {
			return err
		}
		proposalBlockNumberTimestamp = time.Unix(int64(header.Time), 0)
	}

	unfinishedReplace, err := db.RootsMembership().GetUnfinishedPeriodByAddress(rootsVoting.ReplaceDest)
	if err != nil {
		return err
	}
	if unfinishedReplace != nil {
		err := s.updateRootMembershipPeriod(unfinishedReplace.PeriodId, proposalBlockNumber, proposalBlockNumberTimestamp)
		if err != nil {
			return err
		}
	}

	// Adding new period
	latest, err := db.RootsMembership().GetLatest()
	if latest != nil {
		err := s.insertRootMembershipPeriod(latest.PeriodId+1, rootsVoting.Candidate, proposalBlockNumber, proposalBlockNumberTimestamp)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Service) removeRootMember(ctx context.Context, rootsVoting data.RootsVoting) error {
	db := s.db.Clone()
	provider := s.contractRegistry.RootsVoting()

	filterIter, err := provider.FilterProposalExecuted(nil, []*big.Int{big.NewInt(int64(rootsVoting.ProposalId))})
	if err != nil {
		return err
	}
	proposalBlockNumber := int64(0)
	proposalBlockNumberTimestamp := time.Unix(0, 0)
	for filterIter.Next() {
		proposalBlockNumber = int64(filterIter.Event.Raw.BlockNumber)
		header, err := s.client.HeaderByNumber(ctx, big.NewInt(proposalBlockNumber))
		if err != nil {
			return err
		}
		proposalBlockNumberTimestamp = time.Unix(int64(header.Time), 0)
	}

	unfinished, err := db.RootsMembership().GetUnfinishedPeriodByAddress(rootsVoting.ReplaceDest)
	if err != nil {
		return err
	}
	if unfinished != nil {
		err := s.updateRootMembershipPeriod(unfinished.PeriodId, proposalBlockNumber, proposalBlockNumberTimestamp)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *Service) insertRootMembershipPeriod(periodId uint64, rootAddress string, startNumber int64, startTime time.Time) error {
	db := s.db.Clone()

	rnMembership := &data.RootsMembership{
		PeriodId:    periodId,
		RootAddress: rootAddress,
		StartBlock:  startNumber,
		StartTime:   startTime,
		EndBlock:    0,
		EndTime:     time.Unix(0, 0),
	}
	err := db.RootsMembership().Insert(rnMembership)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) updateRootMembershipPeriod(periodId uint64, endNumber int64, endTime time.Time) error {
	db := s.db.Clone()
	rnMembership := &data.RootsMembership{
		PeriodId: periodId,
		EndBlock: endNumber,
		EndTime:  endTime,
	}
	err := db.RootsMembership().Update(rnMembership)
	if err != nil {
		return err
	}
	return nil
}

func (s *Service) handleVoluntaryExit(ctx context.Context, exitedNodes []common.Address, blockNumber uint64) error {
	db := s.db.Clone()

	allPeriods := make([]data.RootsMembership, 0)
	for _, exitedNode := range exitedNodes {
		periods, err := db.RootsMembership().GetUnfinishedPeriodsByAddress(exitedNode.Hex())
		if err != nil {
			s.log.WithError(err).WithField("node", exitedNode).Error("failed to get unfinished periods for node")
			continue
		}
		allPeriods = append(allPeriods, periods...)
	}

	header, err := s.client.HeaderByNumber(ctx, big.NewInt(int64(blockNumber)))
	if err != nil {
		s.log.WithError(err).Error("failed to get block header")
		return err
	}
	exitTime := time.Unix(int64(header.Time), 0)

	for _, period := range allPeriods {
		err := s.updateRootMembershipPeriod(period.PeriodId, int64(blockNumber), exitTime)
		if err != nil {
			s.log.WithError(err).WithFields(logan.F{
				"period_id": period.PeriodId,
				"block":     blockNumber,
			}).Error("failed to update root membership period")
		}
	}

	return nil
}

func newOotStats(qBlock *types.Block) *indexer.OutOfTurnStats {
	return &indexer.OutOfTurnStats{
		BlockNumber:  qBlock.NumberU64(),
		BlockHash:    qBlock.Hash(),
		Difficulty:   big.NewInt(0),
		ActualSigner: common.Address{},
		MainAccount:  common.Address{},
		InTurnSigner: common.Address{},
	}
}
