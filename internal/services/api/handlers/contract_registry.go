package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

// @Summary   	Get contract registry
// @Description Returns contracts registry and it's implementation.
// @ID       	contract-registry
// @Accept    	json
// @Produce    	json
// @Success   	200 {object} resources.SystemContract
// @Failure   	500 "Internal Server Error"
// @Router     	/contract-registry [get]
func (h *systemContractsHandler) GetContractRegistry(w http.ResponseWriter, r *http.Request) {
	contractRegistry, err := h.storage.GetContractRegistry()
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get contract registry from DB")
		return
	}

	ape.Render(w, resources.PopulateSystemContract(contractRegistry))
}
