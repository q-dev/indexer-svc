package handlers

import (
	"net/http"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type RootNodeProposalsHandler struct {
	EventHandler
	storage data.RootNodeProposalsStorage
}

func NewRootNodeProposalsHandler(eventHandler EventHandler, storage data.RootNodeProposalsStorage) RootNodeProposalsHandler {
	return RootNodeProposalsHandler{
		EventHandler: eventHandler,
		storage:      storage,
	}
}

// GetRootNodeProposals returns root node proposals.
// @Summary   	Get root node proposals
// @Description Returns root node proposals.
// @ID       	rootnode-proposals
// @Accept    	json
// @Produce    	json
// @Param     	filter[rootAddress] query string false "Returns ALL root node proposals for `rootAddress`."
// @Param     	filter[startBlock] query int false "Returns root node proposals after `startBlock`."
// @Param		filter[aggregate] query bool false "Aggregate result for all root nodes"
// @Param     	sort query string false "Sorting order. Supported values: `account_address` (sort by account_address, asc), `-block` (sort by account_address, desc). Default - `-account_address`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.EventsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/root-node-metrics/rootnode-proposals [get]
func (h *RootNodeProposalsHandler) GetRootNodeProposals(w http.ResponseWriter, r *http.Request) {
	h.getEvents(h.storage.Select, w, r)
}

// GetRootNodeAggregatedProposals returns aggregated root node proposals.
// @Summary   	Get aggregated root node proposals
// @Description Returns aggregated root node proposals.
// @ID       	rootnode-proposals-aggregated
// @Accept    	json
// @Produce    	json
// @Param     	filter[rootAddress] query string false "Returns ALL root node proposals for `rootAddress`."
// @Param     	filter[startBlock] query int false "Returns root node proposals after `startBlock`."
// @Param     	sort query string false "Sorting order. Supported values: `account_address` (sort by account_address, asc), `-block` (sort by account_address, desc). Default - `-account_address`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.AggregatedEventsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/root-node-metrics/rootnode-proposals/aggregated [get]
func (h *RootNodeProposalsHandler) GetRootNodeAggregatedProposals(w http.ResponseWriter, r *http.Request) {
	h.getAggregatedEvents(h.storage.SelectAggregated, w, r)
}
