package data

// ValidatorsSlashingVoting voting.
type ValidatorsSlashingVoting BaseVoting

// ValidatorsSlashingVotingStorage stores validators slashing votings.
type ValidatorsSlashingVotingStorage interface {
	Insert(votings ...ValidatorsSlashingVoting) error
	GetLatest() (*ValidatorsSlashingVoting, error)

	GetEarliestUnfinishedVotings() ([]ValidatorsSlashingVoting, error)
	Update(voting *ValidatorsSlashingVoting) error
}
