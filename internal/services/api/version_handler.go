package api

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/logan/v3"

	"gitlab.com/q-dev/indexer-svc/internal/resources"

	"gitlab.com/q-dev/indexer-svc/internal/config"
)

type versionHandler struct {
	log *logan.Entry
}

// @Summary   	Get version
// @Description Returns Indexer version.
// @ID       	version
// @Accept    	json
// @Produce    	json
// @Success   	200 {object} resources.VersionList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/version [get]
func (h *versionHandler) getVersion(w http.ResponseWriter, r *http.Request) {
	resp := resources.PopulateVersion(config.IndexerVersionWithMeta)
	ape.Render(w, resp)
}
