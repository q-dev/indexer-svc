package handlers

import (
	"errors"
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

func (h *layerZeroListsHandler) GetLayerZeroLists(w http.ResponseWriter, r *http.Request) {
	roots := make([]resources.RootList, 0, 2)
	exclusions := make([]resources.ExclusionList, 0, 2)

	if err := h.tryToUpdateLists(); err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to update layer zero lists")
		return
	}

	activeRootList, err := h.getRootList("active")
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get root list")
		return
	}
	roots = append(roots, activeRootList)
	proposedRootList, err := h.getRootList("proposed")
	if err != nil && !errors.Is(err, notFoundError) {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get root list")
		return
	}
	if proposedRootList.Data.ID != "" {
		roots = append(roots, proposedRootList)
	}

	activeExclusionList, err := h.getExclusionList("active")
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get exclusion list")
		return
	}
	exclusions = append(exclusions, activeExclusionList)
	proposedExclusionList, err := h.getExclusionList("proposed")
	if err != nil && !errors.Is(err, notFoundError) {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get exclusion list")
		return
	}
	if proposedExclusionList.Data.ID != "" {
		exclusions = append(exclusions, proposedExclusionList)
	}

	ape.Render(w, resources.PopulateLayerZeroList(roots, exclusions))
}
