package handlers

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/requests"
)

// GetDiff
// @Summary   	Get diff between lists
// @Description Returns diff between diffs.
// @ID       	layer-zero-lists
// @Accept    	json
// @Produce    	json
// @Param     	nameA query string true "The name of the first list (active or proposed)."
// @Param     	nameB query string true "The name of the second list (active or proposed)."
// @Success   	200 {object} resources.ListDiff
// @Failure   	400 "Bad Request"
// @Failure   	500 "Internal Server Error"
// @Router     	/list-diff/{type} [get]
func (h *layerZeroListsHandler) GetDiff(w http.ResponseWriter, r *http.Request) {
	if err := h.tryToUpdateLists(); err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to update layer zero lists")
		return
	}

	request, err := requests.NewGetDiffListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	var accountsA []resources.AccountPair
	var accountsB []resources.AccountPair
	if request.Type == requests.ListRoot {
		listA, err := h.storage.GetRootList(request.NameA)
		if err != nil {
			ape.RenderErr(w, problems.InternalError())
			h.log.WithError(err).Errorf("failed to get %s %s", request.Type, request.NameA)
			return
		}
		listB, err := h.storage.GetRootList(request.NameB)
		if err != nil {
			ape.RenderErr(w, problems.InternalError())
			h.log.WithError(err).Errorf("failed to get %s %s", request.Type, request.NameB)
			return
		}

		accountsA = accountsToResources(listA.RootList)
		accountsB = accountsToResources(listB.RootList)
	} else {
		listA, err := h.storage.GetExclusionList(request.NameA)
		if err != nil {
			ape.RenderErr(w, problems.InternalError())
			h.log.WithError(err).Errorf("failed to get %s %s", request.Type, request.NameA)
			return
		}
		listB, err := h.storage.GetExclusionList(request.NameB)
		if err != nil {
			ape.RenderErr(w, problems.InternalError())
			h.log.WithError(err).Errorf("failed to get %s %s", request.Type, request.NameB)
			return
		}

		accountsA = exclusionsToResources(listA.ExclusionSet)
		accountsB = exclusionsToResources(listB.ExclusionSet)
	}

	ape.Render(w, resources.PopulateListDiff(accountsA, accountsB, requests.ListRoot, request.NameA, request.NameB))
}

func accountsToResources(list []data.NodeAccountPair) []resources.AccountPair {
	result := make([]resources.AccountPair, 0, len(list))
	for _, account := range list {
		result = append(result, resources.AccountPair{
			MainAccount: resources.GetMainAccount(account.MainAccount, account.Alias),
		})
	}

	return result
}

func exclusionsToResources(list []data.NodeExclusion) []resources.AccountPair {
	result := make([]resources.AccountPair, 0, len(list))
	for _, account := range list {
		result = append(result, resources.AccountPair{
			MainAccount: resources.GetMainAccount(account.MainAccount, account.Alias),
		})
	}

	return result
}
