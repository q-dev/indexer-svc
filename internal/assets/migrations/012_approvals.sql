-- +migrate Up
CREATE TABLE approvals
(
    id          BIGSERIAL   PRIMARY KEY,
    signature   BYTEA       NOT NULL UNIQUE,
    block       BIGINT      NOT NULL,
    signer      CHAR(42)    NOT NULL,
    alias       CHAR(42)    DEFAULT NULL
);

-- +migrate Down
DROP TABLE approvals;
