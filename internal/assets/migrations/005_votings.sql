-- +migrate Up
CREATE TABLE constitution_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_subtype      INT        NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE general_update_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE emergency_update_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE roots_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL,
    candidate           CHAR(42)   NOT NULL,
    replace_dest        TEXT       NOT NULL
);

CREATE TABLE rootnodes_slashing_votings
(
    proposal_id         BIGINT       NOT NULL PRIMARY KEY,
    voting_type         TEXT         NOT NULL,
    voting_status       INT          NOT NULL,
    voting_start_time   TIMESTAMP    NOT NULL,
    current_quorum      TEXT         DEFAULT NULL,
    required_quorum     TEXT         NOT NULL,
    root_nodes_number   BIGINT       DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    candidate           CHAR(42)     NOT NULL,
    voting_end_time     TIMESTAMP    NOT NULL,
    veto_end_time       TIMESTAMP    NOT NULL
);

CREATE TABLE validators_slashing_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE epqfi_membership_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE epqfi_parameters_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE epdr_membership_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE epdr_parameters_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE eprs_membership_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE eprs_parameters_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_quorum      TEXT       DEFAULT NULL,
    required_quorum     TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    vetos_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL,
    veto_end_time       TIMESTAMP  NOT NULL
);

CREATE TABLE contract_registry_address_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_majority    TEXT       DEFAULT NULL,
    required_majority   TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    votes_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL
);

CREATE TABLE contract_registry_upgrade_votings
(
    proposal_id         BIGINT     NOT NULL PRIMARY KEY,
    voting_type         TEXT       NOT NULL,
    voting_status       INT        NOT NULL,
    voting_start_time   TIMESTAMP  NOT NULL,
    current_majority    TEXT       DEFAULT NULL,
    required_majority   TEXT       NOT NULL,
    root_nodes_number   BIGINT     DEFAULT NULL,
    votes_number        BIGINT     DEFAULT NULL,
    voting_end_time     TIMESTAMP  NOT NULL
);

-- +migrate Down
DROP TABLE constitution_votings;
DROP TABLE general_update_votings;
DROP TABLE emergency_update_votings;
DROP TABLE roots_votings;
DROP TABLE rootnodes_slashing_votings;
DROP TABLE validators_slashing_votings;
DROP TABLE epqfi_membership_votings;
DROP TABLE epqfi_parameters_votings;
DROP TABLE epdr_membership_votings;
DROP TABLE epdr_parameters_votings;
DROP TABLE eprs_membership_votings;
DROP TABLE eprs_parameters_votings;
DROP TABLE contract_registry_address_votings;
DROP TABLE contract_registry_upgrade_votings;
