package requests

import (
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
)

type GetRootNodesListRequest struct {
	Status string `url:"status"`
}

func NewGetRootNodesListRequest(r *http.Request) (GetRootNodesListRequest, error) {
	status := r.URL.Query().Get("status")
	request := GetRootNodesListRequest{status}

	return request, request.Validate()
}

func (r *GetRootNodesListRequest) Validate() error {
	return validation.Errors{
		"status": validation.Validate(r.Status, validation.Required, validation.In(listActive, listProposed)),
	}.Filter()
}
