package data

// EpqfiMembershipVoting voting.
type EpqfiMembershipVoting BaseVoting

// EpqfiMembershipVotingStorage stores epqfi membership votings.
type EpqfiMembershipVotingStorage interface {
	Insert(votings ...EpqfiMembershipVoting) error
	GetLatest() (*EpqfiMembershipVoting, error)

	GetEarliestUnfinishedVotings() ([]EpqfiMembershipVoting, error)
	Update(voting *EpqfiMembershipVoting) error
}
