package handlers

import (
	"context"
	"database/sql"
	"math/big"
	"net/http"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/contracts"
	"gitlab.com/q-dev/q-client/ethclient"
	"gitlab.com/q-dev/q-client/governance"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/requests"
	"gitlab.com/q-dev/indexer-svc/internal/vars"
)

type layerZeroListsHandler struct {
	log         *logan.Entry
	storage     data.LayerZeroListStorage
	updatedAt   time.Time
	cachePeriod time.Duration
	ethclient   *ethclient.Client
	registry    *contracts.Registry
	mu          sync.Mutex
}

func NewLayerZeroListsHandler(
	log *logan.Entry,
	storage data.LayerZeroListStorage,
	cachePeriod time.Duration,
	ethclient *ethclient.Client,
	registry *contracts.Registry,
) layerZeroListsHandler {
	return layerZeroListsHandler{
		log:         log,
		storage:     storage,
		cachePeriod: cachePeriod,
		ethclient:   ethclient,
		registry:    registry,
	}
}

// GetExclusionList
// @Summary   	Get exclusion list
// @Description Returns exclusion list. May return 404 error if no proposed lists found
// @ID       	exclusion-list
// @Accept    	json
// @Produce    	json
// @Param     	status query string true "Active or proposed list."
// @Success   	200 {object} resources.ExclusionList
// @Failure   	400 "Bad Request"
// @Failure     404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Router     	/exclusion-list [get]
func (h *layerZeroListsHandler) GetExclusionList(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetExclusionListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	if err := h.tryToUpdateLists(); err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to update layer zero lists")
		return
	}

	exclList, err := h.getExclusionList(request.Status)
	if err != nil {
		if errors.Is(err, notFoundError) {
			ape.RenderErr(w, problems.NotFound())
			return
		}

		ape.Render(w, problems.InternalError())
		h.log.WithError(err).Error("failed to get exclusion list from DB")
		return
	}

	ape.Render(w, exclList)
}

func (h *layerZeroListsHandler) tryToUpdateLists() error {
	h.mu.Lock()
	defer h.mu.Unlock()

	if h.updatedAt.Add(h.cachePeriod).Before(time.Now()) {
		if err := h.updateLists(context.Background()); err != nil {
			return err
		}
	}

	return nil
}

func (h *layerZeroListsHandler) getExclusionList(status string) (resources.ExclusionList, error) {
	list, err := h.storage.GetExclusionList(status)
	if err != nil {
		h.log.WithError(err).Errorf("failed to get %s exclusion list", status)
		return resources.ExclusionList{}, err
	}
	if list.Hash == "" {
		return resources.ExclusionList{}, notFoundError
	}

	var percentage float32
	if status != "active" {
		currentRoots, err := h.storage.GetCurrentRoots()
		if err != nil {
			h.log.WithError(err).Error("failed to get current roots from DB")
			return resources.ExclusionList{}, err
		}
		percentage = getRootsAcceptedPercentage(currentRoots, list.Signers)
	}

	return resources.PopulateExclusionList(list, percentage), nil
}

func getRootsAcceptedPercentage(currentRoots, signers []data.NodeAccountPair) float32 {
	if len(currentRoots) == 0 {
		return 0
	}

	rootsMap := make(map[string]struct{})
	for _, root := range currentRoots {
		rootsMap[root.Alias] = struct{}{}
	}

	signatures := 0
	for _, signer := range signers {
		if _, ok := rootsMap[signer.MainAccount.String]; ok {
			signatures++
		}
	}

	return float32(signatures) / float32(len(currentRoots)) * 100.0
}

func (h *layerZeroListsHandler) govExclusionListToData(govList governance.ExclusionList, status string) data.NodeExclusionList {
	var list data.NodeExclusionList

	list.Hash = govList.Hash.String()
	list.Type = data.ListTypeExclusion
	list.Timestamp = time.Unix(int64(govList.Timestamp), 0)
	list.Status = status

	list.ExclusionSet = make([]data.NodeExclusion, 0, len(govList.Validators))
	for _, exclusion := range govList.Validators {
		list.ExclusionSet = append(list.ExclusionSet, data.NodeExclusion{
			NodeAccountPair: data.NodeAccountPair{
				Alias:       exclusion.Address.String(),
				MainAccount: sql.NullString{},
			},
			StartBlock: exclusion.Block,
			EndBlock: sql.NullInt64{
				Int64: int64(exclusion.EndBlock),
				Valid: exclusion.Block != 0,
			},
		})
	}

	list.Signers = make([]data.NodeAccountPair, 0, len(govList.Signers))
	for _, signer := range govList.Signers {
		list.Signers = append(list.Signers, data.NodeAccountPair{
			Alias:       signer.String(),
			MainAccount: sql.NullString{},
		})
	}
	exclusionNodes := make([]data.NodeAccountPair, 0, len(list.ExclusionSet))
	for _, exclusion := range list.ExclusionSet {
		exclusionNodes = append(exclusionNodes, data.NodeAccountPair{
			Alias:       "",
			MainAccount: sql.NullString{String: exclusion.Alias, Valid: true},
		})
	}

	h.resolveSignersAliases(list.Signers)
	h.resolveNodesAliases(exclusionNodes)

	for idx, exclusion := range exclusionNodes {
		list.ExclusionSet[idx].MainAccount = exclusion.MainAccount
	}

	return list
}

func (h *layerZeroListsHandler) resolveSignersAliases(signers []data.NodeAccountPair) {
	providerAliases := h.registry.AccountAliases(nil)
	if providerAliases == nil {
		h.log.Error("failed to get account aliases provider")
		return
	}

	purposes := make([]*big.Int, 0, len(signers))
	accounts := make([]common.Address, 0, len(signers))
	for _, signer := range signers {
		purposes = append(purposes, vars.RNOperationPurpose)
		accounts = append(accounts, common.HexToAddress(signer.Alias))
	}

	aliases, errAlias := providerAliases.ResolveBatchReverse(nil, accounts, purposes)
	if errAlias != nil {
		h.log.Error("failed to get account aliases from smart contract", "error", errAlias)
		return
	}

	for idx := range signers {
		aliasStr := aliases[idx].String()
		signers[idx].MainAccount = sql.NullString{String: aliasStr, Valid: true}
	}
}

func (h *layerZeroListsHandler) resolveNodesAliases(nodes []data.NodeAccountPair) {
	providerAliases := h.registry.AccountAliases(nil)
	if providerAliases == nil {
		h.log.Error("failed to get account aliases provider")
		return
	}

	purposes := make([]*big.Int, 0, len(nodes))
	accounts := make([]common.Address, 0, len(nodes))
	for _, node := range nodes {
		purposes = append(purposes, vars.RNOperationPurpose)
		accounts = append(accounts, common.HexToAddress(node.MainAccount.String))
	}

	aliases, errAlias := providerAliases.ResolveBatch(nil, accounts, purposes)
	if errAlias != nil {
		h.log.WithError(errAlias).Error("failed to get account aliases from smart contract")
		return
	}

	for idx := range nodes {
		aliasStr := aliases[idx].String()
		nodes[idx].Alias = aliasStr
		if aliasStr == (common.Address{}).String() {
			nodes[idx].Alias = nodes[idx].MainAccount.String
		}
	}
}

type l0Actions int

const (
	saveProposedRootList           l0Actions = 1 << iota // Signers list can be updated
	outdateCurrentProposedRootList                       // Proposed list became active or was rejected
	saveActiveRootList
	outdateCurrentActiveRootList
	saveProposedExclList
	outdateCurrentProposedExclList
	saveActiveExclList
	outdateCurrentActiveExclList
)

func (h *layerZeroListsHandler) updateLists(ctx context.Context) error {
	currentProposedExclList, err := h.storage.GetExclusionList("proposed")
	if err != nil {
		return errors.WithMessage(err, "failed to get proposed exclusion list from DB")
	}
	currentProposedRootList, err := h.storage.GetRootList("proposed")
	if err != nil {
		return errors.WithMessage(err, "failed to get proposed root list from DB")
	}
	currentActiveRootList, err := h.storage.GetRootList("active")
	if err != nil {
		return errors.WithMessage(err, "failed to get active root list from DB")
	}
	currentActiveExclList, err := h.storage.GetExclusionList("active")
	if err != nil {
		return errors.WithMessage(err, "failed to get active exclusion list from DB")
	}
	activeExclusionList, err := h.ethclient.GetActiveExclusionList(ctx)
	if err != nil {
		return err
	}
	proposedExclusionList, err := h.ethclient.GetProposedExclusionList(ctx)
	if err != nil {
		return err
	}
	activeRootList, err := h.ethclient.GetActiveRootList(ctx)
	if err != nil {
		return err
	}
	proposedRootList, err := h.ethclient.GetProposedRootList(ctx)
	if err != nil {
		return err
	}

	updates := l0Actions(0)

	updates |= saveActiveRootList | saveActiveExclList | saveProposedRootList | saveProposedExclList

	if currentProposedRootList.Hash != "" &&
		(currentProposedRootList.Hash != proposedRootList.Hash.String() ||
			currentProposedRootList.Hash == activeRootList.Hash.String() || // To outdate previously saved list
			uint64(currentProposedRootList.Timestamp.Unix()) < activeRootList.Timestamp) { // To outdate previously saved list
		updates |= outdateCurrentProposedRootList
	}

	if currentActiveRootList.Hash != "" &&
		currentActiveRootList.Hash != activeRootList.Hash.String() {
		updates |= outdateCurrentActiveRootList
	}

	if currentProposedExclList.Hash != "" &&
		(currentProposedExclList.Hash != proposedExclusionList.Hash.String() ||
			currentProposedExclList.Hash == activeExclusionList.Hash.String() ||
			uint64(currentProposedExclList.Timestamp.Unix()) < activeExclusionList.Timestamp) {
		updates |= outdateCurrentProposedExclList
	}

	if currentActiveExclList.Hash != "" &&
		currentActiveExclList.Hash != activeExclusionList.Hash.String() {
		updates |= outdateCurrentActiveExclList
	}

	if updates != 0 {
		err = h.storage.Tx(func() error {
			if updates&outdateCurrentProposedRootList != 0 {
				err := h.storage.OutdateList("root-list", currentProposedRootList.Hash)
				if err != nil {
					return err
				}
			}

			if updates&saveProposedRootList != 0 {
				list := h.govRootListToData(proposedRootList, "proposed")

				err := h.storage.SaveNodeAliases(append(list.Signers, list.RootList...))
				if err != nil {
					return errors.Wrap(err, "failed to save proposed root list's aliases")
				}

				err = h.storage.SaveRootList(list)
				if err != nil {
					return errors.Wrap(err, "failed to save proposed root list")
				}
			}

			if updates&outdateCurrentActiveRootList != 0 {
				err := h.storage.OutdateList("root-list", currentActiveRootList.Hash)
				if err != nil {
					return err
				}
			}

			if updates&saveActiveRootList != 0 {
				list := h.govRootListToData(activeRootList, "active")

				err := h.storage.SaveNodeAliases(append(list.Signers, list.RootList...))
				if err != nil {
					return errors.Wrap(err, "failed to save active root list's aliases")
				}

				err = h.storage.SaveRootList(list)
				if err != nil {
					return errors.Wrap(err, "failed to save active root list")
				}
			}

			if updates&outdateCurrentProposedExclList != 0 {
				err := h.storage.OutdateList("exclusion-list", currentProposedExclList.Hash)
				if err != nil {
					return err
				}
			}

			if updates&saveProposedExclList != 0 {
				list := h.govExclusionListToData(proposedExclusionList, "proposed")

				exclusionListPairs := make([]data.NodeAccountPair, len(list.ExclusionSet))
				for _, excl := range list.ExclusionSet {
					exclusionListPairs = append(exclusionListPairs, excl.NodeAccountPair)
				}

				err := h.storage.SaveNodeAliases(append(list.Signers, exclusionListPairs...))
				if err != nil {
					return errors.Wrap(err, "failed to save proposed exclusion list's aliases")
				}

				err = h.storage.SaveExclusionList(list)
				if err != nil {
					return errors.Wrap(err, "failed to save proposed exclusion list")
				}
			}

			if updates&outdateCurrentActiveExclList != 0 {
				err := h.storage.OutdateList("exclusion-list", currentActiveExclList.Hash)
				if err != nil {
					return err
				}
			}

			if updates&saveActiveExclList != 0 {
				list := h.govExclusionListToData(activeExclusionList, "active")

				exclusionListPairs := make([]data.NodeAccountPair, len(list.ExclusionSet))
				for _, excl := range list.ExclusionSet {
					exclusionListPairs = append(exclusionListPairs, excl.NodeAccountPair)
				}

				err := h.storage.SaveNodeAliases(append(list.Signers, exclusionListPairs...))
				if err != nil {
					return errors.Wrap(err, "failed to save active exclusion list's aliases")
				}

				err = h.storage.SaveExclusionList(list)
				if err != nil {
					return errors.Wrap(err, "failed to save active exclusion list")
				}
			}

			return nil
		})
		if err != nil {
			return errors.Wrap(err, "failed to update lists")
		}
	}

	h.updatedAt = time.Now()

	return nil
}
