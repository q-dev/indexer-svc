package api

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

type blocksHandler struct {
	log     *logan.Entry
	storage data.BlockStorage
}

type getBlocksQuery struct {
	From *uint64 `schema:"filter[from],omitempty"`
	To   *uint64 `schema:"filter[to],omitempty"`

	Sort   string  `schema:"sort"`
	Cursor *uint64 `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	Signer       *string `schema:"filter[signer],omitempty"`
	MainAccount  *string `schema:"filter[mainaccount],omitempty"`
	InTurnSigner *string `schema:"filter[inturnsigner],omitempty"`
	AnySigner    *string `schema:"filter[anysigner],omitempty"`
	OutOfTurn    bool    `schema:"filter[outofturn],omitempty"`
}

func (q *getBlocksQuery) Validate() error {
	return validation.Errors{
		"sort":  validation.Validate(q.Sort, validation.In("id", "-id")),
		"limit": validation.Validate(q.Limit, validation.Max(100)),
	}.Filter()
}

// @Summary   	Get blocks
// @Description Returns history of blocks.
// @ID       	blocks
// @Accept    	json
// @Produce    	json
// @Param     	filter[from] query int false "Return blocks starting with `from` number."
// @Param     	filter[to] query int false "Return blocks with number less than `to` (default: latest)."
// @Param     	filter[signer] query string false "Filter by signer."
// @Param     	filter[mainaccount] query string false "Filter by main account."
// @Param     	filter[inturnsigner] query string false "Filter by inturn signer."
// @Param     	filter[anysigner] query string false "Filter either by signer or inturn signer."
// @Param     	filter[outofturn] query bool false "Filter in-turn / out-of-turn blocks."
// @Param     	sort query string false "Sorting order. Supported values: `id` (sort by block number, asc), `-id` (sort by block number, desc). Default - `id`."
// @Param     	page[cursor] query int false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `10`."
// @Success   	200 {object} resources.BlocksList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/blocks [get]
func (h *blocksHandler) getBlocks(w http.ResponseWriter, r *http.Request) {
	query := getBlocksQuery{
		Sort:  "id",
		Limit: 10,
	}
	if err := schema.NewDecoder().Decode(&query, r.URL.Query()); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blocks, err := h.storage.Select(&data.SelectBlocksParams{
		From: query.From,
		To:   query.To,

		Cursor: query.Cursor,
		IsDesc: strings.HasPrefix(query.Sort, "-"),
		Limit:  query.Limit,

		Signer:       query.Signer,
		MainAccount:  query.MainAccount,
		InTurnSigner: query.InTurnSigner,
		AnySigner:    query.AnySigner,
		OutOfTurn:    query.OutOfTurn,
	})
	if err != nil {
		h.log.WithError(err).Errorf("failed to query blocks; query %#v", query)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	resp := resources.PopulateBlocksList(blocks)
	resp.Links = buildBlocksLinks(blocks, *r.URL, query)
	ape.Render(w, resp)
}

func buildBlocksLinks(blocks []data.Block, endpoint url.URL, query getBlocksQuery) *resources.Links {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(query, params); err != nil {
		panic(errors.Wrap(err, "failed to encode query"))
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(blocks) == 0 {
		return links
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", strconv.FormatUint(blocks[len(blocks)-1].Number, 10))
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", strconv.FormatUint(blocks[0].Number, 10))
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)
	return links
}
