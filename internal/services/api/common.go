package api

import (
	"strconv"
	"strings"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/common"
)

// ensure we pass address in an appropriate format
func transformAddress(str *string) *string {
	if str == nil {
		return nil
	}

	addr := common.HexToAddress(*str)
	s := strings.ToLower(addr.Hex())
	return &s
}

func transformBool(str *string) *bool {
	if str == nil {
		return nil
	}

	b, err := strconv.ParseBool(*str)
	if err != nil {
		panic(errors.Wrap(err, "unexpected bool str"))
	}

	return &b
}

func isAddress(str *string) error {
	if str == nil {
		return nil
	}

	if !common.IsHexAddress(*str) {
		return errors.New("invalid hex address")
	}

	return nil
}
