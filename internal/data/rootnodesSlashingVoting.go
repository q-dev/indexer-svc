package data

// RootnodesSlashingVoting voting.
type RootnodesSlashingVoting struct {
	BaseVoting
	Candidate string `db:"candidate"`
}

// RootnodesSlashingVotingStorage stores rootnodes slashing votings.
type RootnodesSlashingVotingStorage interface {
	Insert(votings ...RootnodesSlashingVoting) error
	GetLatest() (*RootnodesSlashingVoting, error)

	GetEarliestUnfinishedVotings() ([]RootnodesSlashingVoting, error)
	Update(voting *RootnodesSlashingVoting) error
}
