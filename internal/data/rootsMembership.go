package data

import (
	"time"
)

// RootsMembership .
type RootsMembership struct {
	PeriodId    uint64    `db:"period_id"`
	RootAddress string    `db:"root_address"`
	StartBlock  int64     `db:"start_block"`
	EndBlock    int64     `db:"end_block"`
	StartTime   time.Time `db:"start_time"`
	EndTime     time.Time `db:"end_time"`
}

// RootsMembershipStorage stores roots membership periods.
type RootsMembershipStorage interface {
	Insert(rnMembership *RootsMembership) error
	GetLatest() (*RootsMembership, error)

	GetUnfinishedPeriodByAddress(rootAddress string) (*RootsMembership, error)
	GetUnfinishedPeriodsByAddress(rootAddress string) ([]RootsMembership, error)
	FindNextPeriodByAddress(rootAddress string, startBlock int64) (*RootsMembership, error)
	GetUnfinishedPeriods() ([]RootsMembership, error)
	// SelectActiveRootAddresses returns list of active root node addresses in lowercase
	SelectActiveRootAddresses() ([]string, error)

	Update(rnMembership *RootsMembership) error

	Select(params *SelectRootsMembershipParams) ([]RootsMembership, error)

	// Erase all history starting fromID inclusively.
	Erase(fromID uint64) error
}

type SelectRootsMembershipParams struct {
	IsDesc bool
	Cursor *uint64
	Limit  uint64

	RootAddress string
}
