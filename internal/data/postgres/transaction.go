package postgres

import (
	"database/sql"
	"fmt"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type TransactionStorage struct {
	db *pgdb.DB
}

// Insert txs.
func (s *TransactionStorage) Insert(txs ...data.Transaction) error {
	if len(txs) == 0 {
		return nil
	}

	query := squirrel.Insert("transactions").Columns(
		"hash",
		"block_id",
		"sender",
		"recipient",
		"value",
		"nonce",
		"gas_price",
		"gas",
		"input",
		"timestamp",
	)

	for _, tx := range txs {
		query = query.Values(
			tx.Hash,
			tx.Block,
			tx.Sender,
			tx.Recipient,
			tx.Value,
			tx.Nonce,
			tx.GasPrice,
			tx.Gas,
			tx.Input,
			tx.Timestamp,
		)
	}

	return errors.Wrap(s.db.Exec(query), "failed to insert txs")
}

// Get Transaction by hash.
func (s *TransactionStorage) Get(hash string) (*data.Transaction, error) {
	query := squirrel.Select("*").From("transactions").
		Where("hash = ?", hash)

	var tx data.Transaction
	switch err := s.db.Get(&tx, query); err {
	case nil:
		return &tx, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get tx by hash")
	}
}

// Select txs.
func (s *TransactionStorage) Select(params *data.SelectTxsParams) ([]data.Transaction, error) {
	query := squirrel.Select("transactions.*").From("transactions")

	if params.Counterparty != nil {
		query = query.Where(
			"(transactions.sender = ? OR transactions.recipient = ?)",
			*params.Counterparty,
			*params.Counterparty,
		)
	}

	if params.Sender != nil {
		query = query.Where("(transactions.sender = ?)", *params.Sender)
	}

	if params.Recipient != nil {
		query = query.Where("(transactions.recipient = ?)", *params.Recipient)
	}

	order, sign := "DESC", "<"
	if !params.IsDesc {
		order, sign = "ASC", ">"
	}
	query = query.OrderBy(fmt.Sprintf("transactions.block_id %s, transactions.hash %s", order, order)).
		Limit(params.Limit)

	if params.Cursor != nil {
		cursor, err := s.Get(*params.Cursor)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get tx at cursor")
		}

		if cursor == nil {
			return nil, nil
		}

		// todo: there is a simpler way to do this, don't remember it now
		query = query.Where(
			fmt.Sprintf("((transactions.block_id, transactions.hash) %s (?, ?))", sign),
			cursor.Block,
			cursor.Hash,
		)
	}

	var txs []data.Transaction
	if err := s.db.Select(&txs, query); err != nil {
		return nil, errors.Wrap(err, "failed to select transactions")
	}

	return txs, nil
}
