package postgres

import (
	"gitlab.com/distributed_lab/kit/pgdb"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// Storage backed by postgres.
type Storage struct {
	db *pgdb.DB
}

// New Storage.
func New(db *pgdb.DB) *Storage {
	return &Storage{db: db}
}

// Clone Storage for a new tx.
func (s *Storage) Clone() data.Storage {
	return &Storage{db: s.db.Clone()}
}

// ExecInTx isolates execution of fn within sql transaction.
func (s *Storage) ExecInTx(fn func() error) error {
	return s.db.Transaction(fn)
}

// Blocks.
func (s *Storage) Blocks() data.BlockStorage {
	return &BlockStorage{db: s.db}
}

// SummaryMetrics.
func (s *Storage) SummaryMetrics() data.SummaryMetricsStorage {
	return &SummaryMetricsStorage{db: s.db}
}

// Transactions.
func (s *Storage) Transactions() data.TransactionStorage {
	return &TransactionStorage{db: s.db}
}

// Payments storage.
func (s *Storage) Payments() data.PaymentsStorage {
	return &PaymentStorage{db: s.db}
}

func (s *Storage) Delegations() data.DelegationsStorage {
	return &DelegationStorage{db: s.db}
}

func (s *Storage) DelegationBlock() data.DelegationBlocksStorage {
	return &DelegationBlockStorage{db: s.db}
}

// Payments storage.
func (s *Storage) Votings() data.VotingStorage {
	return &VotingStorage{db: s.db}
}

// ConstitutionVotings storage.
func (s *Storage) ConstitutionVotings() data.ConstitutionVotingStorage {
	return &ConstitutionVotingStorage{db: s.db}
}

// GeneralUpdateVotings storage.
func (s *Storage) GeneralUpdateVotings() data.GeneralUpdateVotingStorage {
	return &GeneralUpdateVotingStorage{db: s.db}
}

// EmergencyUpdateVotings storage.
func (s *Storage) EmergencyUpdateVotings() data.EmergencyUpdateVotingStorage {
	return &EmergencyUpdateVotingStorage{db: s.db}
}

// RootsVotings storage.
func (s *Storage) RootsVotings() data.RootsVotingStorage {
	return &RootsVotingStorage{db: s.db}
}

// RootnodesSlashingVotings storage.
func (s *Storage) RootnodesSlashingVotings() data.RootnodesSlashingVotingStorage {
	return &RootnodesSlashingVotingStorage{db: s.db}
}

// ValidatorsSlashingVotings storage.
func (s *Storage) ValidatorsSlashingVotings() data.ValidatorsSlashingVotingStorage {
	return &ValidatorsSlashingVotingStorage{db: s.db}
}

// EpqfiMembershipVotings storage.
func (s *Storage) EpqfiMembershipVotings() data.EpqfiMembershipVotingStorage {
	return &EpqfiMembershipVotingStorage{db: s.db}
}

// EpqfiParametersVotings storage.
func (s *Storage) EpqfiParametersVotings() data.EpqfiParametersVotingStorage {
	return &EpqfiParametersVotingStorage{db: s.db}
}

// EpdrMembershipVotings storage.
func (s *Storage) EpdrMembershipVotings() data.EpdrMembershipVotingStorage {
	return &EpdrMembershipVotingStorage{db: s.db}
}

// EpdrParametersVotings storage.
func (s *Storage) EpdrParametersVotings() data.EpdrParametersVotingStorage {
	return &EpdrParametersVotingStorage{db: s.db}
}

// EprsMembershipVotings storage.
func (s *Storage) EprsMembershipVotings() data.EprsMembershipVotingStorage {
	return &EprsMembershipVotingStorage{db: s.db}
}

// EprsParametersVotings storage.
func (s *Storage) EprsParametersVotings() data.EprsParametersVotingStorage {
	return &EprsParametersVotingStorage{db: s.db}
}

// ContractRegistryAddressVotings storage.
func (s *Storage) ContractRegistryAddressVotings() data.ContractRegistryAddressVotingStorage {
	return &ContractRegistryVotingStorage{db: s.db, tableName: ContractRegistryAddressVotingTable}
}

// ContractRegistryUpgradeVotings storage.
func (s *Storage) ContractRegistryUpgradeVotings() data.ContractRegistryUpgradeVotingStorage {
	return &ContractRegistryVotingStorage{db: s.db, tableName: ContractRegistryUpgradeVotingTable}
}

// ContractRegistryVotings returns a new instance of the querier for contract_registry_votings table
func (s *Storage) ContractRegistryVotings() data.ContractRegistryVotingStorage {
	return &ContractRegistryVotingStorage{db: s.db, tableName: ContractRegistryVotingTable}
}

// SystemContracts storage.
func (s *Storage) SystemContracts() data.SystemContractsStorage {
	return NewSystemContractsStorage(s.db)
}

// LayerZeroLists storage
func (s *Storage) LayerZeroLists() data.LayerZeroListStorage {
	return NewLayerZeroListQ(s.db)
}

// RootsMembership storage.
func (s *Storage) RootsMembership() data.RootsMembershipStorage {
	return &RootsMembershipStorage{db: s.db}
}

// QTokenHolderVotes storage.
func (s *Storage) QTokenHolderVotes() data.QTokenHolderVotesStorage {
	return &QTokenHolderVotesStorage{VotingEventsStorage: &VotingEventsStorage{
		db:       s.db,
		database: "q_token_holder_votes",
	}}
}

// RootNodeVotes storage.
func (s *Storage) RootNodeVotes() data.RootNodeVotesStorage {
	return &RootNodeVotesStorage{VotingEventsStorage: &VotingEventsStorage{
		db:       s.db,
		database: "root_node_votes",
	}}
}

// RootNodeProposals storage.
func (s *Storage) RootNodeProposals() data.RootNodeProposalsStorage {
	return &RootNodeProposalsStorage{VotingEventsStorage: &VotingEventsStorage{
		db:       s.db,
		database: "root_node_proposals",
	}}
}

// VotingAgents storage.
func (s *Storage) VotingAgents() data.VotingAgentStorage {
	return &VotingAgentStorage{db: s.db}
}

// RootNodeApprovals storage.
func (s *Storage) RootNodeApprovals() data.RootNodeApprovalsStorage {
	return &RootNodeApprovalsStorage{db: s.db}
}
