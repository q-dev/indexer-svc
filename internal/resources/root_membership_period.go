package resources

import (
	"strconv"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type RootMembershipPeriodList struct {
	Data  []RootMembershipPeriod `json:"data"`
	Links *Links                 `json:"links,omitempty"`
}

type RootMembershipPeriod struct {
	ID   string `json:"id"`
	Type string `json:"type"`

	Attributes RootMembershipPeriodAttrs `json:"attributes"`
}

type RootMembershipPeriodAttrs struct {
	RootAddress string `json:"rootAddress"`
	StartBlock  int64  `json:"startBlock"`
	EndBlock    int64  `json:"endBlock"`
	StartTime   int64  `json:"startTime"`
	EndTime     int64  `json:"endTime"`
}

// PopulateRootMembershipPeriodList from data layer.
func PopulateRootMembershipPeriodList(rootsMemberships []data.RootsMembership) *RootMembershipPeriodList {
	resp := &RootMembershipPeriodList{
		Data: make([]RootMembershipPeriod, len(rootsMemberships)),
	}
	for i, rM := range rootsMemberships {
		resp.Data[i] = RootMembershipPeriod{
			ID:   strconv.FormatUint(rM.PeriodId, 10),
			Type: "rootMembershipPeriod",
			Attributes: RootMembershipPeriodAttrs{
				RootAddress: rM.RootAddress,
				StartBlock:  rM.StartBlock,
				EndBlock:    rM.EndBlock,
				StartTime:   rM.StartTime.Unix(),
				EndTime:     rM.EndTime.Unix(),
			},
		}
	}

	return resp
}
