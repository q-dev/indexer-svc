package resources

import "gitlab.com/q-dev/indexer-svc/internal/data"

type AggregatedEventsList struct {
	Data  CountEvents `json:"data"`
	Links *Links      `json:"links,omitempty"`
}

type CountEvents struct {
	ID         string           `json:"id"`
	Type       string           `json:"type"`
	Attributes CountEventsAttrs `json:"attributes"`
}

type CountEventsByAddress struct {
	AccountAddress string            `json:"accountAddress"`
	Counts         map[string]uint64 `json:"counts"`
}

type CountEventsAttrs struct {
	TotalCounts     map[string]uint64      `json:"total"`
	CountsByAddress []CountEventsByAddress `json:"byAddress"`
}

// PopulateAggregatedEventsList from data layer.
func PopulateAggregatedEventsList(aggregatedEvents data.CountVotingEvents) *AggregatedEventsList {
	resp := &AggregatedEventsList{
		Data: CountEvents{
			ID:   "0",
			Type: "aggregatedVotingEvents",
			Attributes: CountEventsAttrs{
				TotalCounts:     aggregatedEvents.Total,
				CountsByAddress: make([]CountEventsByAddress, len(aggregatedEvents.ByAddress)),
			},
		},
	}
	for i, countsByAddress := range aggregatedEvents.ByAddress {
		resp.Data.Attributes.CountsByAddress[i] = CountEventsByAddress{
			AccountAddress: countsByAddress.AccountAddress,
			Counts:         countsByAddress.Counts,
		}
	}

	return resp
}
