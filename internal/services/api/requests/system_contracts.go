package requests

import (
	"net/http"
	"reflect"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
)

type GetSystemContractListRequest struct {
	Sort   string  `schema:"sort"`
	Cursor *uint64 `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	Contracts []string `schema:"filter[contracts]"`
	Keys      []string `schema:"filter[keys]"`
}

func NewGetSystemContractListRequest(r *http.Request) (GetSystemContractListRequest, error) {
	var request GetSystemContractListRequest
	request.Limit = defaultListLimit
	request.Sort = "desc"

	decoder := schema.NewDecoder()
	decoder.RegisterConverter([]string{}, func(input string) reflect.Value {
		return reflect.ValueOf(strings.Split(input, ","))
	})
	if err := decoder.Decode(&request, r.URL.Query()); err != nil {
		return request, err
	}

	return request, request.Validate()
}

func (r *GetSystemContractListRequest) Validate() error {
	return validation.Errors{
		"sort":  validation.Validate(r.Sort, validation.In("asc", "desc")),
		"limit": validation.Validate(r.Limit, validation.Max(maxListLimit)),
	}.Filter()
}
