package resources

import (
	"strconv"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type EventsList struct {
	Data  []Event `json:"data"`
	Links *Links  `json:"links,omitempty"`
}

type Event struct {
	ID         string     `json:"id"`
	Type       string     `json:"type"`
	Attributes EventAttrs `json:"attributes"`
}

type EventAttrs struct {
	AccountAddress string `json:"accountAddress"`
	VotingType     string `json:"votingType"`
	ProposalId     uint64 `json:"proposalId"`
	BlockId        uint64 `json:"blockId"`
}

func PopulateEventsList(events []data.VotingEvent) *EventsList {
	resp := &EventsList{
		Data: make([]Event, len(events)),
	}
	for i, event := range events {
		resp.Data[i] = Event{
			ID:   strconv.FormatInt(int64(event.Id), 10),
			Type: "votingEvents",
			Attributes: EventAttrs{
				AccountAddress: event.AccountAddress,
				VotingType:     event.VotingType,
				ProposalId:     event.ProposalId,
				BlockId:        event.BlockId,
			},
		}
	}
	return resp
}
