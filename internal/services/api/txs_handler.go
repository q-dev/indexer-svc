package api

import (
	"net/http"
	"net/url"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

type txHandler struct {
	log     *logan.Entry
	storage data.TransactionStorage
}

type getTxHistoryQuery struct {
	Counterparty *string `schema:"filter[counterparty],omitempty"`
	Sender       *string `schema:"filter[sender],omitempty"`
	Recipient    *string `schema:"filter[recipient],omitempty"`

	Sort   string  `schema:"sort"`
	Cursor *string `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`
}

func (q *getTxHistoryQuery) Validate() error {
	return validation.Errors{
		"filter[counterparty]": func() error {
			if q.Counterparty != nil && (q.Sender != nil || q.Recipient != nil) {
				return errors.New("filter[counterparty] can not be used with filter[sender] or filter[recipient]")
			}

			return isAddress(q.Counterparty)
		}(),
		"filter[sender]":    isAddress(q.Sender),
		"filter[recipient]": isAddress(q.Recipient),
		"sort":              validation.Validate(q.Sort, validation.In("block", "-block")),
		"page[limit]":       validation.Validate(int64(q.Limit), validation.Max(100)),
	}.Filter()
}

// @Summary   	Get transactions
// @Description Returns history of transactions
// @ID       	transactions
// @Accept    	json
// @Produce    	json
// @Param     	filter[sender] query string false "Filter by sender."
// @Param     	filter[recipient] query string false "Filter by recipient."
// @Param     	filter[counterparty] query string false "Returns transactions either where a given address is either sender or recipient. Can not be used with `filter[sender]`."
// @Param     	sort query string false "Sorting order. Supported values: `block` (sort by block number, asc), `-block` (sort by block number, desc). Default - `-block`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `10`."
// @Success   	200 {object} resources.TransactionsList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/transactions [get]
func (h *txHandler) getTxHistory(w http.ResponseWriter, r *http.Request) {
	query := getTxHistoryQuery{
		Sort:  "-block",
		Limit: 10,
	}

	if err := schema.NewDecoder().Decode(&query, r.URL.Query()); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	if err := query.Validate(); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	txs, err := h.storage.Select(&data.SelectTxsParams{
		Limit:        query.Limit,
		IsDesc:       strings.HasPrefix(query.Sort, "-"),
		Cursor:       query.Cursor,
		Recipient:    transformAddress(query.Recipient),
		Sender:       transformAddress(query.Sender),
		Counterparty: transformAddress(query.Counterparty),
	})

	if err != nil {
		h.log.WithError(err).Error("failed to query txs history")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	resp := resources.PopulateTransactionsList(txs)
	resp.Links = buildLinks(txs, *r.URL, query)
	ape.Render(w, resp)
}

func buildLinks(txs []data.Transaction, endpoint url.URL, query getTxHistoryQuery) *resources.Links {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(query, params); err != nil {
		panic(errors.Wrap(err, "failed to encode query"))
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(txs) == 0 {
		return links
	}

	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			params.Set("page[cursor]", txs[len(txs)-1].Hash)
			url.RawQuery = params.Encode()
			return url.String()
		}

		params.Set("page[cursor]", txs[0].Hash)
		if sort := params.Get("sort"); strings.HasPrefix(sort, "-") {
			params.Set("sort", strings.TrimPrefix(sort, "-"))
		} else {
			params.Set("sort", "-"+sort)
		}

		url.RawQuery = params.Encode()
		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)
	return links
}
