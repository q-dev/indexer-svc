package resources

import (
	"fmt"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type VotingsList struct {
	Data  []Voting `json:"data"`
	Links *Links   `json:"links,omitempty"`
}

type Voting struct {
	ID
	Attributes VotingAttributes `json:"attributes"`
}

type approvalProposalStatus uint8

const (
	ApprovalProposalStatusNone approvalProposalStatus = iota
	ApprovalProposalStatusPending
	ApprovalProposalStatusExpired
	ApprovalProposalStatusExecuted
)

type membershipVotingStatus uint8

const (
	MembershipVotingStatusNone membershipVotingStatus = iota
	MembershipVotingStatusPending
	MembershipVotingStatusRejected
	MembershipVotingStatusAccepted
	MembershipVotingStatusPassed
	MembershipVotingStatusExecuted
	MembershipVotingStatusObsolete
	MembershipVotingStatusExpired
)

type votingStatusId uint8

const (
	VotingStatusNone votingStatusId = iota
	VotingStatusPending
	VotingStatusExpired
	VotingStatusExecuted
	VotingStatusRejected
	VotingStatusAccepted
	VotingStatusPassed
	VotingStatusObsolete
)

type VotingAttributes struct {
	VotingStatus     votingStatusId `json:"voting_status"`
	VotingStartTime  uint64         `json:"voting_start_time"`
	RootNodesNumber  uint64         `json:"root_nodes_number"`
	VotingEndTime    uint64         `json:"voting_end_time"`
	CurrentMajority  *string        `json:"current_majority,omitempty"`
	RequiredMajority *string        `json:"required_majority,omitempty"`
	CurrentQuorum    *string        `json:"current_quorum,omitempty"`
	RequiredQuorum   *string        `json:"required_quorum,omitempty"`
	VotesNumber      *uint64        `json:"votes_number,omitempty"`
	VetosNumber      *uint64        `json:"vetos_number,omitempty"`
	VetoEndTime      *uint64        `json:"veto_end_time,omitempty"`
	VotingSubType    *uint8         `json:"voting_sub_type,omitempty"`
	Candidate        *string        `json:"candidate,omitempty"`
	ReplaceDest      *string        `json:"replace_dest,omitempty"`
}

func PopulateVotingsList(votings []data.GeneralVoting) (*VotingsList, error) {
	resp := &VotingsList{
		Data: make([]Voting, len(votings)),
	}

	for i, voting := range votings {
		resp.Data[i] = populateVoting(voting)
	}

	return resp, nil
}

func populateVoting(voting data.GeneralVoting) Voting {
	populated := Voting{
		ID: getVotingID(voting.VotingType, voting.ProposalId),
		Attributes: VotingAttributes{
			VotingStatus:    getVotingStatusId(voting.VotingType, voting.VotingStatus),
			VotingStartTime: voting.VotingStartTime,
			VotingEndTime:   voting.VotingEndTime,
			RootNodesNumber: voting.RootNodesNumber,
		},
	}

	if voting.CurrentMajority.Valid {
		populated.Attributes.CurrentMajority = &voting.CurrentMajority.String
	}
	if voting.RequiredMajority.Valid {
		populated.Attributes.RequiredMajority = &voting.RequiredMajority.String
	}
	if voting.CurrentQuorum.Valid {
		populated.Attributes.CurrentQuorum = &voting.CurrentQuorum.String
	}
	if voting.RequiredQuorum.Valid {
		populated.Attributes.RequiredQuorum = &voting.RequiredQuorum.String
	}
	if voting.VotesNumber.Valid {
		votesNumber := uint64(voting.VotesNumber.Int64)
		populated.Attributes.VotesNumber = &votesNumber
	}
	if voting.VetosNumber.Valid {
		vetosNumber := uint64(voting.VetosNumber.Int64)
		populated.Attributes.VetosNumber = &vetosNumber
	}
	if voting.VetoEndTime.Valid {
		vetoEndTime := uint64(voting.VetoEndTime.Int64)
		populated.Attributes.VetoEndTime = &vetoEndTime
	}
	if voting.VotingSubType.Valid {
		populated.Attributes.VotingSubType = &voting.VotingSubType.Byte
	}
	if voting.Candidate.Valid {
		populated.Attributes.Candidate = &voting.Candidate.String
	}
	if voting.ReplaceDest.Valid {
		populated.Attributes.ReplaceDest = &voting.ReplaceDest.String
	}

	return populated
}

func getVotingID(votingType string, proposalID uint64) ID {
	return ID{
		ID:   fmt.Sprintf("%s:%d", votingType, proposalID),
		Type: "votings",
	}
}

func getVotingStatusId(votingType string, status uint8) votingStatusId {
	switch votingType {
	case "address_voting", "upgrade_voting":
		return approvalProposalStatus(status).getStatusId()
	default:
		return membershipVotingStatus(status).getStatusId()
	}
}

func (a approvalProposalStatus) getStatusId() votingStatusId {
	switch a {
	case ApprovalProposalStatusNone:
		return VotingStatusNone
	case ApprovalProposalStatusPending:
		return VotingStatusPending
	case ApprovalProposalStatusExpired:
		return VotingStatusExpired
	case ApprovalProposalStatusExecuted:
		return VotingStatusExecuted
	default:
		return VotingStatusNone
	}
}

func (a membershipVotingStatus) getStatusId() votingStatusId {
	switch a {
	case MembershipVotingStatusNone:
		return VotingStatusNone
	case MembershipVotingStatusPending:
		return VotingStatusPending
	case MembershipVotingStatusRejected:
		return VotingStatusRejected
	case MembershipVotingStatusAccepted:
		return VotingStatusAccepted
	case MembershipVotingStatusPassed:
		return VotingStatusPassed
	case MembershipVotingStatusExecuted:
		return VotingStatusExecuted
	case MembershipVotingStatusObsolete:
		return VotingStatusObsolete
	case MembershipVotingStatusExpired:
		return VotingStatusExpired
	default:
		return VotingStatusNone
	}
}
