package resources

import (
	"encoding/hex"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type TransactionResponse struct {
	Data  Transaction `json:"data"`
	Links *Links      `json:"links,omitempty"`
}

func PopulateTransactionResponse(tx *data.Transaction) *TransactionResponse {
	return &TransactionResponse{
		Data: newTransaction(tx),
	}
}

type TransactionsList struct {
	Data  []Transaction `json:"data"`
	Links *Links        `json:"links,omitempty"`
}

type Transaction struct {
	ID         string           `json:"id"`
	Type       string           `json:"type"`
	Attributes TransactionAttrs `json:"attributes"`
}

type TransactionAttrs struct {
	Block     uint64  `json:"block_id"`
	Sender    string  `json:"sender"`
	Recipient *string `json:"recipient"`
	Value     string  `json:"value"`

	Nonce    uint64 `json:"nonce"`
	GasPrice string `json:"gas_price"`
	Gas      uint64 `json:"gas"`
	Input    string `json:"input"`

	Timestamp int64 `json:"timestamp"`
}

type Links struct {
	Self string `json:"self"`
	Prev string `json:"prev,omitempty"`
	Next string `json:"next,omitempty"`
}

// PopulateTransactionsList creates a tx list response.
func PopulateTransactionsList(txs []data.Transaction) *TransactionsList {
	models := make([]Transaction, len(txs))
	for i := range txs {
		models[i] = newTransaction(&txs[i])
	}

	return &TransactionsList{
		Data: models,
	}
}

func newTransaction(tx *data.Transaction) Transaction {
	input := hex.EncodeToString(tx.Input)
	return Transaction{
		ID:   tx.Hash,
		Type: "transactions",
		Attributes: TransactionAttrs{
			Block:     tx.Block,
			Sender:    tx.Sender,
			Recipient: tx.Recipient,
			Value:     tx.Value.Unwrap().String(),
			Nonce:     tx.Nonce,
			GasPrice:  tx.GasPrice.Unwrap().String(),
			Gas:       tx.Gas,
			Input:     input,
			Timestamp: tx.Timestamp,
		},
	}
}

func newTxKey(tx *data.Transaction) Key {
	return Key{
		ID:   tx.Hash,
		Type: "transactions",
	}
}
