# API reference
This service implements [jsonapi](https://jsonapi.org) spec.

## Votings history
**GET** `/votings`

Returns history of votings.

Supported url query parameters:
* `filter[type]` Filter votings by type. Supported values : constitutionVoting, generalUpdateVoting, emergencyUpdateVoting,
rootNodesMembershipVoting, rootNodesSlashingVoting, validatorsSlashingVoting, epdrMembershipVoting, epdrParametersVoting,
epqfiMembershipVoting, epqfiParametersVoting, eprsMembershipVoting, eprsParametersVoting,
addressVoting, upgradeVoting.
* `filter[status]` Filter by status. Supported values 0..7.

Pagination filters:
* `sort` Sorting order. Supported values: `voting_end_time` (sort by voting end time, asc),
  `-voting_end_time` (sort by voting end time, desc). Default - `-voting_end_time`
* `page[cursor]` Last seen element.
* `page[limit]` Number of elements on page. Default value - 20.
* 
### Example:
**GET** `/votings?filter[type]=ConstitutionVoting,GeneralUpdateVoting&filter[status]=0,2`

```json
{
  "data": [
    {
      "ProposalId": 181,
      "VotingType": "GeneralUpdateVoting",
      "VotingStatus": 2,
      "VotingStartTime": 1676535888,
      "CurrentQuorum": "6903236642301600250298779",
      "RequiredQuorum": "50000000000000000000",
      "VotingEndTime": 1676967888,
      "VetoEndTime": 1677140688
    },
    {
      "ProposalId": 54,
      "VotingType": "ConstitutionVoting",
      "VotingSubType": 1,
      "VotingStatus": 2,
      "VotingStartTime": 0,
      "CurrentQuorum": "3372027458035694193544",
      "RequiredQuorum": "150000000000000000000000000",
      "VotingEndTime": 1673797115,
      "VetoEndTime": 1673969915
    },
  ],
  "links": {
    "self": "/votings?filter%5Bstatus%5D=0%2C2&filter%5Btype%5D=%27ConstitutionVoting%27%2C%27GeneralUpdateVoting%27&page%5Bcursor%5D=0&page%5Blimit%5D=20&sort=-voting_end_time",
    "prev": "/votings?filter%5Bstatus%5D=0%2C2&filter%5Btype%5D=%27ConstitutionVoting%27%2C%27GeneralUpdateVoting%27&page%5Bcursor%5D=0&page%5Blimit%5D=20&sort=-voting_end_time",
    "next": "/votings?filter%5Bstatus%5D=0%2C2&filter%5Btype%5D=%27ConstitutionVoting%27%2C%27GeneralUpdateVoting%27&page%5Bcursor%5D=20&page%5Blimit%5D=20&sort=-voting_end_time"
  }
}
```

## Metrics history
**GET** `/metrics`

Returns history of metrics.

Supported url query parameters:
* `filter[cycles]` Filter by amount of cycles.
* `filter[signer]` Filter by actual signer.
* `filter[mainaccount]` Filter by main account.

Pagination filters:
* `sort` Sorting order. Supported values: `mainaccount` (sort by id, asc),
  `-mainaccount` (sort by id, desc). Default - `-mainaccount`
* `page[cursor]` Last seen element.
* `page[limit]` Number of elements on page. Default value - 20.

### Example:
**GET** `/metrics?filter[cycles]=1`

```json
{
  "data": [
    {
      "id": "0",
      "metrics": "metrics",
      "attributes": {
        "MainAccount": "0x101bf5cefc653a663311368d68cb79fafb76a25a",
        "DueBlocks": 10,
        "InTurnBlocks": 0,
        "OutOfTurnBlocks": 10,
        "MetricsByMiner": {
          "0x101bf5cefc653a663311368d68cb79fafb76a25a": {
            "DueBlocks": 10,
            "InTurnBlocks": 0,
            "OutOfTurnBlocks": 10
          }
        }
      }
    },
    {
      "id": "1",
      "metrics": "metrics",
      "attributes": {
        "MainAccount": "0x1da8250d468c5ccad40b987690e61e68bed957cd",
        "DueBlocks": 10,
        "InTurnBlocks": 0,
        "OutOfTurnBlocks": 10,
        "MetricsByMiner": {
          "0x1da8250d468c5ccad40b987690e61e68bed957cd": {
            "DueBlocks": 10,
            "InTurnBlocks": 0,
            "OutOfTurnBlocks": 10
          }
        }
      }
    }
  ],
  "links": {
    "self": "/metrics?filter%5Bcycles%5D=1&page%5Blimit%5D=20&sort=mainaccount",
    "prev": "/metrics?filter%5Bcycles%5D=1&page%5Bcursor%5D=0&page%5Blimit%5D=20&sort=-mainaccount",
    "next": "/metrics?filter%5Bcycles%5D=1&page%5Bcursor%5D=9&page%5Blimit%5D=20&sort=mainaccount"
  }
}
```

## Transactions history
**GET** `/transactions`

Returns history of transactions. 

Supported url query parameters:
* `filter[sender]`  Filter transactions by sender
* `filter[recipient]` Filter transactions by recipient 
* `filter[counterparty]` Returns transactions either where a given address
is either sender or recipient. Can not be used with `filter[sender]`
or `filter[recipient]`

Pagination filters:
* `sort` Sorting order. Supported values: `block` (sort by block number, asc), 
`-block` (sort by block number, desc). Default - `-block`
* `page[cursor]` Last seen element.
* `page[limit]` Number of elements on page, max allowed value - 100. Default value - 10.

### Example:
**GET** `/transactions?filter[sender]=0x532c69263800e1f1Cdb72acaE555A85864146986`

```json
{
  "data": [
    {
      "id": "0x11eac5b567e3db6f83e5086018333399a1de0ec3aea59a34ed592ff326363f77",
      "type": "transactions",
      "attributes": {
        "block_id": 500,
        "sender": "0x532c69263800e1f1cdb72acae555a85864146986",
        "recipient": "0x836d1a5f0b237cbab0adfbb7706bab9ff6af8fe5",
        "value": "500000000000000000",
        "nonce": 1,
        "gas_price": "10000000000",
        "gas": 10000,
        "input": ""
      }
    },
    {
      "id": "0x43487050130362ab33a7397a9059dfa149284fa348f36e0e59a09c086991e7ef",
      "type": "transactions",
      "attributes": {
        "block_id": 100,
        "sender": "0x532c69263800e1f1cdb72acae555a85864146986",
        "recipient": "0x836d1a5f0b237cbab0adfbb7706bab9ff6af8fe5",
        "value": "250000000000000000",
        "nonce": 1,
        "gas_price": "10000000000",
        "gas": 10000,
        "input": ""
      }
    }
  ],
  "links": {
    "self": "/transactions?filter%5Bsender%5D=0x532c69263800e1f1Cdb72acaE555A85864146986&page%5Blimit%5D=10&sort=-block",
    "prev": "/transactions?filter%5Bsender%5D=0x532c69263800e1f1Cdb72acaE555A85864146986&page%5Bcursor%5D=0x11eac5b567e3db6f83e5086018333399a1de0ec3aea59a34ed592ff326363f77&page%5Blimit%5D=10&sort=block",
    "next": "/transactions?filter%5Bsender%5D=0x532c69263800e1f1Cdb72acaE555A85864146986&page%5Bcursor%5D=0x43487050130362ab33a7397a9059dfa149284fa348f36e0e59a09c086991e7ef&page%5Blimit%5D=10&sort=-block"
  }
}
```


## Blocks history
**GET** `/blocks`

Returns history of transactions.

Supported url query parameters:
* `filter[from]`  Return blocks starting with `from` number.
* `filter[to]` Return blocks with number less than `to` (default: latest)
* `filter[signer]` Filter by signer
* `filter[inturnsigner]` Filter by inturn signer
* `filter[anysigner]` Filter by signer or inturn signer
* `filter[outofturn]` Filter for out-of-turn blocks

Pagination filters:
* `sort` Sorting order. Supported values: `id` (sort by block number, asc),
  `-id` (sort by block number, desc). Default - `id`
* `page[cursor]` Last seen element.
* `page[limit]` Number of elements on page, max allowed value - 100. Default value - 10.

### Example:
**GET** `/blocks?filter[from]=128&page[limit]=5`

```json
{
  "data": [
    {
      "id": "128",
      "type": "blocks",
      "attributes": {
        "hash": "0xb432c7de8047cabac9235e70d3f76ce5800e80348b87fded9795354eeea2fa4a",
        "parent_hash": "0xd66f6b812b2ecdefc0675f33f54fa641165c1a7395386f75a9db1da195126eaa",
        "miner": "0x6a39b688d591ea00c9ea69658438794204b5cc62",
        "timestamp": 1647034265,
        "inturn_signer": "0x6a39b688d591ea00c9ea69658438794204b5cc62",
        "difficulty": 2
      }
    },
    {
      "id": "129",
      "type": "blocks",
      "attributes": {
        "hash": "0x53385aa6e4a21066ae87ffa2a204d836bca7797df0baf07bac6b6d7140a3db04",
        "parent_hash": "0xb432c7de8047cabac9235e70d3f76ce5800e80348b87fded9795354eeea2fa4a",
        "miner": "0x4a14d788d86d021670ebcece1196631d66595984",
        "timestamp": 1647034270,
        "inturn_signer": "0x4a14d788d86d021670ebcece1196631d66595984",
        "difficulty": 2
      }
    },
    {
      "id": "130",
      "type": "blocks",
      "attributes": {
        "hash": "0xc7a52d1e20f4b07d93e21aa872507e47bd210317bd64093f589d40e8eb0c7b73",
        "parent_hash": "0x53385aa6e4a21066ae87ffa2a204d836bca7797df0baf07bac6b6d7140a3db04",
        "miner": "0x64d4edefe8ba86d3588b213b0a053e7b910cad68",
        "timestamp": 1647034275,
        "inturn_signer": "0x64d4edefe8ba86d3588b213b0a053e7b910cad68",
        "difficulty": 2
      }
    }
  ],
  "links": {
    "self": "/blocks?filter%5Bfrom%5D=128&page%5Blimit%5D=5&sort=id",
    "prev": "/blocks?filter%5Bfrom%5D=128&page%5Bcursor%5D=128&page%5Blimit%5D=5&sort=-id",
    "next": "/blocks?filter%5Bfrom%5D=128&page%5Bcursor%5D=132&page%5Blimit%5D=5&sort=id"
  }
}
```
