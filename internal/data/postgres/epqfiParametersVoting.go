package postgres

import (
	"database/sql"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// EpqfiParametersVotingStorage implements data.EpqfiParametersVotingStorage.
type EpqfiParametersVotingStorage struct {
	db *pgdb.DB
}

func (s *EpqfiParametersVotingStorage) Insert(votings ...data.EpqfiParametersVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert("epqfi_parameters_votings").Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_quorum",
		"required_quorum",
		"root_nodes_number",
		"vetos_number",
		"voting_end_time",
		"veto_end_time",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentQuorum,
			voting.RequiredQuorum,
			voting.RootNodesNumber,
			voting.VetosNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
			time.Unix(int64(voting.VetoEndTime), 0),
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_quorum=EXCLUDED.current_quorum," +
		"root_nodes_number=EXCLUDED.root_nodes_number," +
		"vetos_number=EXCLUDED.vetos_number",
	)

	return errors.Wrap(s.db.Exec(query), "failed to insert epqfi parameters voting")
}

// GetLatest epqfi parameters voting.
func (s *EpqfiParametersVotingStorage) GetLatest() (*data.EpqfiParametersVoting, error) {
	return s.queryBlock(squirrel.Select(commonVotingColumns...).From("epqfi_parameters_votings").
		Where("proposal_id = (SELECT max(proposal_id) from epqfi_parameters_votings)"))
}

func (s *EpqfiParametersVotingStorage) GetEarliestUnfinishedVotings() ([]data.EpqfiParametersVoting, error) {
	var votings []data.EpqfiParametersVoting
	query := squirrel.Select(commonVotingColumns...).From("epqfi_parameters_votings").
		Where("voting_status IN (1, 3, 4)").OrderBy("proposal_id ASC")
	if err := s.db.Select(&votings, query); err != nil {
		return nil, errors.Wrap(err, "failed to select epqfi parameters votings")
	}
	return votings, nil
}

func (s *EpqfiParametersVotingStorage) Update(voting *data.EpqfiParametersVoting) error {
	query := squirrel.Update("epqfi_parameters_votings").SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_quorum":    voting.CurrentQuorum,
		"root_nodes_number": voting.RootNodesNumber,
		"vetos_number":      voting.VetosNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query), "failed to update epqfi parameters voting")
}

func (s *EpqfiParametersVotingStorage) queryBlock(query squirrel.SelectBuilder) (*data.EpqfiParametersVoting, error) {
	var voting data.EpqfiParametersVoting
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get epqfi parameters voting")
	}
}
