package data

// GeneralUpdateVoting voting.
type GeneralUpdateVoting BaseVoting

// GeneralUpdateVotingStorage stores general update votings.
type GeneralUpdateVotingStorage interface {
	Insert(votings ...GeneralUpdateVoting) error
	GetLatest() (*GeneralUpdateVoting, error)

	GetEarliestUnfinishedVotings() ([]GeneralUpdateVoting, error)
	Update(voting *GeneralUpdateVoting) error
}
