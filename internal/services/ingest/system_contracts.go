package ingest

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/system-contracts/generated"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/vars"
)

type SystemContract struct {
	Contract       common.Address
	Implementation common.Address
	Key            string
}

type SystemContracts []SystemContract

func (s *Service) updateSystemContracts(ctx context.Context) error {
	contracts, err := s.getSystemContracts(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to get system contracts")
	}

	err = s.db.Clone().SystemContracts().Upsert(contracts.toData()...)
	if err != nil {
		return errors.Wrap(err, "failed to update db")
	}

	return nil
}

func (sc *SystemContract) toData() data.SystemContract {
	impl := sql.NullString{
		String: sc.Implementation.String(),
		Valid:  sc.Implementation != common.Address{},
	}

	key := sql.NullString{
		String: sc.Key,
		Valid:  sc.Key != "",
	}

	return data.SystemContract{
		Contract:       sc.Contract.String(),
		Implementation: impl,
		Key:            key,
	}
}

func (scs SystemContracts) toData() []data.SystemContract {
	contracts := make([]data.SystemContract, 0, len(scs))
	for _, contr := range scs {
		contracts = append(contracts, contr.toData())
	}

	return contracts
}

func (s *Service) getSystemContracts(ctx context.Context) (SystemContracts, error) {
	systemContracts := s.contractRegistry.SystemContracts()
	systemContracts = append(systemContracts, generated.IContractRegistryPair{
		Key:  data.ContractRegistryName,
		Addr: s.contractRegistry.GetSelfAddress(),
	})

	contracts := make(SystemContracts, 0, len(systemContracts))
	for _, sc := range systemContracts {
		contract, err := s.getSystemContractImpl(ctx, sc.Addr, sc.Key)
		if err != nil {
			return nil, err
		}

		contracts = append(contracts, contract)
	}

	return contracts, nil
}

func (s *Service) getSystemContractImpl(ctx context.Context, proxy common.Address, key string) (SystemContract, error) {
	storage, err := s.client.StorageAt(ctx, proxy, vars.ProxyImplementationKey, nil)
	if err != nil {
		return SystemContract{}, errors.Wrap(err, fmt.Sprintf("failed to get storage at: %s", vars.ProxyImplementationKey))
	}

	return SystemContract{
		Contract:       proxy,
		Implementation: common.BytesToAddress(storage),
		Key:            key,
	}, nil
}
