package resources

type VersionList struct {
	Data Version `json:"data"`
}

type Version struct {
	VersionNumber string `json:"version"`
}

func PopulateVersion(version string) *VersionList {
	resp := &VersionList{
		Data: Version{VersionNumber: version},
	}

	return resp
}
