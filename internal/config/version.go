package config

import (
	"fmt"
)

const (
	IndexerVersionMajor = 1        // Indexer major version component of the current release
	IndexerVersionMinor = 1        // Indexer minor version component of the current release
	IndexerVersionPatch = 5        // Indexer patch version component of the current release
	IndexerVersionMeta  = "stable" // Indexer version metadata to append to the version string
)

// IndexerVersion holds the textual indexer version string.
var IndexerVersion = func() string {
	return fmt.Sprintf("%d.%d.%d", IndexerVersionMajor, IndexerVersionMinor, IndexerVersionPatch)
}()

// IndexerVersionWithMeta holds the textual indexer version string including the metadata.
var IndexerVersionWithMeta = func() string {
	v := IndexerVersion
	if IndexerVersionMeta != "" {
		v += "-" + IndexerVersionMeta
	}
	return v
}()

func VersionWithCommit(gitCommit, gitDate string) string {
	vsn := IndexerVersionWithMeta
	if len(gitCommit) >= 8 {
		vsn += "-" + gitCommit[:8]
	}
	if (IndexerVersion != "stable") && (gitDate != "") {
		vsn += "-" + gitDate
	}
	return vsn
}
