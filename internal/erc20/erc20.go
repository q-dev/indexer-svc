package erc20

import (
	"bytes"
	"math/big"
	"strings"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/common"
)

type Transfer struct {
	To     string
	Amount *big.Int
}

func ParseTransfer(input []byte) (*Transfer, error) {
	method := tokenABI.Methods["transfer"]
	if !bytes.HasPrefix(input, method.ID) {
		return nil, errors.New("not a transfer method")
	}

	input = bytes.TrimPrefix(input, method.ID)
	args, err := method.Inputs.UnpackValues(input)
	if err != nil {
		return nil, errors.Wrap(err, "not a transfer method")
	}

	if len(args) != 2 {
		return nil, errors.New("wrong number of args")
	}

	to, ok := args[0].(common.Address)
	if !ok {
		return nil, errors.New("invalid first arg")
	}

	amount, ok := args[1].(*big.Int)
	if !ok {
		return nil, errors.New("invalid second arg")
	}

	return &Transfer{
		To:     strings.ToLower(to.Hex()),
		Amount: amount,
	}, nil
}
