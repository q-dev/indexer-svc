package erc20

import (
	"encoding/hex"
	"math/big"
	"testing"
)

func TestParseTransfer(t *testing.T) {
	raw := `a9059cbb000000000000000000000000cca19442f5b3e5fa71aae69c092ac280e81fd39f000000000000000000000000000000000000000000000000000000003b9aca00`
	input, _ := hex.DecodeString(raw)

	res, err := ParseTransfer(input)
	if err != nil {
		t.Fatal(err)
	}

	if res == nil {
		t.Fatal("transfer is nil")
	}

	if res.To != "0xcca19442f5b3e5fa71aae69c092ac280e81fd39f" {
		t.Error("wrong to")
	}

	if res.Amount.Cmp(big.NewInt(1000000000)) != 0 {
		t.Error("wrong amount")
	}
}
