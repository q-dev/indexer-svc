package ingest

import (
	"fmt"
	"io"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"golang.org/x/crypto/sha3"

	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/core/types"
	"gitlab.com/q-dev/q-client/crypto"
	"gitlab.com/q-dev/q-client/rlp"
	"gitlab.com/q-dev/system-contracts/generated"
)

// this duplicates logic in q-client,
// because for some reason, header.Hash() is not the hash being
// signed by miners.
func sealHash(header *types.Header) common.Hash {
	hasher := sha3.NewLegacyKeccak256()
	encodeSigHeader(hasher, header)

	var hash common.Hash
	hasher.Sum(hash[:0])
	return hash
}

func encodeSigHeader(w io.Writer, header *types.Header) {
	err := rlp.Encode(w, []interface{}{
		header.ParentHash,
		header.UncleHash,
		header.Coinbase,
		header.Root,
		header.TxHash,
		header.ReceiptHash,
		header.Bloom,
		header.Difficulty,
		header.Number,
		header.GasLimit,
		header.GasUsed,
		header.Time,
		header.Extra[:len(header.Extra)-crypto.SignatureLength], // Yes, this will panic if extra is too short
		header.MixDigest,
		header.Nonce,
	})
	if err != nil {
		panic("can't encode: " + err.Error())
	}
}

func (s *Service) parseEvent(log types.Log, provider interface{}, voteOrProposal int64) (interface{}, error) {
	switch parser := provider.(type) {
	case *generated.ConstitutionVoting:
		switch voteOrProposal {
		case 0:
			return provider.(*generated.ConstitutionVoting).ParseUserVoted(log)
		}
	case *generated.GeneralUpdateVoting:
		switch voteOrProposal {
		case 0:
			return provider.(*generated.GeneralUpdateVoting).ParseUserVoted(log)
		}
	case *generated.EmergencyUpdateVoting:
		switch voteOrProposal {
		case 1:
			return provider.(*generated.EmergencyUpdateVoting).ParseUserVoted(log)
		case 2:
			return provider.(*generated.EmergencyUpdateVoting).ParseProposalCreated(log)
		}
	case *generated.RootsVoting:
		switch voteOrProposal {
		case 0:
			return provider.(*generated.RootsVoting).ParseUserVoted(log)
		}
	case *generated.RootNodesSlashingVoting:
		switch voteOrProposal {
		case 0:
			return provider.(*generated.RootNodesSlashingVoting).ParseUserVoted(log)
		case 2:
			return provider.(*generated.RootNodesSlashingVoting).ParseProposalCreated(log)
		}
	case *generated.ValidatorsSlashingVoting:
		switch voteOrProposal {
		case 1:
			return provider.(*generated.ValidatorsSlashingVoting).ParseUserVoted(log)
		case 2:
			return provider.(*generated.ValidatorsSlashingVoting).ParseProposalCreated(log)
		}
	case *generated.EPQFIMembershipVoting:
		switch voteOrProposal {
		case 0: // please forgive me, I am just trying to fix it
			return provider.(*generated.EPQFIMembershipVoting).ParseUserVoted(log)
		}
	case *generated.EPRSMembershipVoting:
		switch voteOrProposal {
		case 0: // please forgive me, I am just trying to fix it
			return provider.(*generated.EPRSMembershipVoting).ParseUserVoted(log)
		}
	case *generated.EPDRMembershipVoting:
		switch voteOrProposal {
		case 0: // please forgive me, I am just trying to fix it
			return provider.(*generated.EPDRMembershipVoting).ParseUserVoted(log)
		}
	case *generated.ContractRegistryUpgradeVoting:
		switch voteOrProposal {
		case 1:
			return provider.(*generated.ContractRegistryUpgradeVoting).ParseRootNodeApproved(log)
		}
	case *generated.ContractRegistryAddressVoting:
		switch voteOrProposal {
		case 1:
			return provider.(*generated.ContractRegistryAddressVoting).ParseRootNodeApproved(log)
		}
	case *generated.GenericContractRegistryVoting:
		switch voteOrProposal {
		case 1:
			return parser.ParseRootNodeApproved(log)
		}
	default:
		return nil, errors.From(errors.New("wrong provider type"), logan.F{"provider_type": fmt.Sprintf("%T", provider)})
	}

	s.log.WithFields(logan.F{
		"provider_type":  fmt.Sprintf("%T", provider),
		"voteOrProposal": voteOrProposal,
	}).Error("voting event parsing is skipped, returning nil")
	return nil, nil
}
