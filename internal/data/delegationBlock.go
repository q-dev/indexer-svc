package data

type DelegationBlock struct {
	Number uint64 `db:"number"`
}

type DelegationBlocksStorage interface {
	GetNumber() (uint64, error)
	Update(number uint64) error
}
