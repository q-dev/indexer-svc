-- +migrate Up
CREATE TABLE q_token_holder_votes
(
    id                BIGSERIAL PRIMARY KEY,
    voting_type       TEXT      NOT NULL,
    account_address   CHAR(42)  NOT NULL,
    proposal_id       BIGINT    NOT NULL,
    block_id          BIGINT    NOT NULL
);

CREATE TABLE root_node_votes
(
    id                BIGSERIAL PRIMARY KEY,
    voting_type       TEXT      NOT NULL,
    account_address   CHAR(42)  NOT NULL,
    proposal_id       BIGINT    NOT NULL,
    block_id          BIGINT    NOT NULL
);

CREATE TABLE root_node_proposals
(
    id                BIGSERIAL PRIMARY KEY,
    voting_type       TEXT      NOT NULL,
    account_address   CHAR(42)  NOT NULL,
    proposal_id       BIGINT    NOT NULL,
    block_id          BIGINT    NOT NULL
);

-- +migrate Down
DROP TABLE q_token_holder_votes;
DROP TABLE root_node_votes;
DROP TABLE root_node_proposals;
