package requests

import (
	"net/http"
	"reflect"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
)

type GetRootNodeApprovalsRequest struct {
	Sort   string  `schema:"sort"`
	Cursor *string `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	Cycles      uint64  `schema:"filter[cycles],omitempty"`
	RootAddress *string `schema:"filter[rootAddress],omitempty"`
	BlocksDelay uint64  `schema:"filter[blocksDelay],omitempty"`
}

func NewGetRootNodeApprovalsRequest(r *http.Request) (GetRootNodeApprovalsRequest, error) {
	var request GetRootNodeApprovalsRequest
	request.Limit = maxListLimit

	decoder := schema.NewDecoder()
	decoder.RegisterConverter([]string{}, func(input string) reflect.Value {
		return reflect.ValueOf(strings.Split(input, ","))
	})
	if err := decoder.Decode(&request, r.URL.Query()); err != nil {
		return request, err
	}

	return request, request.Validate()
}

func (r *GetRootNodeApprovalsRequest) Validate() error {
	return validation.Errors{
		"sort":  validation.Validate(r.Sort, validation.In("asc", "desc")),
		"limit": validation.Validate(r.Limit, validation.Max(maxListLimit)),
	}.Filter()
}
