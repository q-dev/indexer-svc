package resources

import (
	"time"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type RootAttributes struct {
	Status    string        `json:"status"`
	Timestamp time.Time     `json:"timestamp"`
	Signers   []AccountPair `json:"signers"`
	Roots     []AccountPair `json:"roots"`

	ActiveRootPercentage float32 `json:"active_root_percentage,omitempty"`
}

type RootListData struct {
	ID   string `json:"id"`
	Type string `json:"type"`

	Attributes RootAttributes `json:"attributes"`
}

type RootList struct {
	Data RootListData `json:"data"`
}

func PopulateRootList(l data.RootNodeList, percentage float32) RootList {
	signers := make([]AccountPair, 0, len(l.Signers))
	for _, signer := range l.Signers {
		mainAccount := GetMainAccount(signer.MainAccount, signer.Alias)

		signers = append(signers, AccountPair{
			MainAccount: mainAccount,
		})
	}

	roots := make([]AccountPair, 0, len(l.RootList))
	for _, root := range l.RootList {
		mainAccount := GetMainAccount(root.MainAccount, root.Alias)

		roots = append(roots, AccountPair{
			MainAccount: mainAccount,
		})
	}

	return RootList{
		Data: RootListData{
			ID:   l.Hash,
			Type: l.Type,
			Attributes: RootAttributes{
				Status:               l.Status,
				Timestamp:            l.Timestamp,
				Signers:              signers,
				Roots:                roots,
				ActiveRootPercentage: percentage,
			},
		},
	}
}
