package api

import (
	"net/http"
	"net/url"
	"strconv"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/gorilla/schema"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

type rootMembershipPeriodHandler struct {
	log     *logan.Entry
	storage data.RootsMembershipStorage
}

type getRootMembershipPeriodQuery struct {
	Sort   string  `schema:"sort"`
	Cursor *uint64 `schema:"page[cursor],omitempty"`
	Limit  uint64  `schema:"page[limit]"`

	RootAddress string `schema:"filter[rootAddress],omitempty"`
}

func (q *getRootMembershipPeriodQuery) Validate() error {
	return validation.Errors{
		"sort":  validation.Validate(q.Sort, validation.In("voting_end_time", "-voting_end_time")),
		"limit": validation.Validate(q.Limit, validation.Max(uint64(100))),
	}.Filter()
}

// @Summary   	Get root node membership
// @Description Returns root node membership periods.
// @ID       	rn_membership
// @Accept    	json
// @Produce    	json
// @Param     	filter[rootAddress] query string false "Returns ALL root node membership periods for `rootAddress`."
// @Param     	sort query string false "Sorting order. Supported values: `block` (sort by block number, asc), `-block` (sort by block number, desc). Default - `-block`."
// @Param     	page[cursor] query string false "Filter by last seen element."
// @Param     	page[limit] query int false "Number of elements on page, max allowed value - `100`. Default value - `20`."
// @Success   	200 {object} resources.RootMembershipPeriodList
// @Failure   	400 "Bad request"
// @Failure   	404 "Not Found"
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/root-node-metrics/onchain-membership [get]
func (h *rootMembershipPeriodHandler) getRootMemberships(w http.ResponseWriter, r *http.Request) {
	cursor := uint64(0)
	query := &getRootMembershipPeriodQuery{
		Sort:   "-voting_end_time",
		Limit:  uint64(20),
		Cursor: &cursor,
	}

	if err := schema.NewDecoder().Decode(query, r.URL.Query()); err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	err := query.Validate()
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	rootMemberships, err := h.storage.Select(&data.SelectRootsMembershipParams{
		IsDesc:      strings.HasPrefix(query.Sort, "-"),
		Cursor:      query.Cursor,
		Limit:       query.Limit,
		RootAddress: query.RootAddress,
	})

	if err != nil {
		h.log.WithError(err).Errorf("failed to query blocks; query %#v", query)
		ape.RenderErr(w, problems.InternalError())
		return
	}

	resp := resources.PopulateRootMembershipPeriodList(rootMemberships)
	resp.Links = buildRootMembershipLinks(rootMemberships, *r.URL, query)
	ape.Render(w, resp)
}

func buildRootMembershipLinks(rootMemberships []data.RootsMembership, endpoint url.URL, query *getRootMembershipPeriodQuery) *resources.Links {
	params := url.Values{}
	if err := schema.NewEncoder().Encode(query, params); err != nil {
		panic(errors.Wrap(err, "failed to encode query"))
	}

	// ensure defaults are in link
	endpoint.RawQuery = params.Encode()
	links := &resources.Links{
		Self: endpoint.String(),
	}

	if len(rootMemberships) == 0 {
		return links
	}

	size := uint64(len(rootMemberships))
	makeLink := func(url url.URL, isReverse bool) string {
		params := url.Query()
		if !isReverse {
			if size < query.Limit {
				return ""
			}
			step := *query.Cursor + query.Limit
			params.Set("page[cursor]", strconv.FormatUint(step, 10))
			url.RawQuery = params.Encode()
			return url.String()
		} else {
			cursor := int64(0)
			if query.Cursor != nil {
				cursor = int64(*query.Cursor)
			}
			step := cursor - int64(query.Limit)
			if step < 0 {
				step = 0
			}
			params.Set("page[cursor]", strconv.FormatUint(uint64(step), 10))
			url.RawQuery = params.Encode()
		}

		return url.String()
	}

	links.Prev = makeLink(endpoint, true)
	links.Next = makeLink(endpoint, false)
	return links
}
