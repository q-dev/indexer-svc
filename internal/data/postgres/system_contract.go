package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

const (
	systemContractsTable = "system_contracts"
)

var (
	sysContractsColumns  = []string{"id", "proxy", "implementation", "key"}
	sysContractsSelector = sq.Select(sysContractsColumns...).From(systemContractsTable)
)

type systemContractsQ struct {
	db *pgdb.DB
}

func NewSystemContractsStorage(db *pgdb.DB) data.SystemContractsStorage {
	return &systemContractsQ{
		db: db,
	}
}

func (q *systemContractsQ) Upsert(contracts ...data.SystemContract) error {
	if len(contracts) == 0 {
		return nil
	}

	stmt := sq.Insert(systemContractsTable).Columns(sysContractsColumns[1:]...)
	for _, contr := range contracts {
		stmt = stmt.Values(contr.Contract, contr.Implementation, contr.Key)
	}
	stmt = stmt.Suffix("ON CONFLICT (proxy) DO UPDATE SET implementation = excluded.implementation")

	return q.db.Exec(stmt)
}

func (q *systemContractsQ) UpdateImplAddress(contract data.SystemContract) error {
	stmt := sq.
		Update(systemContractsTable).
		Where(sq.Eq{
			"proxy": contract.Contract,
		}).
		Set("implementation", contract.Implementation)

	return q.db.Exec(stmt)
}

func (q *systemContractsQ) Select(params data.SelectSysContractsParams) ([]data.SystemContract, error) {
	var contracts []data.SystemContract

	order, sign := "ASC", ">"
	if params.IsDesc {
		order, sign = "DESC", "<"
	}

	query := sysContractsSelector.
		Limit(params.Limit).
		OrderBy(fmt.Sprintf("id %s", order))

	if params.Cursor != nil {
		query = query.Where(
			fmt.Sprintf("id %s ?", sign),
			*params.Cursor,
		)
	}

	if len(params.Contracts) != 0 {
		query = query.Where(
			sq.Eq{
				"proxy": params.Contracts,
			},
		)
	}

	if len(params.Keys) != 0 {
		query = query.Where(
			sq.Eq{
				"key": params.Keys,
			},
		)
	}

	query = query.Where(sq.NotEq{
		"key": data.ContractRegistryName,
	})

	err := q.db.Select(&contracts, query)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return contracts, nil
}

func (q *systemContractsQ) GetContractRegistry() (*data.SystemContract, error) {
	var registry data.SystemContract
	if err := q.db.Get(&registry, sysContractsSelector.Where(
		sq.Eq{
			"key": data.ContractRegistryName,
		},
	)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	return &registry, nil
}
