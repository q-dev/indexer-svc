package data

// RootsVoting voting.
type RootsVoting struct {
	BaseVoting
	Candidate   string `db:"candidate"`
	ReplaceDest string `db:"replace_dest"`
}

// RootsVotingStorage stores roots votings.
type RootsVotingStorage interface {
	Insert(votings ...RootsVoting) error
	GetLatest() (*RootsVoting, error)

	GetEarliestUnfinishedVotingIds() ([]uint64, error)
	Update(voting *RootsVoting) error
}
