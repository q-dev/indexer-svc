FROM golang:1.21 AS build

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY cmd/ cmd/
COPY internal/ internal/
COPY docs/ docs/


RUN CGO_ENABLED=0 \
    GOOS=linux \
    go build -o /go/bin/indexer \
       /app/cmd/indexer

FROM alpine:latest

COPY --from=build /go/bin/indexer /usr/local/bin/indexer
