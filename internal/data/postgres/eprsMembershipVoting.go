package postgres

import (
	"database/sql"
	"time"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

// EprsMembershipVotingStorage implements data.EprsMembershipVotingStorage.
type EprsMembershipVotingStorage struct {
	db *pgdb.DB
}

func (s *EprsMembershipVotingStorage) Insert(votings ...data.EprsMembershipVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert("eprs_membership_votings").Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_quorum",
		"required_quorum",
		"root_nodes_number",
		"vetos_number",
		"voting_end_time",
		"veto_end_time",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentQuorum,
			voting.RequiredQuorum,
			voting.RootNodesNumber,
			voting.VetosNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
			time.Unix(int64(voting.VetoEndTime), 0),
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_quorum=EXCLUDED.current_quorum," +
		"root_nodes_number=EXCLUDED.root_nodes_number," +
		"vetos_number=EXCLUDED.vetos_number",
	)

	return errors.Wrap(s.db.Exec(query), "failed to insert eprs membership voting")
}

// GetLatest eprs membership voting.
func (s *EprsMembershipVotingStorage) GetLatest() (*data.EprsMembershipVoting, error) {
	return s.queryBlock(squirrel.Select(commonVotingColumns...).From("eprs_membership_votings").
		Where("proposal_id = (SELECT max(proposal_id) from eprs_membership_votings)"))
}

func (s *EprsMembershipVotingStorage) GetEarliestUnfinishedVotings() ([]data.EprsMembershipVoting, error) {
	var votings []data.EprsMembershipVoting
	query := squirrel.Select(commonVotingColumns...).From("eprs_membership_votings").
		Where("voting_status IN (1, 3, 4)").OrderBy("proposal_id ASC")
	if err := s.db.Select(&votings, query); err != nil {
		return nil, errors.Wrap(err, "failed to select eprs membership votings")
	}
	return votings, nil
}

func (s *EprsMembershipVotingStorage) Update(voting *data.EprsMembershipVoting) error {
	query := squirrel.Update("eprs_membership_votings").SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_quorum":    voting.CurrentQuorum,
		"root_nodes_number": voting.RootNodesNumber,
		"vetos_number":      voting.VetosNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query), "failed to update eprs membership voting")
}

func (s *EprsMembershipVotingStorage) queryBlock(query squirrel.SelectBuilder) (*data.EprsMembershipVoting, error) {
	var voting data.EprsMembershipVoting
	switch err := s.db.Get(&voting, query); err {
	case nil:
		return &voting, nil
	case sql.ErrNoRows:
		return nil, nil
	default:
		return nil, errors.Wrap(err, "failed to get eprs membership voting")
	}
}
