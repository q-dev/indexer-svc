# Changelog

All notable changes to this project will be documented in this file.

## [1.0.10]
### Added
- System contracts implementations
- Layer zero lists

### Fixed
- Sending old notifications
- Panic after notification send failure

## [1.0.0] - 2023-04
### Changed
- Metrics are now calculated from blocks database, removed metrics database

### Added
- Version file and version api
- API for votings
- Firestore database for notifications
- New config setting to ignore notifications up to some block

### Fixed
- optimized sync of data from q-client by moving firebase notifications and votings to separate go-routines
