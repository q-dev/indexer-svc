package data

type SummaryQuery struct {
	MainAccount          string  `db:"main_account"`
	Signer               string  `db:"signer"`
	TotalDueBlocks       uint64  `db:"total_due_blocks"`
	TotalInTurnBlocks    uint64  `db:"total_inturn_blocks"`
	TotalOutOfTurnBlocks uint64  `db:"total_outofturn_blocks"`
	Availability         float64 `db:"availability"`
}

// Block of summary of metrics.
type SummaryMetrics struct {
	Number               uint64                    `db:"id"`
	MainAccount          string                    `db:"mainaccount"`
	TotalDueBlocks       uint64                    `db:"totaldueblocks"`
	TotalInTurnBlocks    uint64                    `db:"totalinturnblocks"`
	TotalOutOfTurnBlocks uint64                    `db:"totaloutofturnblocks"`
	MetricsByMiner       map[string]MetricsByMiner `db:"metricsbyminer"`
}

type MetricsByMiner struct {
	DueBlocks       uint64 `db:"dueblocks"`
	InTurnBlocks    uint64 `db:"inturnblocks"`
	OutOfTurnBlocks uint64 `db:"outofturnblocks"`
}

// SummaryMetricsStorage stores blocks.
type SummaryMetricsStorage interface {
	Select(params *SelectSummaryMetricsParams) ([]SummaryMetrics, error)
}

type SelectSummaryMetricsParams struct {
	IsDesc bool
	Cursor *uint64
	Limit  uint64

	Cycles      uint64
	Signer      *string
	MainAccount *string
}
