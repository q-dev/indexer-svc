package data

import (
	"database/sql/driver"
	"math/big"

	"gitlab.com/distributed_lab/logan/v3/errors"
)

// BigInt is a wrapper for saving to db.
type BigInt big.Int

// Unwrap returns underlying big.Int
func (b *BigInt) Unwrap() *big.Int {
	if b == nil {
		return nil
	}

	return (*big.Int)(b)
}

// NewBigInt.
func NewBigInt(b *big.Int) *BigInt {
	return (*BigInt)(b)
}

// Value implements driver.Valuer.
func (b BigInt) Value() (driver.Value, error) {
	return []byte(b.Unwrap().String()), nil
}

// Scan implements sql.Scanner interface.
func (b *BigInt) Scan(src interface{}) error {
	switch t := src.(type) {
	case []byte:
		z, ok := big.NewInt(0).SetString(string(t), 10)
		if !ok {
			return errors.Errorf("invalid big.Int %s", string(t))
		}

		*b = BigInt(*z)
	case string:
		z, ok := big.NewInt(0).SetString(t, 10)
		if !ok {
			return errors.Errorf("invalid big.Int %s", t)
		}

		*b = BigInt(*z)
	default:
		return errors.Errorf("unexpected input type %T", src)
	}

	return nil
}
