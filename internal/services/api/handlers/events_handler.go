package handlers

import (
	"context"
	"math/big"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/q-dev/q-client/accounts/abi/bind"
	"gitlab.com/q-dev/q-client/contracts"
	"gitlab.com/q-dev/q-client/ethclient"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/helpers"
	"gitlab.com/q-dev/indexer-svc/internal/services/api/requests"
)

type EventHandler struct {
	log       *logan.Entry
	registry  *contracts.Registry
	ethclient *ethclient.Client
	rootNodes data.RootsMembershipStorage
}

func NewEventHandler(log *logan.Entry, ethclient *ethclient.Client,
	registry *contracts.Registry, storage data.RootsMembershipStorage,
) EventHandler {
	return EventHandler{
		log:       log,
		registry:  registry,
		ethclient: ethclient,
		rootNodes: storage,
	}
}

type selectEventsFunc func(params data.SelectVotingEventsParams, addresses []string) ([]data.VotingEvent, error)
type selectAggregatedEventsFunc func(params data.SelectVotingEventsParams) (data.CountVotingEvents, error)

func (h *EventHandler) selectEvents(fn selectEventsFunc, request requests.GetVotingEventsRequest, opts *bind.CallOpts) ([]data.VotingEvent, error) {
	rootNodes, err := h.registry.Roots().GetMembers(opts)
	if err != nil {
		return nil, err
	}
	var stringAddresses []string
	for _, rootNode := range rootNodes {
		stringAddresses = append(stringAddresses, strings.ToLower(rootNode.String()))
	}

	events, err := fn(data.SelectVotingEventsParams{
		IsDesc:         request.Sort == "desc",
		Cursor:         request.Cursor,
		Limit:          request.Limit,
		StartBlock:     request.StartBlock,
		AccountAddress: strings.ToLower(request.AccountAddress),
	}, stringAddresses)
	if err != nil {
		return nil, errors.Wrap(err, "failed to query events from DB")
	}
	return events, nil
}

func (h *EventHandler) selectAggregatedEvents(fn selectAggregatedEventsFunc, request requests.GetVotingEventsRequest) (data.CountVotingEvents, error) {
	header, err := h.ethclient.HeaderByNumber(context.TODO(), big.NewInt(int64(request.StartBlock)))
	if err != nil {
		return data.CountVotingEvents{}, err
	}
	time := uint64(0)
	if request.StartBlock != 0 {
		time = header.Time
	}

	activeRootNodes, err := h.rootNodes.SelectActiveRootAddresses()
	if err != nil {
		return data.CountVotingEvents{}, errors.Wrap(err, "failed to select active root addresses")
	}

	h.log.WithField("addresses", activeRootNodes).Debug("Successfully retrieved active root nodes addresses")

	events, err := fn(data.SelectVotingEventsParams{
		IsDesc:              request.Sort == "desc",
		Cursor:              request.Cursor,
		Limit:               request.Limit,
		StartBlock:          request.StartBlock,
		AccountAddress:      strings.ToLower(request.AccountAddress),
		StartBlockTimestamp: time,
		AddressList:         activeRootNodes, // already lowercase
	})
	if err != nil {
		return data.CountVotingEvents{}, errors.Wrap(err, "failed to query aggregated events from DB")
	}
	return events, nil
}

func (h *EventHandler) getAggregatedEvents(fn selectAggregatedEventsFunc, w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetVotingEventsRequest(r)
	if err != nil {
		ape.Render(w, problems.BadRequest(err))
		return
	}

	events, err := h.selectAggregatedEvents(fn, request)
	if err != nil {
		ape.Render(w, problems.InternalError())
		h.log.WithError(err).Error("failed to query the DB")
		return
	}

	resp := resources.PopulateAggregatedEventsList(events)
	resp.Links, err = helpers.GetAggregatedVotingEventsList(events, request, *r.URL)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to build links")
		return
	}

	ape.Render(w, resp)
}

func (h *EventHandler) getEvents(fn selectEventsFunc, w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetVotingEventsRequest(r)
	if err != nil {
		ape.Render(w, problems.BadRequest(err))
		return
	}

	aggregatedEvents, err := h.selectEvents(fn, request, nil)
	if err != nil {
		ape.Render(w, problems.InternalError())
		h.log.WithError(err).Error("failed to query the DB")
		return
	}

	resp := resources.PopulateEventsList(aggregatedEvents)
	resp.Links, err = helpers.GetVotingEventsLinks(aggregatedEvents, request, *r.URL)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		h.log.WithError(err).Error("failed to build links")
		return
	}

	ape.Render(w, resp)
}
