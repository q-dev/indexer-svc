-- +migrate Up
CREATE TABLE variables
(
    name   TEXT     NOT NULL PRIMARY KEY,
    value  BIGINT   NOT NULL
);

-- +migrate Down
DROP TABLE variables;
