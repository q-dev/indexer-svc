package data

// EprsMembershipVoting voting.
type EprsMembershipVoting BaseVoting

// EprsMembershipVotingStorage stores eprs membership votings.
type EprsMembershipVotingStorage interface {
	Insert(votings ...EprsMembershipVoting) error
	GetLatest() (*EprsMembershipVoting, error)

	GetEarliestUnfinishedVotings() ([]EprsMembershipVoting, error)
	Update(voting *EprsMembershipVoting) error
}
