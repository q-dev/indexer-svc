-- +migrate Up
ALTER TABLE node_account_pairs ADD COLUMN active BOOLEAN NOT NULL DEFAULT true;

-- +migrate Down
ALTER TABLE node_account_pairs DROP COLUMN active;
