package postgres

import (
	"fmt"
	"strings"

	"gitlab.com/distributed_lab/logan/v3"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

var (
	sortedQueryColumns = []string{
		"proposal_id",
		"voting_type",
		"voting_end_time",
		"voting_status",
	}
	tableNames = []string{
		"constitution_votings", "general_update_votings", "emergency_update_votings",
		"roots_votings", "rootnodes_slashing_votings", "validators_slashing_votings",
		"epqfi_membership_votings", "epqfi_parameters_votings", "epdr_membership_votings", "epdr_parameters_votings",
		"eprs_membership_votings", "eprs_parameters_votings",
		ContractRegistryAddressVotingTable,
		ContractRegistryUpgradeVotingTable,
		ContractRegistryVotingTable,
	}
)

// VotingStorage implements all votings from package data.
type VotingStorage struct {
	db *pgdb.DB
}

type AllVotings struct {
	Cvotings      []data.GeneralVoting
	GUvotings     []data.GeneralVoting
	EUvotings     []data.GeneralVoting
	Rvotings      []data.GeneralVoting
	RSvotings     []data.GeneralVoting
	VSvotings     []data.GeneralVoting
	EPQFIMvotings []data.GeneralVoting
	EPQFIPvotings []data.GeneralVoting
	EPDRMvotings  []data.GeneralVoting
	EPDRPvotings  []data.GeneralVoting
	EPRSMvotings  []data.GeneralVoting
	EPRSPvotings  []data.GeneralVoting
	CRAvotings    []data.GeneralVoting
	CRUvotings    []data.GeneralVoting
	CRvotings     []data.GeneralVoting
}

type crSelectVar struct {
	votings   *[]data.GeneralVoting
	tableName string
}

// Select blocks with params.
func (s *VotingStorage) Select(
	params *data.SelectVotingsParams,
	currentRootNodesCount uint64,
) ([]data.GeneralVoting, error) {
	var votings AllVotings
	votingsByType := make(map[string][]data.GeneralVoting)

	rootNodesCountSql := fmt.Sprintf(
		"COALESCE(NULLIF(root_nodes_number, 0), %d) AS root_nodes_number",
		currentRootNodesCount,
	)
	columns := append(
		cVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.Cvotings, squirrel.Select(columns...).
		From("constitution_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select constitution votings")
	}
	if votings.Cvotings != nil {
		votingsByType[votings.Cvotings[0].VotingType] = votings.Cvotings
	}

	columns = append(
		commonVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.GUvotings, squirrel.Select(columns...).
		From("general_update_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select general update votings")
	}
	if votings.GUvotings != nil {
		votingsByType[votings.GUvotings[0].VotingType] = votings.GUvotings
	}

	columns = append(
		eUVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.EUvotings, squirrel.Select(columns...).
		From("emergency_update_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select emergency update votings")
	}
	if votings.EUvotings != nil {
		votingsByType[votings.EUvotings[0].VotingType] = votings.EUvotings
	}

	columns = append(
		rMVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.Rvotings, squirrel.Select(columns...).
		From("roots_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select roots votings")
	}
	if votings.Rvotings != nil {
		votingsByType[votings.Rvotings[0].VotingType] = votings.Rvotings
	}

	columns = append(
		rSVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.RSvotings, squirrel.Select(columns...).
		From("rootnodes_slashing_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select rootnodes slashing votings")
	}
	if votings.RSvotings != nil {
		votingsByType[votings.RSvotings[0].VotingType] = votings.RSvotings
	}

	columns = append(
		vSVotingColumns,
		rootNodesCountSql,
	)

	if err := s.db.Select(&votings.VSvotings, squirrel.Select(columns...).
		From("validators_slashing_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select validators slashing votings")
	}
	if votings.VSvotings != nil {
		votingsByType[votings.VSvotings[0].VotingType] = votings.VSvotings
	}

	columns = append(
		commonVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.EPQFIMvotings, squirrel.Select(columns...).
		From("epqfi_membership_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select epqfi membership votings")
	}
	if votings.EPQFIMvotings != nil {
		votingsByType[votings.EPQFIMvotings[0].VotingType] = votings.EPQFIMvotings
	}

	columns = append(
		commonVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.EPQFIPvotings, squirrel.Select(columns...).
		From("epqfi_parameters_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select epqfi parameters votings")
	}
	if votings.EPQFIPvotings != nil {
		votingsByType[votings.EPQFIPvotings[0].VotingType] = votings.EPQFIPvotings
	}

	columns = append(
		commonVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.EPDRMvotings, squirrel.Select(columns...).
		From("epdr_membership_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select epdr membership otings")
	}
	if votings.EPDRMvotings != nil {
		votingsByType[votings.EPDRMvotings[0].VotingType] = votings.EPDRMvotings
	}

	columns = append(
		commonVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.EPDRPvotings, squirrel.Select(columns...).
		From("epdr_parameters_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select epdr parameters votings")
	}
	if votings.EPDRPvotings != nil {
		votingsByType[votings.EPDRPvotings[0].VotingType] = votings.EPDRPvotings
	}

	columns = append(
		commonVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.EPRSMvotings, squirrel.Select(columns...).
		From("eprs_membership_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select eprs membership votings")
	}
	if votings.EPRSMvotings != nil {
		votingsByType[votings.EPRSMvotings[0].VotingType] = votings.EPRSMvotings
	}

	columns = append(
		commonVotingColumns,
		rootNodesCountSql,
	)
	if err := s.db.Select(&votings.EPRSPvotings, squirrel.Select(columns...).
		From("eprs_parameters_votings").OrderBy("proposal_id ASC")); err != nil {
		return nil, errors.Wrap(err, "failed to select eprs parameters votings")
	}
	if votings.EPRSPvotings != nil {
		votingsByType[votings.EPRSPvotings[0].VotingType] = votings.EPRSPvotings
	}

	crVotings := []crSelectVar{
		{&votings.CRAvotings, ContractRegistryAddressVotingTable},
		{&votings.CRUvotings, ContractRegistryUpgradeVotingTable},
		{&votings.CRvotings, ContractRegistryVotingTable},
	}

	columns = append(
		cRVotingColumns,
		rootNodesCountSql,
	)
	for i := range crVotings {
		err := s.db.Select(crVotings[i].votings,
			squirrel.Select(columns...).
				From(crVotings[i].tableName).
				OrderBy("proposal_id ASC"))
		if err != nil {
			return nil, errors.Wrap(err, "failed to select contract registry votings", logan.F{
				"table_name": crVotings[i].tableName,
			})
		}
		if len(*crVotings[i].votings) > 0 {
			votingsByType[(*crVotings[i].votings)[0].VotingType] = *crVotings[i].votings
		}
	}

	queries := make([]squirrel.SelectBuilder, len(tableNames))

	for i := range queries {
		queries[i] = squirrel.Select(sortedQueryColumns...).From(tableNames[i])
	}

	if params.Finished != nil {
		if !*params.Finished {
			for i := 0; i < len(queries); i++ {
				queries[i] = queries[i].Where("(voting_status IN (1,3,4))")
			}
		} else {
			for i := 0; i < len(queries); i++ {
				queries[i] = queries[i].Where("(voting_status IN (0,2,5,6,7))")
			}
		}
	}

	querySorted := queries[0]
	for i := 1; i < len(queries); i++ {
		err := s.Union(&querySorted, queries[i])
		if err != nil {
			return nil, err
		}
	}

	querySorted = squirrel.Select("proposal_id,voting_type, "+
		"cast(extract(epoch from voting_end_time) as integer) as voting_end_time, "+
		"voting_status").FromSelect(querySorted, "allvotings")

	if params.Status != nil {
		querySorted = querySorted.Where("(voting_status IN (" + strings.Join(params.Status, ",") + "))")
	}

	if len(params.Type) > 2 {
		querySorted = querySorted.Where("(voting_type IN (" + params.Type + "))")
	}

	order := "ASC"
	if params.IsDesc {
		order = "DESC"
	}
	querySorted = querySorted.OrderBy(fmt.Sprintf("voting_end_time %s", order)).
		OrderBy(fmt.Sprintf("proposal_id %s", order)).Limit(params.Limit)

	if params.Cursor != nil {
		querySorted = querySorted.Offset(*params.Cursor)
	}

	var votingsSorted []data.Voting
	if err := s.db.Select(&votingsSorted, querySorted); err != nil {
		return nil, errors.Wrap(err, "failed to select votings")
	}

	var votingsMerged []data.GeneralVoting
	for _, votingS := range votingsSorted {
		proposal := votingsByType[votingS.VotingType][int(votingS.ProposalId)]
		votingsMerged = append(votingsMerged, proposal)
	}

	return votingsMerged, nil
}

func (s *VotingStorage) Union(mainQuery *squirrel.SelectBuilder, query squirrel.SelectBuilder) error {
	sql, args, err := query.ToSql()
	if err != nil {
		return err
	}
	*mainQuery = mainQuery.Suffix("UNION "+sql, args...)
	return nil
}
