package api

import (
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3"

	"gitlab.com/q-dev/indexer-svc/internal/data"
	"gitlab.com/q-dev/indexer-svc/internal/resources"
)

type delegationsHandler struct {
	log     *logan.Entry
	storage data.Storage
}

// @Summary   	Get delegator count
// @Description Returns current number of delegators.
// @ID       	delegator-count
// @Produce    	json
// @Success   	200 {object} resources.DelegatorCountData
// @Failure   	500 "Internal Server Error"
// @Failure   	default "Page not found"
// @Router     	/delegator-count [get]
func (h *delegationsHandler) getDelegatorCount(w http.ResponseWriter, r *http.Request) {
	res, err := h.storage.Delegations().CountDelegators()
	if err != nil {
		h.log.WithError(err).Error("failed to query delegator count")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	ape.Render(w, resources.NewDelegatorCount(res))
}
