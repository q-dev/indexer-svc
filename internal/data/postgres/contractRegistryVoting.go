package postgres

import (
	"database/sql"
	"time"

	"gitlab.com/distributed_lab/logan/v3"

	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

const (
	ContractRegistryVotingTable        = "contract_registry_votings"
	ContractRegistryAddressVotingTable = "contract_registry_address_votings"
	ContractRegistryUpgradeVotingTable = "contract_registry_upgrade_votings"
)

var (
	cRVotingColumns = []string{
		"proposal_id",
		"voting_type",
		"voting_status",
		"current_majority",
		"required_majority",
		"votes_number",
		"cast(extract(epoch from voting_end_time) as integer) as voting_end_time",
		"cast(extract(epoch from voting_start_time) as integer) as voting_start_time",
	}
)

// ContractRegistryVotingStorage implements data.ContractRegistryVotingStorage.
type ContractRegistryVotingStorage struct {
	db        *pgdb.DB
	tableName string
}

func (s *ContractRegistryVotingStorage) Insert(votings ...data.ContractRegistryVoting) error {
	if len(votings) == 0 {
		return nil
	}

	query := squirrel.Insert(s.tableName).Columns(
		"proposal_id",
		"voting_type",
		"voting_status",
		"voting_start_time",
		"current_majority",
		"required_majority",
		"root_nodes_number",
		"votes_number",
		"voting_end_time",
	)

	for _, voting := range votings {
		query = query.Values(
			voting.ProposalId,
			voting.VotingType,
			voting.VotingStatus,
			time.Unix(int64(voting.VotingStartTime), 0),
			voting.CurrentMajority,
			voting.RequiredMajority,
			voting.RootNodesNumber,
			voting.VotesNumber,
			time.Unix(int64(voting.VotingEndTime), 0),
		)
	}

	query = query.Suffix("ON CONFLICT (proposal_id) DO UPDATE SET " +
		"voting_status=EXCLUDED.voting_status," +
		"current_majority=EXCLUDED.current_majority," +
		"root_nodes_number=EXCLUDED.root_nodes_number," +
		"votes_number=EXCLUDED.votes_number",
	)

	return errors.Wrap(s.db.Exec(query),
		"failed to insert contract registry voting",
		logan.F{
			"table_name": s.tableName,
		},
	)
}

// GetLatest block.
func (s *ContractRegistryVotingStorage) GetLatestId() (*int64, error) {
	var maxProposalId *int64
	err := s.db.Get(&maxProposalId, squirrel.Select("max(proposal_id)").From(s.tableName))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, errors.Wrap(err, "failed to get contract registry voting latest proposal id", logan.F{
			"table_name": s.tableName,
		})
	}

	return maxProposalId, nil
}

func (s *ContractRegistryVotingStorage) GetEarliestUnfinishedVotings() ([]data.ContractRegistryVoting, error) {
	var votings []data.ContractRegistryVoting
	query := squirrel.Select(cRVotingColumns...).
		From(s.tableName).
		Where(squirrel.Eq{"voting_status": 1}). // 1 means pending
		OrderBy("proposal_id ASC")

	err := s.db.Select(&votings, query)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}

		return nil, errors.Wrap(err, "failed to select pending contract registry votings", logan.F{
			"table_name": s.tableName,
		})
	}
	return votings, nil
}

func (s *ContractRegistryVotingStorage) Update(voting *data.ContractRegistryVoting) error {
	query := squirrel.Update(s.tableName).SetMap(map[string]interface{}{
		"voting_status":     voting.VotingStatus,
		"current_majority":  voting.CurrentMajority,
		"root_nodes_number": voting.RootNodesNumber,
		"votes_number":      voting.VotesNumber,
	}).Where("proposal_id = ?", voting.ProposalId)

	return errors.Wrap(s.db.Exec(query),
		"failed to update contract registry voting",
		logan.F{
			"table_name": s.tableName,
		},
	)
}
