package config

import (
	"context"
	"encoding/base64"
	"math/big"
	"net/url"
	"reflect"
	"time"

	"cloud.google.com/go/firestore"
	fbase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/messaging"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/q-dev/q-client/common"
	"gitlab.com/q-dev/q-client/ethclient"
	"gitlab.com/q-dev/q-client/params"
	"google.golang.org/api/option"

	"gitlab.com/q-dev/indexer-svc/internal/services/firebase"
)

// Config of the service.
type Config interface {
	comfig.Logger
	comfig.Listenerer
	pgdb.Databaser
	Client() *ethclient.Client
	Notifier(ctx context.Context) *firebase.Notifier
	IgnoreNotificationsCfg() int64
	LayerZeroLists() LayerZeroListsCfg
	ContractsRegistry() ContractsRegistry
}

// New Config.
func New(getter kv.Getter) Config {
	return &config{
		getter:     getter,
		Logger:     comfig.NewLogger(getter, comfig.LoggerOpts{}),
		Listenerer: comfig.NewListenerer(getter),
		Databaser:  pgdb.NewDatabaser(getter),
	}
}

type config struct {
	getter kv.Getter

	comfig.Logger
	comfig.Listenerer
	pgdb.Databaser

	qclient               comfig.Once
	ignoreNfConf          comfig.Once
	firebaseMessagingConf comfig.Once
	firestoreConf         comfig.Once
	firebaseApp           comfig.Once
	firestoreApp          comfig.Once
	firestore             comfig.Once
	messaging             comfig.Once
	whitelistApprovals    comfig.Once
	rpc                   comfig.Once
	ws                    comfig.Once
	layerZeroLists        comfig.Once
	contractsRegistry     comfig.Once

	notifier comfig.Once
	wsServer comfig.Once
}

type WhitelistApprovalsCfg struct {
	CachingDelay int64 `fig:"caching_delay"`
}

type firebaseCfg struct {
	ServiceAccount string `fig:"service_account,required"`
	Disabled       bool   `fig:"disabled,required"`
}

type ignoreNotificationsCfg struct {
	Block int64 `fig:"block,required"`
}

type qclient struct {
	RPC *url.URL `fig:"http.url,required"`
	WS  *url.URL `fig:"ws.url"`
}

type LayerZeroListsCfg struct {
	CachePeriod time.Duration `fig:"cache_period"`
}

type ContractsRegistry struct {
	Registry       *common.Address `fig:"registry"`
	RewardReceiver *common.Address `fig:"reward_receiver"`
}

func (cfg *config) firebaseMessagingCfg() firebaseCfg {
	return cfg.firebaseMessagingConf.Do(func() interface{} {
		var firebaseConfig firebaseCfg
		if err := figure.Out(&firebaseConfig).From(kv.MustGetStringMap(cfg.getter, "firebase-messaging")).Please(); err != nil {
			panic(errors.Wrap(err, "invalid firebase config"))
		}

		return firebaseConfig
	}).(firebaseCfg)
}

func (cfg *config) firestoreCfg() firebaseCfg {
	return cfg.firestoreConf.Do(func() interface{} {
		var firestoreConfig firebaseCfg
		if err := figure.Out(&firestoreConfig).From(kv.MustGetStringMap(cfg.getter, "firestore")).Please(); err != nil {
			cfg.Log().Warn("separate firestore not specified, using firestore from firebase messaging app")
			return cfg.firebaseMessagingCfg()
		}

		return firestoreConfig
	}).(firebaseCfg)
}

func (cfg *config) qclientCfg() qclient {
	return cfg.qclient.Do(func() interface{} {
		var qclient qclient
		if err := figure.Out(&qclient).From(kv.MustGetStringMap(cfg.getter, "qclient")).Please(); err != nil {
			panic(errors.Wrap(err, "invalid qclient config"))
		}

		return qclient
	}).(qclient)
}

func (cfg *config) IgnoreNotificationsCfg() int64 {
	return cfg.ignoreNfConf.Do(func() interface{} {
		var block ignoreNotificationsCfg
		if err := figure.Out(&block).From(kv.MustGetStringMap(cfg.getter, "ignoreNotificationsUpTo")).Please(); err != nil {
			panic(errors.Wrap(err, "invalid notification config"))
		}

		return block.Block
	}).(int64)
}

func (cfg *config) firebaseMessagingApp() *fbase.App {
	if cfg.firebaseMessagingCfg().Disabled {
		return nil
	}
	return cfg.firebaseApp.Do(func() interface{} {

		serviceAccountString := cfg.firebaseMessagingCfg().ServiceAccount
		serviceAccount, err := base64.StdEncoding.DecodeString(serviceAccountString)
		if err != nil {
			panic(errors.Wrap(err, "invalid firebase service account config"))
		}

		app, err := fbase.NewApp(context.Background(), nil, option.WithCredentialsJSON(serviceAccount))
		if err != nil {
			panic(errors.Wrap(err, "invalid firebase service account config"))
		}

		return app
	}).(*fbase.App)
}

func (cfg *config) firebaseFirestore() *fbase.App {
	if cfg.firebaseMessagingCfg().Disabled {
		return nil
	}
	return cfg.firestoreApp.Do(func() interface{} {
		serviceAccountString := cfg.firestoreCfg().ServiceAccount

		if serviceAccountString == "" {
			serviceAccountString = cfg.firebaseMessagingCfg().ServiceAccount
		}

		serviceAccount, err := base64.StdEncoding.DecodeString(serviceAccountString)
		if err != nil {
			panic(errors.Wrap(err, "invalid firebase service account config"))
		}

		app, err := fbase.NewApp(context.Background(), nil, option.WithCredentialsJSON(serviceAccount))
		if err != nil {
			panic(errors.Wrap(err, "invalid firebase service account config"))
		}

		return app
	}).(*fbase.App)
}

func (cfg *config) firestoreClient() *firestore.Client {
	firebaseFirestore := cfg.firebaseFirestore()
	if firebaseFirestore == nil {
		return nil
	}
	return cfg.firestore.Do(func() interface{} {

		firestoreClient, err := firebaseFirestore.Firestore(context.Background())
		if err != nil {
			panic(errors.Wrap(err, "unable to set up firestore client"))
		}

		return firestoreClient
	}).(*firestore.Client)
}

func (cfg *config) messagingClient() *messaging.Client {
	messagingApp := cfg.firebaseMessagingApp()
	if messagingApp == nil {
		return nil
	}
	return cfg.messaging.Do(func() interface{} {

		messagingClient, err := messagingApp.Messaging(context.Background())
		if err != nil {
			panic(errors.Wrap(err, "unable to set up messaging client"))
		}

		return messagingClient
	}).(*messaging.Client)
}

func (cfg *config) WhitelistApprovals() WhitelistApprovalsCfg {
	return cfg.whitelistApprovals.Do(func() interface{} {
		var whitelistApprovalsCfg WhitelistApprovalsCfg
		if err := figure.Out(&whitelistApprovalsCfg).From(kv.MustGetStringMap(cfg.getter, "whitelist_approvals")).Please(); err != nil {
			panic(errors.Wrap(err, "invalid notification config"))
		}

		return whitelistApprovalsCfg
	}).(WhitelistApprovalsCfg)
}

func (cfg *config) Client() *ethclient.Client {
	return cfg.rpc.Do(func() interface{} {
		client, err := ethclient.Dial(cfg.qclientCfg().WS.String())
		if err != nil {
			panic(errors.Wrap(err, "failed to init client of q node"))
		}

		return client
	}).(*ethclient.Client)
}

func (cfg *config) Notifier(ctx context.Context) *firebase.Notifier {
	chainId, err := cfg.Client().ChainID(ctx)
	if err != nil {
		panic(errors.Wrap(err, "unable to get chain id"))
	}
	return cfg.notifier.Do(func() interface{} {
		messagingClient := cfg.messagingClient()
		firestoreClient := cfg.firestoreClient()

		if messagingClient == nil || firestoreClient == nil {
			return firebase.NewDisabledNotifier(cfg.Log(), chainId.Int64())
		}
		return firebase.NewNotifier(cfg.Log(), firestoreClient, messagingClient, chainId.Int64())
	}).(*firebase.Notifier)
}

func (cfg *config) LayerZeroLists() LayerZeroListsCfg {
	return cfg.layerZeroLists.Do(func() interface{} {
		var layerZeroCfg LayerZeroListsCfg
		if err := figure.Out(&layerZeroCfg).From(kv.MustGetStringMap(cfg.getter, "layer_zero_lists")).Please(); err != nil {
			panic(errors.Wrap(err, "invalid layer zero lists config"))
		}

		return layerZeroCfg
	}).(LayerZeroListsCfg)
}

func (cfg *config) ContractsRegistry() ContractsRegistry {
	return cfg.contractsRegistry.Do(func() interface{} {
		var contractsRegistry ContractsRegistry
		_ = figure.
			Out(&contractsRegistry).
			From(kv.MustGetStringMap(cfg.getter, "contracts_registry")).
			With(figure.Hooks{
				"*common.Address": func(value interface{}) (reflect.Value, error) {
					switch v := value.(type) {
					case string:
						if !common.IsHexAddress(v) {
							// provide value does not look like valid address
							return reflect.Value{}, errors.New("invalid address")
						}
						addr := common.HexToAddress(v)
						return reflect.ValueOf(&addr), nil
					default:
						return reflect.Value{}, errors.Errorf("unsupported conversion from %T", value)
					}
				},
			}).
			Please()
		if contractsRegistry.Registry != nil && contractsRegistry.RewardReceiver != nil {
			return contractsRegistry
		}

		defaultIfNil := func(defaultValue, currentValue *common.Address) *common.Address {
			if currentValue == nil {
				return defaultValue
			}

			return currentValue
		}

		chainId, err := cfg.Client().ChainID(context.TODO())
		if err != nil {
			panic(errors.Wrap(err, "failed to get chain id from q-client"))
		}
		contractsRegistry.Registry = defaultIfNil(getDefaultRegistryAddress(chainId), contractsRegistry.Registry)
		contractsRegistry.RewardReceiver = defaultIfNil(getDefaultRewardReceiver(chainId), contractsRegistry.RewardReceiver)

		return contractsRegistry
	}).(ContractsRegistry)
}

func getDefaultRegistryAddress(chainId *big.Int) *common.Address {
	devnetRegistryAddress := common.HexToAddress("0xc3E589056Ece16BCB88c6f9318e9a7343b663522")

	return getDefaultAddress(
		chainId,
		&params.MainnetChainConfig.Clique.Registry,
		&params.TestnetChainConfig.Clique.Registry,
		&devnetRegistryAddress,
	)
}

func getDefaultRewardReceiver(chainId *big.Int) *common.Address {
	devnetRewardReceiver := common.HexToAddress("0xc4D32b94f039991703b869AA8AcB1A354c32AFd1")

	return getDefaultAddress(
		chainId,
		&params.MainnetChainConfig.Clique.RewardReceiver,
		&params.TestnetChainConfig.Clique.RewardReceiver,
		&devnetRewardReceiver,
	)
}

func getDefaultAddress(chainID *big.Int, mainnet, testnet, devnet *common.Address) *common.Address {
	switch chainID.Uint64() {
	case params.MainnetChainConfig.ChainID.Uint64():
		return mainnet
	case params.TestnetChainConfig.ChainID.Uint64():
		return testnet
	default:
		return devnet
	}
}
