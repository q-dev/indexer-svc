package data

// RootNodeApproval .
type RootNodeApproval struct {
	Id        uint64  `db:"id"`
	Signature []byte  `db:"signature"`
	Block     uint64  `db:"block"`
	Signer    string  `db:"signer"`
	Alias     *string `db:"alias"`
}

type RootNodeBreakdown struct {
	FirstTransitionBlock uint64
	LastTransitionBlock  uint64
	ByAddress            []ByAddress
}

type ByAddress struct {
	Account           string
	ObservedApprovals []ObservedApprovals
}

type ObservedApprovals struct {
	Alias                 *string
	FirstObservedApproval ObservedApproval
	LastObservedApproval  ObservedApproval
}

type ObservedApproval struct {
	Block  uint64
	Cycles uint64
}

type FirstAndLastBlocksByAccount struct {
	First     uint64  `db:"first"`
	Last      uint64  `db:"last"`
	Account   string  `db:"signer"`
	Alias     *string `db:"alias"`
	Approvals uint64  `db:"approvals"`
}

// RootNodeApprovalsStorage stores root node approvals.
type RootNodeApprovalsStorage interface {
	Insert(approvals ...RootNodeApproval) error
	GetLatest() (*RootNodeApproval, error)

	Select(params SelectRootNodeApprovalParams, highestBlock uint64) (RootNodeBreakdown, error)
}

type SelectRootNodeApprovalParams struct {
	IsDesc bool
	Cursor *string
	Limit  uint64

	Cycles      uint64
	RootAddress *string
	BlocksDelay uint64
}
