package resources

import (
	"bytes"
	"encoding/json"
	"math/big"
	"testing"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

func TestPopulatePaymentResponse(t *testing.T) {
	payments := []data.Payment{
		{
			TxHash:    "0xc7407a5a50bfa95cc28ab410885ccd3e4bea2b94de60c48c0aa3d8c6750221e3",
			Timestamp: 1618517598,
			Sender:    "0x4711b1dff063609b4a87f3d092059a9e12eab4f9",
			Recipient: "0x228c8108bd0368bbb6a47cc3ad89934c82952cc2",
			Token:     newString("0x228c8108bd0368bbb6a47cc3ad89934c82952cc2"),
			Value:     data.NewBigInt(big.NewInt(123000000000001)),
			Tx: &data.Transaction{
				Hash:      "0xc7407a5a50bfa95cc28ab410885ccd3e4bea2b94de60c48c0aa3d8c6750221e3",
				Timestamp: 1618517598,
				Sender:    "0x4711b1dff063609b4a87f3d092059a9e12eab4f9",
				Recipient: newString("0x228c8108bd0368bbb6a47cc3ad89934c82952cc2"),
				Value:     data.NewBigInt(big.NewInt(123000000000001)),
				Gas:       100500,
				GasPrice:  data.NewBigInt(big.NewInt(1000022)),
				Nonce:     13,
				Input:     []byte{1, 2, 3, 100, 15},
				Block:     10,
			},
		},
		{
			TxHash:    "0x3f8f01e333bf94e7470bb3db7fa7b6a188e9477e53799f2391d227da4b4e3306",
			Timestamp: 1618517853,
			Sender:    "0x91e894cd2fd19be4fb661f225a96b96293a575e2",
			Recipient: "0x228c8108bd0368bbb6a47cc3ad89934c82952cc2",
			Token:     newString("0x228c8108bd0368bbb6a47cc3ad89934c82952cc2"),
			Value:     data.NewBigInt(big.NewInt(123000000000001)),
			Tx: &data.Transaction{
				Hash:      "0x3f8f01e333bf94e7470bb3db7fa7b6a188e9477e53799f2391d227da4b4e3306",
				Timestamp: 1618517853,
				Sender:    "0x91e894cd2fd19be4fb661f225a96b96293a575e2",
				Recipient: newString("0x228c8108bd0368bbb6a47cc3ad89934c82952cc2"),
				Value:     data.NewBigInt(big.NewInt(23000000000001)),
				Gas:       300500,
				GasPrice:  data.NewBigInt(big.NewInt(2000022)),
				Nonce:     30,
				Input:     []byte{1, 2, 3, 100, 15, 100},
				Block:     20,
			},
		},
	}

	resp := PopulatePaymentsList(payments)
	raw, err := json.Marshal(resp)
	if err != nil {
		t.Fatal(err)
	}

	expected := `{"data":[{"id":"0xc7407a5a50bfa95cc28ab410885ccd3e4bea2b94de60c48c0aa3d8c6750221e3","type":"payments","attributes":{"sender":"0x4711b1dff063609b4a87f3d092059a9e12eab4f9","recipient":"0x228c8108bd0368bbb6a47cc3ad89934c82952cc2","value":"123000000000001","token":"0x228c8108bd0368bbb6a47cc3ad89934c82952cc2","timestamp":1618517598},"relationships":{"transaction":{"data":{"id":"0xc7407a5a50bfa95cc28ab410885ccd3e4bea2b94de60c48c0aa3d8c6750221e3","type":"transactions"}}}},{"id":"0x3f8f01e333bf94e7470bb3db7fa7b6a188e9477e53799f2391d227da4b4e3306","type":"payments","attributes":{"sender":"0x91e894cd2fd19be4fb661f225a96b96293a575e2","recipient":"0x228c8108bd0368bbb6a47cc3ad89934c82952cc2","value":"123000000000001","token":"0x228c8108bd0368bbb6a47cc3ad89934c82952cc2","timestamp":1618517853},"relationships":{"transaction":{"data":{"id":"0x3f8f01e333bf94e7470bb3db7fa7b6a188e9477e53799f2391d227da4b4e3306","type":"transactions"}}}}],"included":[{"id":"0xc7407a5a50bfa95cc28ab410885ccd3e4bea2b94de60c48c0aa3d8c6750221e3","type":"transactions","attributes":{"block_id":10,"sender":"0x4711b1dff063609b4a87f3d092059a9e12eab4f9","recipient":"0x228c8108bd0368bbb6a47cc3ad89934c82952cc2","value":"123000000000001","nonce":13,"gas_price":"1000022","gas":100500,"input":"010203640f","timestamp":1618517598}},{"id":"0x3f8f01e333bf94e7470bb3db7fa7b6a188e9477e53799f2391d227da4b4e3306","type":"transactions","attributes":{"block_id":20,"sender":"0x91e894cd2fd19be4fb661f225a96b96293a575e2","recipient":"0x228c8108bd0368bbb6a47cc3ad89934c82952cc2","value":"23000000000001","nonce":30,"gas_price":"2000022","gas":300500,"input":"010203640f64","timestamp":1618517853}}]}`
	if bytes.Compare(raw, []byte(expected)) != 0 {
		t.Fatalf("unexpecte output:\n expected: %s\n actual: %s\n", expected, string(raw))
	}
}

func newString(str string) *string {
	return &str
}
