package data

// EpqfiParametersVoting voting.
type EpqfiParametersVoting BaseVoting

// EpqfiParametersVotingStorage stores epqfi parameters votings.
type EpqfiParametersVotingStorage interface {
	Insert(votings ...EpqfiParametersVoting) error
	GetLatest() (*EpqfiParametersVoting, error)

	GetEarliestUnfinishedVotings() ([]EpqfiParametersVoting, error)
	Update(voting *EpqfiParametersVoting) error
}
