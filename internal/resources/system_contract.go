package resources

import (
	"strconv"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type SystemContractList struct {
	Data  []SystemContract `json:"data"`
	Links *Links           `json:"links,omitempty"`
}

type SystemContract struct {
	ID   string `json:"id"`
	Type string `json:"key"`

	Attributes SystemContractAttributes `json:"attributes"`
}

type SystemContractAttributes struct {
	Proxy          string  `json:"proxy"`
	Implementation *string `json:"implementation"`
	Key            string  `json:"key"`
}

func PopulateSysContractsList(l []data.SystemContract) SystemContractList {
	resp := SystemContractList{
		Data: make([]SystemContract, 0, len(l)),
	}

	for _, i := range l {
		resp.Data = append(resp.Data, PopulateSystemContract(&i))
	}

	return resp
}

func PopulateSystemContract(i *data.SystemContract) SystemContract {
	var impl string
	if i.Implementation.Valid {
		impl = i.Implementation.String
	}

	return SystemContract{
		ID:   strconv.FormatUint(i.ID, 10),
		Type: "system-contracts",
		Attributes: SystemContractAttributes{
			Proxy:          i.Contract,
			Implementation: &impl,
			Key:            i.Key.String,
		},
	}
}
