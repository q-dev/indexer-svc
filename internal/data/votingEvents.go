package data

// VotingEvent .
type VotingEvent struct {
	Id             uint64 `db:"id"`
	VotingType     string `db:"voting_type"`
	AccountAddress string `db:"account_address"`
	ProposalId     uint64 `db:"proposal_id"`
	BlockId        uint64 `db:"block_id"`
}

type VotingEventByAddress struct {
	AccountAddress string `db:"account_address" json:"accountAddress"`
	VotingType     string `db:"voting_type" json:"votingType"`
	Amount         uint64 `db:"amount" json:"amount"`
}

type CountEventsByAddress struct {
	AccountAddress string            `json:"accountAddress"`
	Counts         map[string]uint64 `json:"counts"`
}

type CountVotingEvents struct {
	Total     map[string]uint64      `json:"total"`
	ByAddress []CountEventsByAddress `json:"byAddress"`
}

// VotingEventsStorage stores voting events.
type VotingEventsStorage interface {
	Insert(votingEvents ...VotingEvent) error
	GetByLatestByType(votingType string) (*VotingEvent, error)

	Select(params SelectVotingEventsParams, addresses []string) ([]VotingEvent, error)
	SelectAggregated(params SelectVotingEventsParams) (CountVotingEvents, error)

	// Erase all history starting fromID inclusively.
	Erase(fromID uint64) error
}

// QTokenHolderVotesStorage stores q token holder votes.
type QTokenHolderVotesStorage interface {
	VotingEventsStorage
}

// RootNodeVotesStorage stores root nodes votes.
type RootNodeVotesStorage interface {
	VotingEventsStorage
}

// RootNodeProposalsStorage stores root nodes proposals.
type RootNodeProposalsStorage interface {
	VotingEventsStorage
}

type SelectVotingEventsParams struct {
	IsDesc bool
	Cursor *string
	Limit  uint64

	StartBlock          uint64
	AccountAddress      string
	StartBlockTimestamp uint64
	AddressList         []string
}
