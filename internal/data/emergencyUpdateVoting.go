package data

// EmergencyUpdateVoting voting.
type EmergencyUpdateVoting BaseVoting

// EmergencyUpdateVotingStorage stores emergency update votings.
type EmergencyUpdateVotingStorage interface {
	Insert(votings ...EmergencyUpdateVoting) error
	GetLatest() (*EmergencyUpdateVoting, error)

	GetEarliestUnfinishedVotings() ([]EmergencyUpdateVoting, error)
	Update(voting *EmergencyUpdateVoting) error
}
