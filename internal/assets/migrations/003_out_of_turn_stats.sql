-- +migrate Up
-- force resync and make col required
DELETE
FROM blocks;

ALTER TABLE blocks
    ADD COLUMN mainaccount     text     NOT NULL,
    ADD COLUMN inturn_signer   text     NOT NULL,
    ADD COLUMN difficulty      bigint   NOT NULL;

-- +migrate Down
ALTER TABLE blocks
DROP COLUMN mainaccount,
DROP COLUMN inturn_signer,
DROP COLUMN difficulty;
