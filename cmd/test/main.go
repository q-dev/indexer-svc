package main

import (
	"context"
	"fmt"
	"log"
	"math/big"
	"strings"

	"gitlab.com/q-dev/q-client/consensus/clique"

	"gitlab.com/q-dev/q-client/crypto"
	"gitlab.com/q-dev/q-client/ethclient"
)

func main() {
	client, _ := ethclient.Dial("https://rpc.qtestnet.org:8545")
	header, err := client.HeaderByNumber(context.TODO(), big.NewInt(10))
	if err != nil {
		log.Fatal(err)
	}

	extra := header.Extra
	if len(extra) < crypto.SignatureLength {
		log.Fatal(err)
	}

	sig := extra[len(extra)-crypto.SignatureLength:]
	pubkey, err := crypto.SigToPub(clique.SealHash(header).Bytes(), sig)
	if err != nil {
		log.Fatal(err)
	}

	addr := strings.ToLower(crypto.PubkeyToAddress(*pubkey).Hex())
	fmt.Println(addr)

}
