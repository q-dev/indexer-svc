package resources

import (
	"database/sql"
	"time"

	"gitlab.com/q-dev/indexer-svc/internal/data"
)

type AccountPair struct {
	MainAccount string `json:"main_account"`
}

type Exclusion struct {
	AccountPair
	StartBlock uint64  `json:"start_block"`
	EndBlock   *uint64 `json:"end_block"`
}

type ExclusionListAttributes struct {
	Status     string        `json:"status"`
	Timestamp  time.Time     `json:"timestamp"`
	Signers    []AccountPair `json:"signers"`
	Exclusions []Exclusion   `json:"exclusions"`

	ActiveRootPercentage float32 `json:"active_root_percentage,omitempty"`
}

type ExclusionListData struct {
	ID   string `json:"id"`
	Type string `json:"type"`

	Attributes ExclusionListAttributes `json:"attributes"`
}

type ExclusionList struct {
	Data ExclusionListData `json:"data"`
}

func GetMainAccount(account sql.NullString, alias string) string {
	mainAccount := account.String
	if !account.Valid {
		mainAccount = alias
	}

	return mainAccount
}

func PopulateExclusionList(l data.NodeExclusionList, percentage float32) ExclusionList {
	signers := make([]AccountPair, 0, len(l.Signers))
	for _, signer := range l.Signers {
		mainAccount := GetMainAccount(signer.MainAccount, signer.Alias)

		signers = append(signers, AccountPair{
			MainAccount: mainAccount,
		})
	}

	exclusions := make([]Exclusion, 0, len(l.ExclusionSet))
	for _, exclusion := range l.ExclusionSet {
		mainAccount := GetMainAccount(exclusion.MainAccount, exclusion.Alias)

		endBlock64 := uint64(exclusion.EndBlock.Int64)
		endBlock := &endBlock64
		if !exclusion.EndBlock.Valid {
			endBlock = nil
		}

		exclusions = append(exclusions, Exclusion{
			AccountPair: AccountPair{
				MainAccount: mainAccount,
			},
			StartBlock: exclusion.StartBlock,
			EndBlock:   endBlock,
		})
	}

	return ExclusionList{
		Data: ExclusionListData{
			ID:   l.Hash,
			Type: l.Type,
			Attributes: ExclusionListAttributes{
				Status:               l.Status,
				Timestamp:            l.Timestamp,
				Signers:              signers,
				Exclusions:           exclusions,
				ActiveRootPercentage: percentage,
			},
		},
	}
}
